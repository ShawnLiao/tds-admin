<!-- begin #content -->
<div id="content" class="content">
  <!-- begin breadcrumb -->
  <ol class="breadcrumb pull-right">
    <li><a href="/dashboard">首页</a></li>
    <li class="active">修改密码</li>
  </ol>
  <!-- end breadcrumb -->
  <!-- begin page-header -->
  <h1 class="page-header">修改密码<small></small></h1>
  <!-- end page-header -->

  <div class="panel panel-inverse" data-sortable-id="form-plugins-5" style="width: 600px; margin-left: auto; margin-right: auto; margin-top: 50px;">
    <div class="panel-heading">
      <h4 class="panel-title">设置密码</h4>
    </div>
    <div class="panel-body panel-form">
      <div class="form-horizontal form-bordered">
        <div class="form-group">
          <label class="control-label col-md-2">旧密码</label>
          <div class="col-md-10">
            <input type="password" name="password" id="old-password" class="form-control m-b-5">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-2">新密码</label>
          <div class="col-md-10">
            <input type="password" name="password" id="new-password" class="form-control m-b-5">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-2">确认密码</label>
          <div class="col-md-10">
            <input type="password" name="password" id="new-password-confirm" class="form-control m-b-5">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-2"></label>
          <div class="col-md-10">
            <button type="submit" class="btn btn-primary" id="btn-submit">提交</button>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
<!-- end #content -->



