<div id="content" class="content">
  <ol class="breadcrumb pull-right">
    <li><a href="/dashboard">首页</a></li>
    <li class="active">服务器域名管理</li>
  </ol>

  <h1 class="page-header">服务器域名管理<small></small></h1>

  <div id="error-panel">
  </div>
  <div class="alert alert-danger fade in m-b-15" id="error-template" style="display: none;">
    <strong>错误：</strong>
    <span>时间间隔不能超过三个月</span>
    <span class="close" data-dismiss="alert">×</span>
  </div>

  <!-- begin panel -->
	<!--
  <div class="panel panel-inverse" data-sortable-id="ui-widget-3">
    <div class="panel-heading">
      <h4 class="panel-title">根据服务器属性查询</h4>
    </div>

    <div class="panel-body">
      <div class="row">
        <div class="col-md-4">
          <form class="form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label">服务器ip</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="host-ip" placeholder="可不填">
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <form class="form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label">来源ID</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="deal-way-list" placeholder="可多填，用,分隔">
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <form class="form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label"></label>
              <div class="col-md-9">
								<select class="form-control" id="status" >
								  <option value="" selected>全部</option>
								  <option value="-1">宕机</option>
								  <option value="0">空闲</option>
								  <option value="1">连接等待</option>
								  <option value="2">运行中</option>
								  <option value="3">空闲准备</option>
								</select>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <a href="javascript:;" class="btn btn-primary btn-block btn-search">查询</a>
	-->

  <!-- begin table -->
  <div class="table-responsive table-bg-white">
    <div class="dataTables_length" id="data-table_length" style="margin: 15px 0 0 15px;">
      <label>每页显示
        <select id="page-item-count" name="data-table_length" aria-controls="data-table" class="">
          <option value="10">10</option>
          <option value="50">50</option>
          <option value="100">100</option>
          <option value="200">200</option>
        </select>条记录
      </label>
    </div>
    <div>
      <input  id=sort_name type = "hidden" value = "0" >
    </div>
    <table class="table">
      <thead>
        <tr>
          <th>服务器地址</th>
          <th>服务器域名</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody id="item-list">
        <tr id="item-template" style="display:none">
          <td></td>
          <td><input type="text" style="width:500px"></td>
          <td><button class="change-op">域名变更</button></td>
        </tr>
      </tbody>
    </table>
		<div class="dataTables_info" id="data-table_info1" style="margin:0 0 15px 15px; display: none;" role="status" aria-live="polite">显示<span>57</span>条数据中的第<span>1</span>到<span>10</span>条</div>
		<div class="dataTables_paginate paging_simple_numbers" id="data-table_paginate" style="display: none;">
			<a class="paginate_button previous disabled" aria-controls="data-table" data-dt-idx="0" tabindex="0" id="data-table_previous">Previous</a>
			<span>
				<a id="page-btn-template" style="display:none;" class="paginate_button" aria-controls="data-table" data-dt-idx="0" tabindex="0">0</a>
			</span>
			<a class="paginate_button next" aria-controls="data-table" data-dt-idx="7" tabindex="0" id="data-table_next">Next</a>
			<input id="result-rows" type="hidden" value=""/>
			<input id="result-count-per-page" type="hidden" value=""/>
			<input id="result-table" type="hidden" value=""/>
		</div>
  </div>

  <!-- end table -->
</div>
