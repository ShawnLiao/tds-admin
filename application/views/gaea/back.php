<style>
  pre {outline: 1px solid #ccc; padding: 5px; margin: 5px; }
  .string { color: green; }
  .number { color: blue; }
  .boolean { color: darkorange; }
  .null { color: magenta; }
  .key { color: purple; }
</style>
<div id="content" class="content">
  <ol class="breadcrumb pull-right">
    <li><a href="/dashboard">首页</a></li>
    <li class="active">游戏数据</li>
  </ol>

  <h1 class="page-header">游戏数据<small></small></h1>

  <div id="error-panel">
  </div>
  <div class="alert alert-danger fade in m-b-15" id="error-template" style="display: none;">
    <strong>错误：</strong>
    <span>时间间隔不能超过三个月</span>
    <span class="close" data-dismiss="alert">×</span>
  </div>

  <!-- begin panel -->
  <div class="panel panel-inverse" data-sortable-id="ui-widget-3">
    <div class="panel-heading">
      <h4 class="panel-title">根据角色属性查询</h4>
    </div>

    <div class="panel-body">
      <div class="row">
        <div class="col-md-4">
          <form class="form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label">角色id</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="role-id" placeholder="可不填" value=<?php echo $role_id;?>>
              </div>
            </div>
          </form>
        </div>
				<div class="col-md-4">                                    
					<form class="form-horizontal">                          
						<div class="form-group">                              
							<label class="col-md-3 control-label">小镇列表</label>
							<div class="col-md-9">                              
								<select class="form-control" id="town-id" >     
								</select>                                         
							</div>                                              
						</div>                                                
					</form>                                                 
				</div>                                                    
      </div>
    </div>
  </div>
  <!-- end panel -->

  <a href="javascript:;" class="btn btn-primary btn-block btn-search">查询</a>

  <!-- begin table -->
		<div style="float:left;width:50%;border:1px">
			<pre id="roleData" style="height:600px;width:99%;">

			</pre>
		</div>
		<div style="float:left;width:50%;border:1px">
			<pre id="townData" style="height:600px;width:99%;">

			</pre>
		</div>
</div>
