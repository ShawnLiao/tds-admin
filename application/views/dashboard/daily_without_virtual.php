<div id="content" class="content">
  <!-- begin breadcrumb -->
  <ol class="breadcrumb pull-right">
    <li><a href="/dashboard/daily_without_virtual">天级数据(不包含虚拟商品)</a></li>
  </ol>

  <h1 class="page-header">天级数据(去除虚拟商品、WAP14、一分钱、挖矿、一元购等)<small> （销售总额：用户实际购买的商品价值总和；用户付款总额：用户实际支付的金额总和即不包括优惠券金额）</small></h1>

  <div id="daily-gmv" style="margin-bottom: 15px; height: 240px;"></div>

  <div id="daily-order-count" style="margin-bottom: 15px; height: 240px;"></div>

  <div id="daily-user-count" style="margin-bottom: 15px; height: 240px;"></div>

  <div id="daily-price" style="margin-bottom: 15px; height: 240px;"></div>

  <div id="daily-shop-count" style="margin-bottom: 15px; height: 240px;"></div>

</div>
