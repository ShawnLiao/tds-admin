<div id="content" class="content">
  <!-- begin breadcrumb -->
  <ol class="breadcrumb pull-right">
    <li><a href="/dashboard">天级数据</a></li>
  </ol>

  <h1 class="page-header">天级数据<small> （销售总额：用户实际购买的商品价值总和；用户付款总额：用户实际支付的金额总和即不包括优惠券金额）</small></h1>

  <div id="daily-gmv" style="margin-bottom: 15px; height: 240px;"></div>

  <div id="daily-order-count" style="margin-bottom: 15px; height: 240px;"></div>

  <div id="daily-user-count" style="margin-bottom: 15px; height: 240px;"></div>

  <div id="daily-price" style="margin-bottom: 15px; height: 240px;"></div>

  <div id="daily-shop-count" style="margin-bottom: 15px; height: 240px;"></div>
  
  <div id="daily-dau" style="margin-bottom: 15px; height: 240px;"></div>

</div>
