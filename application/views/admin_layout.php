<html>
<head>
  <meta charset="utf-8" />
  <title><?php echo $page_title ?></title>
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />

  <!-- ================== BEGIN BASE CSS STYLE ================== -->
  <?php foreach ($base_styles as $file => $type) echo HTML::style($file), "\n" ?>
  <!-- ================== END BASE CSS STYLE ================== -->

	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
  <?php foreach ($page_styles as $file => $type) echo HTML::style($file), "\n" ?>
	<!-- ================== END PAGE LEVEL STYLE ================== -->

  <!-- ================== BEGIN PACE JS ================== -->
  <?php foreach ($pace_scripts as $file) echo HTML::script($file), "\n" ?>
  <!-- ================== END PACE JS ================== -->
  
</head>
<body>
  <!-- Page Loader -->
  <?php echo $page_loader ?>

  <!-- Page Container -->
  <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    <!-- Page Header -->
    <?php echo $page_header ?>

    <!-- Page Sidebar -->
    <?php echo $page_sidebar ?>
    
    <!-- Page Content-->
    <?php echo $page_content ?>

  </div>

  <!-- ================== BEGIN BASE JS ================== -->
  <?php foreach ($base_scripts as $file) echo HTML::script($file), "\n" ?>
  <?php foreach ($ie_scripts as $file) echo HTML::script($file), "\n" ?>
  <!-- ================== END BASE JS ================== -->

  <!-- ================== BEGIN PAGE JS ================== -->
  <?php foreach ($page_scripts as $file) echo HTML::script($file), "\n" ?>
  <!-- ================== END PAGE JS ================== -->
  
  <?php echo $init_script ?>
</body>
</html>
