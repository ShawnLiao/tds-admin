<div id="content" class="content">
  <!-- begin breadcrumb -->
  <ol class="breadcrumb pull-right">
    <li><a href="/dashboard">首页</a></li>
    <li class="active"> 日报汇总</li>
  </ol>
  <!-- end breadcrumb -->
  <!-- begin page-header -->
  <h1 class="page-header">日报汇总
    <small></small>
  </h1>
  <!-- end page-header -->

  <div id="error-panel">
  </div>
  <div class="alert alert-danger fade in m-b-15" id="error-template" style="display: none;">
    <strong>错误：</strong>
    <span>时间间隔不能超过三个月</span>
    <span class="close" data-dismiss="alert">×</span>
  </div>

  <!-- begin panel -->
  <div class="panel panel-inverse" data-sortable-id="form-stuff-5">
    <div class="panel-heading">
      <div class="panel-heading-btn">
        <!--
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        -->
      </div>
      <h4 class="panel-title">根据时间查询</h4>
    </div>
    <div class="panel-body">
      <!-- begin row-->
      <div class="row">
        <div class="col-md-4">
          <form class="form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label">查询时间</label>

              <div class="col-md-9">
                <div class="input-group input-daterange daterange-customer">
                  <input type="text" class="form-control" name="start" id="nature-date-start" placeholder="起始日期">
                  <span class="input-group-addon">到</span>
                  <input type="text" class="form-control" name="end" id="nature-date-end" placeholder="结束日期">
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-4">
    <form class="form-horizontal">

      <div class="form-group ">
        <label for="sceneModel_title" class="col-md-3 control-label"><B>可选类目</B></label>

        <div class="col-md-9">
          <select class="form-control selectpicker" id="chooseTitle" multiple>
            <option name="s" value="0"  truename="date" selected>日期</option>
            <option name="s" value="1"  truename="name" selected>类型</option>
            <option name="s" value="2"  truename="DAU" selected>DAU</option>
            <option name="s" value="3"  truename="total_install" selected>总新增</option>
            <option name="s" value="4"  truename="new_user" selected>总新客</option>
            <option name="s" value="5"  truename="new_user_rate" selected>新客转化率(%)</option>
            <option name="s" value="6"  truename="total_naturl_install" selected>自然量</option>
            <option name="s" value="7"  truename="fee_install" selected>付费新增</option>
            <option name="s" value="8"  truename="fee" selected>花费</option>
            <option name="s" value="9"  truename="fee_new_user" selected>付费新客</option>
            <option name="s" value="10"  truename="pay_new_user" selected>当日付费新客</option>
            <option name="s" value="11"  truename="fee_cost" selected>付费新增成本</option>
            <option name="s" value="12" truename="fee_new_user_cost" selected>付费新客成本</option>
            <option name="s" value="13" truename="new_fee" selected>总新客付款金额</option>
            <option name="s" value="14" truename="feeuser_fee" selected>付费新客付款金额</option>
            <option name="s" value="15" truename="new_user_pay_fee" selected>当日付费新客付款金额</option>
            <option name="s" value="16" truename="user_roi" selected>新客ROI</option>
            <option name="s" value="17" truename="install_roi" selected>付费新客ROI</option>
            <option name="s" value="18" truename="new_user_roi" selected>当日付费新客ROI</option>
            <option name="s" value="19" truename="pay_order" selected>订单数</option>
            <option name="s" value="20" truename="pay_perprice" selected>总客单价</option>
            <option name="s" value="21" truename="install" selected>总新增</option>
            <option name="s" value="22" truename="iccj_install">楚楚街ios总新增</option>
            <option name="s" value="23" truename="accj_install">楚楚街安卓总新增</option>
            <option name="s" value="24" truename="i99_install">9块9ios总新增</option>
            <option name="s" value="25" truename="a99_install">9块9安卓总新增</option>
            <option name="s" value="26" truename="lt_install">辣条总新增</option>
            <option name="s" value="27" truename="total_fee_install">总付费新增</option>
            <option name="s" value="28" truename="iccj_fee_install">楚楚街ios总付费新增</option>
            <option name="s" value="29" truename="accj_fee_install">楚楚街安卓总付费新增</option>
            <option name="s" value="30" truename="i99_fee_install">9块9ios总付费新增</option>
            <option name="s" value="31" truename="lt_fee_install">辣条付费新增</option>
            <option name="s" value="32" truename="a99_fee_install">9块9安卓总付费新增</option>
            <option name="s" value="33" truename="total_naturl_install">总自然量</option>
            <option name="s" value="34" truename="i99_naturl_install">楚楚街ios总自然量</option>
            <option name="s" value="35" truename="accj_naturl_install">楚楚街安卓总自然量</option>
            <option name="s" value="36" truename="i99_naturl_install">9块9ios总自然量</option>
            <option name="s" value="37" truename="a99_naturl_install">9块9安卓总自然量</option>
            <option name="s" value="38" truename="lt_naturl_install">辣条总自然量</option>
            <option name="s" value="39" truename="refund_fee">退款金额</option>
          </select>
        </div>

      </div>
    </form>
  </div>



        <div class="col-md-2"><a href="javascript:;" class="btn btn-primary btn-block btn-search">查询</a></div>
      </div>
    </div>

    <!-- end row -->

  </div>
  <!-- end panel -->
  <!-- begin table -->
  <div class="table-responsive table-bg-white">
    <div class="download-group">
      <button id="download" type="button" class="btn btn-default"><i class="fa fa-download"></i> CVS</button>
    </div>
    <!--    <div class="dataTables_length" id="data-table_length" style="margin: 15px 0 0 15px;">-->
    <!--      <label>每页显示-->
    <!--        <select id="page-item-count" name="data-table_length" aria-controls="data-table" class="">-->
    <!--          <option value="10">10</option>-->
    <!--          <option value="50">50</option>-->
    <!--          <option value="100">100</option>-->
    <!--          <option value="200">200</option>-->
    <!--        </select>条记录-->
    <!--      </label>-->
    <!--    </div>-->
    <div>
      <input id=sort_name type="hidden" value="0">
    </div>
    <table class="table table-striped dataTable no-footer" role="grid" aria-describedby="data-table_info" id="tab">
      <thead id="thd">
      <tr>
        <th class="sorting" id="sortdate" value="0">日期</th>
        <th class="sorting" id="sortdate" value="1">类型</th>
        <th class="sorting" id="sortnature" value="2">DAU</th>
        <th class="sorting" id="sortinstall" value="3">总新增</th>
        <th class="sorting" id="sortnew_user" value="4">总新客</th>
        <th class="sorting" id="sortnew_user_rate" value="5">新客转化率(%)</th>
        <th class="sorting" id="sortnaturl_install" value="6">自然量</th>
        <th class="sorting" id="sortfee_install" value="7">付费新增</th>
        <th class="sorting" id="sortfee" value="8">花费</th>
        <th class="sorting" id="sortfee_new_user" value="9">付费新客</th>
        <th class="sorting" id="sortpay_new_user" value="10">当日付费新客</th>
        <th class="sorting" id="sortfee_cost" value="11">付费新增成本</th>
        <th class="sorting" id="sortfee_new_user_cost" value="12">付费新客成本</th>
        <th class="sorting" id="sortnew_fee" value="13">总新客付款金额</th>
        <th class="sorting" id="sortfeeuser_fee" value="14">付费新客付款金额</th>
        <th class="sorting" id="sortnew_user_pay_fee" value="15">当日付费新客付款金额</th>
        <th class="sorting" id="sortuser_roi" value="16">新客ROI</th>
        <th class="sorting" id="sortinstall_roi" value="17">付费新客ROI</th>
        <th class="sorting" id="sortnew_user_roi" value="18">当日付费新客ROI</th>
        <th class="sorting" id="sortpay_order" value="19">订单数</th>
        <th class="sorting" id="sortpay_perprice" value="20">总客单价</th>
        <th class="sorting" id="sortbrand_adid" value="21">总新增</th>
        <th class="sorting" id="sortbrand_title" value="22">楚楚街ios总新增</th>
        <th class="sorting" id="sortbegin_date" value="23">楚楚街安卓总新增</th>
        <th class="sorting" id="sortend_date" value="24">9块9ios总新增</th>
        <th class="sorting" id="sortbrand_type" value="25">9块9安卓总新增</th>
        <th class="sorting" id="sortbrand_type" value="26">辣条总新增</th>
        <th class="sorting" id="sortpv" value="27">总付费新增</th>
        <th class="sorting" id="sortpc" value="28">楚楚街ios总付费新增</th>
        <th class="sorting" id="sortpcr" value="29">楚楚街安卓总付费新增</th>
        <th class="sorting" id="sortuv" value="30">9块9ios总付费新增</th>
        <th class="sorting" id="sortorder_pay_count_1" value="31">9块9安卓总付费新增</th>
        <th class="sorting" id="sortbrand_type" value="32">辣条付费新增</th>
        <th class="sorting" id="sortorder_pay_fee_1" value="33">总自然量</th>
        <th class="sorting" id="sortorder_pay_fee_1_uv" value="34">楚楚街ios总自然量</th>
        <th class="sorting" id="sortorder_pay_count_2" value="35">楚楚街安卓总自然量</th>
        <th class="sorting" id="sortorder_pay_count_3" value="36">9块9ios总自然量</th>
        <th class="sorting" id="sortorder_pay_count_4" value="37">9块9安卓总自然量</th>
        <th class="sorting" id="sortbrand_type" value="38">辣条总自然量</th>
        <th class="sorting" id="sortrefund_fee" value="39">退款金额</th>
      </tr>
      <!-- 汇总 行 开始   -->
      <!--      <tr id="tr2">-->
      <!--        <th>汇总</th>-->
      <!--        <th>null</th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th>null</th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th>null</th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th>null</th>-->
      <!--        <th>null</th>-->
      <!--      </tr>-->
      <!-- 汇总 行 结束   -->
      </thead>
      <tbody id="total-list">
        <tr id="total-template" style="display:none">
          <th value="0"></th>
          <th value="1"></th>
          <th value="2"></th>
          <th value="3"></th>
          <th value="4"></th>
          <th value="5"></th>
          <th value="6"></th>
          <th value="7"></th>
          <th value="8"></th>
          <th value="9"></th>
          <th value="10"></th>
          <th value="11"></th>
          <th value="12"></th>
          <th value="13"></th>
          <th value="14"></th>
          <th value="15"></th>
          <th value="16"></th>
          <th value="17"></th>
          <th value="18"></th>
          <th value="19"></th>
          <th value="20"></th>
          <th value="21"></th>
          <th value="22"></th>
          <th value="23"></th>
          <th value="24"></th>
          <th value="25"></th>
          <th value="26"></th>
          <th value="27"></th>
          <th value="28"></th>
          <th value="29"></th>
          <th value="30"></th>
          <th value="31"></th>
          <th value="32"></th>
          <th value="33"></th>
          <th value="34"></th>
          <th value="35"></th>
          <th value="36"></th>
          <th value="37"></th>
          <th value="38"></th>
          <th value="39"></th>
        </tr>
      </tbody>
      <tbody id="nature-item-list">
      <tr id="nature-item-template" style="display:none">
        <td value="0"></td>
        <td value="1"></td>
        <td value="2"></td>
        <td value="3"></td>
        <td value="4"></td>
        <td value="5"></td>
        <td value="6"></td>
        <td value="7"></td>
        <td value="8"></td>
        <td value="9"></td>
        <td value="10"></td>
        <td value="11"></td>
        <td value="12"></td>
        <td value="13"></td>
        <td value="14"></td>
        <td value="15"></td>
        <td value="16"></td>
        <td value="17"></td>
        <td value="18"></td>
        <td value="19"></td>
        <td value="20"></td>
        <td value="21"></td>
        <td value="22"></td>
        <td value="23"></td>
        <td value="24"></td>
        <td value="25"></td>
        <td value="26"></td>
        <td value="27"></td>
        <td value="28"></td>
        <td value="29"></td>
        <td value="30"></td>
        <td value="31"></td>
        <td value="32"></td>
        <td value="33"></td>
        <td value="34"></td>
        <td value="35"></td>
        <td value="36"></td>
        <td value="37"></td>
        <td value="38"></td>
        <td value="39"></td>
      </tr>

      </tbody>
    </table>
    <div class="dataTables_info" id="data-table_info" style="margin:0 0 15px 15px; display: none;" role="status"
         aria-live="polite">显示<span>57</span>条数据中的第<span>1</span>到<span>10</span>条
    </div>
    <div class="dataTables_paginate paging_simple_numbers" id="data-table_paginate" style="display: none;">
      <a class="paginate_button previous disabled" aria-controls="data-table" data-dt-idx="0" tabindex="0"
         id="data-table_previous">Previous</a>
      <span>
        <a id="page-btn-template" style="display:none;" class="paginate_button" aria-controls="data-table"
           data-dt-idx="0" tabindex="0">0</a>
      </span>
      <a class="paginate_button next" aria-controls="data-table" data-dt-idx="7" tabindex="0"
         id="data-table_next">Next</a>
      <input id="result-rows" type="hidden" value=""/>
      <input id="result-count-per-page" type="hidden" value=""/>
      <input id="result-table" type="hidden" value=""/>
    </div>
  </div>
  <!-- end table -->
 <div class="panel panel-inverse" data-sortable-id="form-stuff-5" style="margin-top:20px">

   <div class="panel-body">

     <div class="row">
      <!-- <div class="col-md-4">
         <form class="form-horizontal">
           <div class="form-group">
             <label class="col-md-3 control-label"><B>类型类目</B></label>
             <div class="col-md-9">
               <select class="form-control" id="typeChoose" >
                 <option value="0" truename="online">线上市场</option>
                 <option value="1" truename="drop">精准投放</option>
                 <option value="2" truename="nature">自然量</option>
                 <option value="3" truename="market">营销活动</option>
               </select>
             </div>
           </div>
         </form>
       </div>-->
       <div class="col-md-4">
         <form class="form-horizontal">

           <div class="form-group ">
             <label for="sceneModel_title" class="col-md-3 control-label"><B>图表类目</B></label>

             <div class="col-md-9">
               <select class="form-control selectpicker" id="chartTitle" multiple>
                <!-- <option name="s" value="0"  truename="date">日期</option>
                 <option name="s" value="1"  truename="name">类型</option>-->
                 <option name="q" value="0"  truename="DAU" flag=0>DAU</option>
                 <option name="q" value="1"  truename="total_install" flag=0 >总新增</option>
                 <option name="q" value="2"  truename="new_user" flag=0>总新客</option>
                 <option name="q" value="3"  truename="new_user_rate" flag=0>新客转化率(%)</option>
                 <option name="q" value="4"  truename="total_naturl_install" flag=0 >自然量</option>
                 <option name="q" value="5"  truename="fee_install" flag=0 >付费新增</option>
                 <option name="q" value="6"  truename="fee" flag=0>花费</option>
                 <option name="q" value="7"  truename="fee_new_user" flag=0>付费新客</option>
                 <option name="q" value="8"  truename="fee_cost" flag=1>付费新增成本</option>
                 <option name="q" value="9"  truename="fee_new_user_cost" flag=1>付费新客成本</option>
                 <option name="q" value="10"  truename="new_fee" flag=0>总新客付款金额</option>
                 <option name="q" value="11" truename="feeuser_fee" flag=0>付费新客付款金额</option>
                 <option name="q" value="12" truename="user_roi" flag=1>新客ROI</option>
                 <option name="q" value="13" truename="install_roi" flag=1>付费新客ROI</option>
                 <option name="q" value="14" truename="pay_order" flag=0>订单数</option>
                 <option name="q" value="15" truename="pay_perprice" flag=1>总客单价</option>
                 <option name="q" value="16" truename="install" flag=0>总新增</option>
                 <option name="q" value="17" truename="iccj_install" flag=0>楚楚街ios总新增</option>
                 <option name="q" value="18" truename="accj_install" flag=0>楚楚街安卓总新增</option>
                 <option name="q" value="19" truename="i99_install" flag=0>9块9ios总新增</option>
                 <option name="q" value="20" truename="a99_install" flag=0>9块9安卓总新增</option>
                 <option name="q" value="21" truename="lt_install" flag=0>辣条总新增</option>
                 <option name="q" value="22" truename="total_fee_install" flag=0>总付费新增</option>
                 <option name="q" value="23" truename="iccj_fee_install" flag=0>楚楚街ios总付费新增</option>
                 <option name="q" value="24" truename="accj_fee_install" flag=0>楚楚街安卓总付费新增</option>
                 <option name="q" value="25" truename="i99_fee_install" flag=0>9块9ios总付费新增</option>
                 <option name="q" value="26" truename="a99_fee_install" flag=0>9块9安卓总付费新增</option>
                 <option name="q" value="27" truename="lt_fee_install" flag=0>辣条付费新增</option>

                 <option name="s" value="28" truename="total_naturl_install" flag=0>总自然量</option>
                 <option name="s" value="29" truename="i99_naturl_install" flag=0>楚楚街ios总自然量</option>
                 <option name="s" value="30" truename="accj_naturl_install" flag=0>楚楚街安卓总自然量</option>
                 <option name="s" value="31" truename="i99_naturl_install" flag=0>9块9ios总自然量</option>
                 <option name="s" value="32" truename="a99_naturl_install" flag=0>9块9安卓总自然量</option>
                 <option name="s" value="33" truename="lt_naturl_install" flag=0>辣条总自然量</option>
                 <option name="q" value="34" truename="refund_fee" flag=1>退款金额</option>
                </select>
              </div>

            </div>
          </form>
        </div>

      </div>
    </div>
  </div>
  <div class="table-responsive table-bg-white">
    <div id="daily-charts" style="margin-bottom: 15px; height: 400px;"></div>
  </div>
                
</div>
<!-- end #content -->



