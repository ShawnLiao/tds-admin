<div id="content" class="content">
  <!-- begin breadcrumb -->
  <ol class="breadcrumb pull-right">
    <li><a href="/dashboard">首页</a></li>
    <li class="active"> 营销活动</li>
  </ol>
  <!-- end breadcrumb -->
  <!-- begin page-header -->
  <h1 class="page-header">营销活动
    <small></small>
  </h1>
  <!-- end page-header -->

  <div id="error-panel">
  </div>
  <div class="alert alert-danger fade in m-b-15" id="error-template" style="display: none;">
    <strong>错误：</strong>
    <span>时间间隔不能超过三个月</span>
    <span class="close" data-dismiss="alert">×</span>
  </div>

  <!-- begin panel -->
  <div class="panel panel-inverse" data-sortable-id="form-stuff-5">
    <div class="panel-heading">
      <div class="panel-heading-btn">
        <!--
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        -->
      </div>
      <h4 class="panel-title">根据时间查询</h4>
    </div>
    <div class="panel-body">
      <!-- begin row-->
      <div class="row">
        <div class="col-md-4">
          <form class="form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label">查询时间</label>

              <div class="col-md-9">
                <div class="input-group input-daterange daterange-customer">
                  <input type="text" class="form-control" name="start" id="market-date-start" placeholder="起始日期">
                  <span class="input-group-addon">到</span>
                  <input type="text" class="form-control" name="end" id="market-date-end" placeholder="结束日期">
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <a href="javascript:;" class="btn btn-primary btn-block btn-search">查询</a>
        </div>
        <div class="col-md-4">
          <a href="javascript:;" class="btn btn-primary btn-block btn-ok" style="background: #00acac;border: none">确认更改</a>
        </div>
      </div>

    </div>

    <!-- end row -->

  </div>
  <!-- end panel -->
  <!-- begin table -->
  <div class="table-responsive table-bg-white">
    <div class="download-group">
      <button id="download" type="button" class="btn btn-default"><i class="fa fa-download"></i> CVS</button>
    </div>
    <!--    <div class="dataTables_length" id="data-table_length" style="margin: 15px 0 0 15px;">-->
    <!--      <label>每页显示-->
    <!--        <select id="page-item-count" name="data-table_length" aria-controls="data-table" class="">-->
    <!--          <option value="10">10</option>-->
    <!--          <option value="50">50</option>-->
    <!--          <option value="100">100</option>-->
    <!--          <option value="200">200</option>-->
    <!--        </select>条记录-->
    <!--      </label>-->
    <!--    </div>-->
    <div>
      <input id=sort_name type="hidden" value="0">
    </div>
    <table class="table table-striped dataTable no-footer" role="grid" aria-describedby="data-table_info">
      <thead id="thd">
      <tr>
        <th class="sorting" id="sortdate">日期</th>
        <th class="sorting" id="sortnew_oc_order_num">下单用户数</th>
        <th class="sorting" id="sortnew_oc_order_sum">下单金额</th>
        <th class="sorting" id="sortnew_oc_pay_num">付款用户数</th>
        <th class="sorting" id="sortnew_oc_pay_sum">付款金额</th>
        <th class="sorting" id="sortnew_oc_pay_ratio">付款成功率</th>
        <th class="sorting" id="sortnw_2oc_num">总新客</th>
        <th class="sorting" id="sortnw_2oc_sum">总新客付款金额</th>
        <th class="sorting" id="sortnw_2oc_ratio">总新客客单价</th>
        <th class="sorting" id="sortnew_cost">总新客成本</th>
        <th class="sorting" id="sortfee">花费</th>
        <th class="sorting" id="sortprice">支出单价</th>
        <th class="sorting" id="sortroi">ROI</th>
      </tr>
      <!-- 汇总 行 开始   -->
      <!--      <tr id="tr2">-->
      <!--        <th>汇总</th>-->
      <!--        <th>null</th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th>null</th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th>null</th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th>null</th>-->
      <!--        <th>null</th>-->
      <!--      </tr>-->
      <!-- 汇总 行 结束   -->
      </thead>
      <tbody id="market-item-list">
      <tr id="market-item-template" style="display:none">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><input type="text" class="unit-price"></td>
        <td></td>
      </tr>

      </tbody>
    </table>
    <div class="dataTables_info" id="data-table_info" style="margin:0 0 15px 15px; display: none;" role="status" aria-live="polite">显示<span>57</span>条数据中的第<span>1</span>到<span>10</span>条
    </div>
    <div class="dataTables_paginate paging_simple_numbers" id="data-table_paginate" style="display: none;">
      <a class="paginate_button previous disabled" aria-controls="data-table" data-dt-idx="0" tabindex="0"
         id="data-table_previous">Previous</a>
      <span>
        <a id="page-btn-template" style="display:none;" class="paginate_button" aria-controls="data-table"
           data-dt-idx="0" tabindex="0">0</a>
      </span>
      <a class="paginate_button next" aria-controls="data-table" data-dt-idx="7" tabindex="0"
         id="data-table_next">Next</a>
      <input id="result-rows" type="hidden" value=""/>
      <input id="result-count-per-page" type="hidden" value=""/>
      <input id="result-table" type="hidden" value=""/>
    </div>
  </div>
  <!-- end table -->
</div>
<!-- end #content -->



