<div id="content" class="content">
  <!-- begin breadcrumb -->
  <ol class="breadcrumb pull-right">
    <li><a href="/dashboard">首页</a></li>
    <li class="active"> 月报汇总</li>
  </ol>
  <!-- end breadcrumb -->
  <!-- begin page-header -->
  <h1 class="page-header">月报汇总
    <small></small>
  </h1>
  <!-- end page-header -->

  <div id="error-panel">
  </div>
  <div class="alert alert-danger fade in m-b-15" id="error-template" style="display: none;">
    <strong>错误：</strong>
    <span>时间间隔不能超过三个月</span>
    <span class="close" data-dismiss="alert">×</span>
  </div>

  <!-- begin panel -->
  <div class="panel panel-inverse" data-sortable-id="form-stuff-5">
    <div class="panel-heading">
      <div class="panel-heading-btn">
        <!--
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        -->
      </div>
      <h4 class="panel-title">根据时间查询</h4>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-4">
          <form class="form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label">查询时间</label>

              <div class="col-md-9">
                <div class="input-group input-daterange daterange-customer">
                  <input type="text" class="form-control" name="start" id="date-start" placeholder="起始日期">
                  <span class="input-group-addon">到</span>
                  <input type="text" class="form-control" name="end" id="date-end" placeholder="结束日期">
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-2"><a href="javascript:;" class="btn btn-primary btn-block btn-search">查询</a></div>
      </div>
    </div>
  </div>
  <!-- end panel -->
  <!-- begin table -->
  <div class="table-responsive table-bg-white">
    <div class="download-group">
      <button id="download" type="button" class="btn btn-default"><i class="fa fa-download"></i> CVS</button>
    </div>
    <!--    <div class="dataTables_length" id="data-table_length" style="margin: 15px 0 0 15px;">-->
    <!--      <label>每页显示-->
    <!--        <select id="page-item-count" name="data-table_length" aria-controls="data-table" class="">-->
    <!--          <option value="10">10</option>-->
    <!--          <option value="50">50</option>-->
    <!--          <option value="100">100</option>-->
    <!--          <option value="200">200</option>-->
    <!--        </select>条记录-->
    <!--      </label>-->
    <!--    </div>-->
    <div>
      <input id=sort_name type="hidden" value="0">
    </div>
    <table class="table table-striped dataTable no-footer" role="grid" aria-describedby="data-table_info" id="tab">
      <thead id="thd">
      <tr>
        <th class="sorting" id="sortdate">月份</th>
        <th class="sorting" id="sortdate">类型</th>
        <th class="sorting" id="sortdate">DAU</th>
        <th class="sorting" id="sortinstall">总新增</th>
        <th class="sorting" id="sortnew_user">总新客</th>
        <th class="sorting" id="sortnaturl_install">自然量</th>
        <th class="sorting" id="sortfee_install">付费新增</th>
        <th class="sorting" id="sort">花费</th>
        <th class="sorting" id="sortfee_new_user">付费新客</th>
        <th class="sorting" id="sortfee_cost">付费新增成本</th>
        <th class="sorting" id="sortfee_new_user_cost">付费新客成本</th>
        <th class="sorting" id="sortnew_fee">总新客付款金额</th>
        <th class="sorting" id="sortfeeuser_fee">付费新客付款金额</th>
        <th class="sorting" id="sortuser_roi">新客ROI</th>
        <th class="sorting" id="sortinstall_roi">付费新客ROI</th>
        <th class="sorting" id="sortpay_order">订单数</th>
        <th class="sorting" id="sortpay_perprice">总客单价</th>
        <th class="sorting" id="sortm_install">月新增</th>
        <th class="sorting" id="sortm_user">月新客</th>
        <th class="sorting" id="sortd_install">日均新增</th>
        <th class="sorting" id="sortd_user">日均新客</th>
        <th class="sorting" id="sortp_install">单用户新增成本</th>
        <th class="sorting" id="sortp_user">单用户新客成本</th>
        <th class="sorting" id="sortm_fee">月推广费</th>
      </tr>
      <!-- 汇总 行 开始   -->
      <!--      <tr id="tr2">-->
      <!--        <th>汇总</th>-->
      <!--        <th>null</th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th>null</th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th>null</th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th>null</th>-->
      <!--        <th>null</th>-->
      <!--      </tr>-->
      <!-- 汇总 行 结束   -->
      </thead>
      <tbody id="monthlyreport-item-list">
      <tr id="monthlyreport-item-template" style="display:none">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>

      </tbody>
    </table>
    <div class="dataTables_info" id="data-table_info" style="margin:0 0 15px 15px; display: none;" role="status"
         aria-live="polite">显示<span>57</span>条数据中的第<span>1</span>到<span>10</span>条
    </div>
    <div class="dataTables_paginate paging_simple_numbers" id="data-table_paginate" style="display: none;">
      <a class="paginate_button previous disabled" aria-controls="data-table" data-dt-idx="0" tabindex="0"
         id="data-table_previous">Previous</a>
      <span>
        <a id="page-btn-template" style="display:none;" class="paginate_button" aria-controls="data-table"
           data-dt-idx="0" tabindex="0">0</a>
      </span>
      <a class="paginate_button next" aria-controls="data-table" data-dt-idx="7" tabindex="0"
         id="data-table_next">Next</a>
      <input id="result-rows" type="hidden" value=""/>
      <input id="result-count-per-page" type="hidden" value=""/>
      <input id="result-table" type="hidden" value=""/>
    </div>
  </div>
  <!-- end table -->
</div>
<!-- end #content -->



