<div id="content" class="content">
  <!-- begin breadcrumb -->
  <ol class="breadcrumb pull-right">
    <li><a href="/dashboard">首页</a></li>
    <li class="active"> 月报</li>
  </ol>
  <!-- end breadcrumb -->
  <!-- begin page-header -->
  <h1 class="page-header">月报
    <small></small>
  </h1>
  <!-- end page-header -->

  <div id="error-panel">
  </div>
  <div class="alert alert-danger fade in m-b-15" id="error-template" style="display: none;">
    <strong>错误：</strong>
    <span>时间间隔不能超过三个月</span>
    <span class="close" data-dismiss="alert">×</span>
  </div>

  <!-- begin panel -->
  <div class="panel panel-inverse" data-sortable-id="form-stuff-5">
    <div class="panel-heading">
      <div class="panel-heading-btn">
        <!--
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        -->
      </div>
      <h4 class="panel-title">根据时间查询</h4>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-4">
          <form class="form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label">查询时间</label>

              <div class="col-md-9">
                <div class="input-group input-daterange daterange-customer">
                  <input type="text" class="form-control" name="start" id="date-start" placeholder="起始日期">
                  <span class="input-group-addon">到</span>
                  <input type="text" class="form-control" name="end" id="date-end" placeholder="结束日期">
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <form class="form-horizontal">

            <div class="form-group ">
              <label for="sceneModel_title" class="col-md-3 control-label">可选类目</label>

              <div class="col-md-9">
                <select class="form-control selectpicker" id="chooseTitle" multiple>
                  <option name="s" value="0"  truename="date" selected>月份</option>
                  <option name="s" value="1"  truename="name" selected>类型</option>
                  <option name="s" value="2"  truename="DAU" selected>DAU</option>
                  <option name="s" value="3"  truename="install" selected>总新增</option>
                  <option name="s" value="4"  truename="new_user" selected>总新客</option>
                  <option name="s" value="5"  truename="naturl_install" selected>自然量</option>
                  <option name="s" value="6"  truename="fee_install" selected>付费新增</option>
                  <option name="s" value="7"  truename="fee" selected>花费</option>
                  <option name="s" value="8"  truename="fee_new_user" selected>付费新客</option>
                  <option name="s" value="9"  truename="fee_cost" selected>付费新增成本</option>
                  <option name="s" value="10" truename="p_user" selected>付费新客成本</option>
                  <option name="s" value="11" truename="new_fee" selected>总新客付款金额</option>
                  <option name="s" value="12" truename="feeuser_fee" selected>付费新客付款金额</option>
                  <option name="s" value="13" truename="user_roi" selected>新客ROI</option>
                  <option name="s" value="14" truename="install_roi" selected>付费新客ROI</option>
                  <option name="s" value="15" truename="pay_order" selected>订单数</option>
                  <option name="s" value="16" truename="pay_perprice" selected>总客单价</option>
                  <option name="s" value="17" truename="fee_install">月新增</option>
                  <option name="s" value="18" truename="fee_new_user">月新客</option>
                  <option name="s" value="19" truename="d_install">日均新增</option>
                  <option name="s" value="20" truename="d_user">日均新客</option>
                  <option name="s" value="21" truename="fee_cost">单用户新增成本</option>
                  <option name="s" value="22" truename="p_user">单用户新客成本</option>
                  <option name="s" value="23" truename="m_fee">月推广费</option>
                </select>
              </div>

            </div>
          </form>
        </div>
        <div class="col-md-2"><a href="javascript:;" class="btn btn-primary btn-block btn-search">查询</a></div>
      </div>
    </div>
  </div>
  <!-- end panel -->
  <!-- begin table -->
  <div class="table-responsive table-bg-white">
    <div class="download-group">
      <button id="download" type="button" class="btn btn-default"><i class="fa fa-download"></i> CVS</button>
    </div>
    <!--    <div class="dataTables_length" id="data-table_length" style="margin: 15px 0 0 15px;">-->
    <!--      <label>每页显示-->
    <!--        <select id="page-item-count" name="data-table_length" aria-controls="data-table" class="">-->
    <!--          <option value="10">10</option>-->
    <!--          <option value="50">50</option>-->
    <!--          <option value="100">100</option>-->
    <!--          <option value="200">200</option>-->
    <!--        </select>条记录-->
    <!--      </label>-->
    <!--    </div>-->
    <div>
      <input id=sort_name type="hidden" value="0">
    </div>
    <table class="table table-striped dataTable no-footer" role="grid" aria-describedby="data-table_info" id="tab"> 
      <thead id="thd">
      <tr>
        <th class="sorting" id="sortdate" value="0">月份</th>
        <th class="sorting" id="sortdate" value="1">类型</th>
        <th class="sorting" id="sortdate" value="2">DAU</th>
        <th class="sorting" id="sortinstall" value="3">总新增</th>
        <th class="sorting" id="sortnew_user" value="4">总新客</th>
        <th class="sorting" id="sortnaturl_install" value="5">自然量</th>
        <th class="sorting" id="sortfee_install" value="6">付费新增</th>
        <th class="sorting" id="sort" value="7">花费</th>
        <th class="sorting" id="sortfee_new_user" value="8">付费新客</th>
        <th class="sorting" id="sortfee_cost" value="9">付费新增成本</th>
        <th class="sorting" id="sortfee_new_user_cost" value="10">付费新客成本</th>
        <th class="sorting" id="sortnew_fee" value="11">总新客付款金额</th>
        <th class="sorting" id="sortfeeuser_fee" value="12">付费新客付款金额</th>
        <th class="sorting" id="sortuser_roi" value="13">新客ROI</th>
        <th class="sorting" id="sortinstall_roi" value="14">付费新客ROI</th>
        <th class="sorting" id="sortpay_order" value="15">订单数</th>
        <th class="sorting" id="sortpay_perprice" value="16">总客单价</th>
        <th class="sorting" id="sortm_install" value="17">月新增</th>
        <th class="sorting" id="sortm_user" value="18">月新客</th>
        <th class="sorting" id="sortd_install" value="19">日均新增</th>
        <th class="sorting" id="sortd_user" value="20">日均新客</th>
        <th class="sorting" id="sortp_install" value="21">单用户新增成本</th>
        <th class="sorting" id="sortp_user" value="22">单用户新客成本</th>
        <th class="sorting" id="sortm_fee" value="23">月推广费</th>
      </tr>
      <!-- 汇总 行 开始   -->
      <!--      <tr id="tr2">-->
      <!--        <th>汇总</th>-->
      <!--        <th>null</th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th>null</th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th>null</th>-->
      <!--        <th></th>-->
      <!--        <th></th>-->
      <!--        <th>null</th>-->
      <!--        <th>null</th>-->
      <!--      </tr>-->
      <!-- 汇总 行 结束   -->
      </thead>
      <tbody id="monthlyreport-item-list">
      <tr id="monthlyreport-item-template" style="display:none">
        <td value="0"></td>
        <td value="1"></td>
        <td value="2"></td>
        <td value="3"></td>
        <td value="4"></td>
        <td value="5"></td>
        <td value="6"></td>
        <td value="7"></td>
        <td value="8"></td>
        <td value="9"></td>
        <td value="10"></td>
        <td value="11"></td>
        <td value="12"></td>
        <td value="13"></td>
        <td value="14"></td>
        <td value="15"></td>
        <td value="16"></td>
        <td value="17"></td>
        <td value="18"></td>
        <td value="19"></td>
        <td value="20"></td>
        <td value="21"></td>
        <td value="22"></td>
        <td value="23"></td>
      </tr>

      </tbody>
    </table>
    <div class="dataTables_info" id="data-table_info" style="margin:0 0 15px 15px; display: none;" role="status"
         aria-live="polite">显示<span>57</span>条数据中的第<span>1</span>到<span>10</span>条
    </div>
    <div class="dataTables_paginate paging_simple_numbers" id="data-table_paginate" style="display: none;">
      <a class="paginate_button previous disabled" aria-controls="data-table" data-dt-idx="0" tabindex="0"
         id="data-table_previous">Previous</a>
      <span>
        <a id="page-btn-template" style="display:none;" class="paginate_button" aria-controls="data-table"
           data-dt-idx="0" tabindex="0">0</a>
      </span>
      <a class="paginate_button next" aria-controls="data-table" data-dt-idx="7" tabindex="0"
         id="data-table_next">Next</a>
      <input id="result-rows" type="hidden" value=""/>
      <input id="result-count-per-page" type="hidden" value=""/>
      <input id="result-table" type="hidden" value=""/>
    </div>
  </div>
  <!-- end table -->
</div>
<!-- end #content -->



