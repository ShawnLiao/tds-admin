<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>Color Admin | Login Page</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />

	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="static/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="static/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
	<link href="static/assets/css/animate.min.css" rel="stylesheet" />
	<link href="static/assets/css/style.min.css" rel="stylesheet" />
	<link href="static/assets/css/style-responsive.min.css" rel="stylesheet" />
	<link href="static/assets/css/theme/default.css" rel="stylesheet" id="theme" />
	<!-- ================== END BASE CSS STYLE ================== -->
</head>
<body>
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade in"><span class="spinner"></span></div>
	<!-- end #page-loader -->

	<!-- begin #page-container -->
	<div id="page-container" class="fade" id="login-views">
	    <!-- begin login -->
        <div class="login bg-black animated fadeInDown">
            <!-- begin brand -->
            <div class="login-header">
                <div class="brand">
                    <span class="logo"></span> Adam数据平台
                    <small>数据驱动发展，欢迎来到Adam数据平台！</small>
                </div>
                <div class="icon">
                    <i class="fa fa-sign-in"></i>
                </div>
            </div>
            <!-- end brand -->
            <div class="login-content">
                    <div class="form-group m-b-20">
                        <input type="text" id="username" class="form-control input-lg" style="font-size: 16px;" placeholder="电子邮箱地址" />
                    </div>
                    <div class="form-group m-b-20">
                        <input type="password" id="password" class="form-control input-lg" style="font-size: 16px;" placeholder="密码" />
                    </div>
                    <div class="checkbox m-b-20">
                        <label>
                            <input type="checkbox" id="rememberMe"checked="checked"/>记住密码
                        </label>
                    </div>
                    <div class="login-buttons">
                        <button id="btn-login" type="submit" class="btn btn-success btn-block btn-lg">登  录</button>
                    </div>
            </div>
        </div>
        <!-- end login -->
</div>
     <div  style="margin-left:300px;margin-top:00px;width:1500px;height:20px;" id="apply-method">
     <h3 style="text-indent:2em">申请流程如下:</h3>
     <h4 style="text-indent:4em">1.邮件格式(下面是一个实例)</h4>
     <h5 style="text-indent:8em">权限申请：申请Adam数据平台权限</h5>
     <h5 style="text-indent:8em">申请理由：统计每日数据</h5>
     <h5 style="text-indent:8em">申请人员：申请人员：技术中心运维组李德祥(lidx@culiu.org)</h5>
     <h5 style="text-indent:8em">发邮件给姜忠涛(jiangzt@chuchujie.com)、自己部门的负责人(如运维组负责人：朱俊杰)</h5>
 </div>
    
        <!-- begin theme-panel -->
        <div class="theme-panel">
            <a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>
            <div class="theme-panel-content">
                <h5 class="m-t-0">Color Theme</h5>
                <ul class="theme-list clearfix">
                    <li class="active"><a href="javascript:;" class="bg-green" data-theme="default" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Default">&nbsp;</a></li>
                    <li><a href="javascript:;" class="bg-red" data-theme="red" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red">&nbsp;</a></li>
                    <li><a href="javascript:;" class="bg-blue" data-theme="blue" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Blue">&nbsp;</a></li>
                    <li><a href="javascript:;" class="bg-purple" data-theme="purple" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Purple">&nbsp;</a></li>
                    <li><a href="javascript:;" class="bg-orange" data-theme="orange" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Orange">&nbsp;</a></li>
                    <li><a href="javascript:;" class="bg-black" data-theme="black" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Black">&nbsp;</a></li>
                </ul>
                <div class="divider"></div>
                <div class="row m-t-10">
                    <div class="col-md-5 control-label double-line">Header Styling</div>
                    <div class="col-md-7">
                        <select name="header-styling" class="form-control input-sm">
                            <option value="1">default</option>
                            <option value="2">inverse</option>
                        </select>
                    </div>
                </div>
                <div class="row m-t-10">
                    <div class="col-md-5 control-label">Header</div>
                    <div class="col-md-7">
                        <select name="header-fixed" class="form-control input-sm">
                            <option value="1">fixed</option>
                            <option value="2">default</option>
                        </select>
                    </div>
                </div>
                <div class="row m-t-10">
                    <div class="col-md-5 control-label double-line">Sidebar Styling</div>
                    <div class="col-md-7">
                        <select name="sidebar-styling" class="form-control input-sm">
                            <option value="1">default</option>
                            <option value="2">grid</option>
                        </select>
                    </div>
                </div>
                <div class="row m-t-10">
                    <div class="col-md-5 control-label">Sidebar</div>
                    <div class="col-md-7">
                        <select name="sidebar-fixed" class="form-control input-sm">
                            <option value="1">fixed</option>
                            <option value="2">default</option>
                        </select>
                    </div>
                </div>
                <div class="row m-t-10">
                    <div class="col-md-5 control-label double-line">Sidebar Gradient</div>
                    <div class="col-md-7">
                        <select name="content-gradient" class="form-control input-sm">
                            <option value="1">disabled</option>
                            <option value="2">enabled</option>
                        </select>
                    </div>
                </div>
                <div class="row m-t-10">
                    <div class="col-md-5 control-label double-line">Content Styling</div>
                    <div class="col-md-7">
                        <select name="content-styling" class="form-control input-sm">
                            <option value="1">default</option>
                            <option value="2">black</option>
                        </select>
                    </div>
                </div>
                <div class="row m-t-10">
                    <div class="col-md-12">
                        <a href="#" class="btn btn-inverse btn-block btn-sm" data-click="reset-local-storage"><i class="fa fa-refresh m-r-3"></i> Reset Local Storage</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- end theme-panel -->
	</div>
	<!-- end page container -->

	<!-- ================== BEGIN BASE JS ================== -->
	<script src="static/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="static/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="static/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="static/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
		<script src="static/assets/crossbrowserjs/html5shiv.js"></script>
		<script src="static/assets/crossbrowserjs/respond.min.js"></script>
		<script src="static/assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="static/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="static/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->

	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="static/assets/js/apps.min.js"></script>
	<script src="static/js/login.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->

	<script>
		$(document).ready(function() {
			App.init();
		});
	</script>
</body>
</html>
