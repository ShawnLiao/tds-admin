<!-- begin #sidebar -->
<div id="sidebar" class="sidebar">
  <!-- begin sidebar scrollbar -->
  <div data-scrollbar="true" data-height="100%">
    <!-- begin sidebar nav -->
    <ul id="side-bar-nav" class="nav">
      <li class="nav-header">功能导航</li>
      <li class="has-sub">
        <a href="javascript:;">
          <b class="caret pull-right"></b>
          <i class="fa fa-laptop"></i>
          <span>奋斗吧！领主大人 <span class="label label-theme m-l-5"></span></span>
        </a>
        <ul class="sub-menu">
          <li><a href="/gaea/form/gameServer">服务器监控</a></li>
          <li><a href="/gaea/form/serverDomain">服务器域名管理</a></li>
          <li><a href="/gaea/form/gameDataAdmin">游戏数据管理</a></li>
          <li><a href="/gaea/form/gameData">游戏数据查询</a></li>
        </ul>
      </li>

      <!-- end sidebar minify button -->
    </ul>
    <!-- end sidebar nav -->
  </div>
  <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>
<!-- end #sidebar -->
