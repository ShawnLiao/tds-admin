<script>

var setName = function() {
  $(".user-name").text("<?php echo 'hello' ?>");
}

var highlightSideBarNav = function() {
  var item = $("#side-bar-nav").children().eq(<?php echo $level_one_index ?>).addClass("active");
  if (item.hasClass("has-sub")) {
    item.children(".sub-menu").children().eq(<?php echo $level_two_index ?>).addClass("active");
  }
}

$(document).ready(function() {
  setName();

  highlightSideBarNav();

  App.init();
  FormPlugins.init();
});

</script>

