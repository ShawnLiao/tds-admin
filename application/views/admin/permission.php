<div id="content" class="content">

  <ol class="breadcrumb pull-right">
    <li><a href="/admin/dashboard/index">管理员首页</a></li>
    <li class="active">权限管理</li>
  </ol>

  <h1 class="page-header">权限管理<small></small></h1>

  <!-- begin panel -->
  <div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
        </div>
        <h4 class="panel-title">选择用户组</h4>
    </div>
    <div class="panel-body">
      <!-- begin row-->
      <div class="row">
        <div class="col-md-4">
          <form class="form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label">选择用户组</label>
              <div class="col-md-9">
                <select class="form-control" id="group" ></select>
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-6">
          <form class="form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label">添加用户组</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="add_group" placeholder="用户组名称">
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-2">
          <a href="javascript:;" class="btn btn-primary btn-block btn-add-group">添加用户组</a>
        </div>
      </div>
      <!-- end row -->
    </div>
  </div>
  <!-- end panel -->
  <!-- begin panel -->
  <div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
        </div>
        <h4 class="panel-title">数据库添加页面</h4>
    </div>
    <div class="panel-body">
      <!-- begin row-->
      <div class="row">
        <div class="col-md-4">
          <form class="form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label">路径</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="path_name" placeholder="路径">
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-6">
          <form class="form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label">页面名称</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="page_name" placeholder="页面名称">
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-2">
          <a href="javascript:;" class="btn btn-primary btn-block btn-add-db-page">添加页面</a>
        </div>
      </div>
      <!-- end row -->
    </div>
  </div>
  <!-- end panel -->
    <!-- begin panel -->
  <div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
        </div>
        <h4 class="panel-title">数据库删除页面</h4>
    </div>
    <div class="panel-body">
      <!-- begin row-->
      <div class="row">
        <div class="col-md-4">
         <form class="form-horizontal">
          <div class="form-group">
           <label class="col-md-3 control-label">页面名称</label>
            <div class="col-md-9">
            <select class="form-control" id="del_page_name" ></select>
            </div>
          </div>
        </form>
      </div>
        <div class="col-md-2">
          <a href="javascript:;" class="btn btn-primary btn-block btn-del-db-page">删除页面</a>
        </div>

      </div>
      <!-- end row -->
    </div>
  </div>
  <!-- end panel -->
     <!-- begin panel -->
   <div class="panel panel-inverse">
     <div class="panel-heading">
         <div class="panel-heading-btn">
         </div>
         <h4 class="panel-title">添加财务权限</h4>
     </div>
     <div class="panel-body">
       <!-- begin row-->
       <div class="row">
         <div class="col-md-4">
          <form class="form-horizontal">
           <div class="form-group">
            <label class="col-md-3 control-label">用户姓名</label>
             <div class="col-md-9">
               <input type="text" class="form-control" id="add_finance_name" placeholder="姓名">
             </div>
           </div>
         </form>
       </div>
       <div class="col-md-4">
         <form class="form-horizontal">
           <div class="form-group">
             <label class="col-md-3 control-label">邮箱</label>
               <div class="col-md-9">
                 <input type="text" class="form-control" id="add_finance_email" placeholder="工作邮箱">
               </div>
             </div>
           </form>
         </div>

         <div class="col-md-2">
           <a href="javascript:;" class="btn btn-primary btn-block btn-add-db-finance">添加财务权限</a>
         </div>
 
       </div>
       <!-- end row -->
     </div>
   </div>
   <!-- end panel -->

  <div class="row">
    <div class="col-md-6 ui-sortable">
      <!-- begin panel -->
      <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
            </div>
            <h4 class="panel-title">用户组添加用户</h4>
        </div>

        <div class="panel-body">
          <!-- begin row-->
          <div class="row">
            <div class="col-md-8">
              <form class="form-horizontal">
                <div class="form-group">
                  <label class="col-md-3 control-label">姓名</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" id="name" placeholder="姓名">
                  </div>
                </div>
              </form>
            </div>
            <div class="col-md-4">
              <a href="javascript:;" class="btn btn-primary btn-block btn-add-user">添加用户</a>
            </div>
          </div>
          <!-- end row -->
        </div>
      </div>
    </div>
    <div class="col-md-6 ui-sortable">
      <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
            </div>
            <h4 class="panel-title">用户组添加页面</h4>
        </div>
        <div class="panel-body">
          <!-- begin row-->
          <div class="row">
            <div class="col-md-8">
              <form class="form-horizontal">
                <div class="form-group">
                  <label class="col-md-3 control-label">页面名称</label>
                  <div class="col-md-9">
                    <select class="form-control" id="page" ></select>
                  </div>
                </div>
              </form>
            </div>
            <div class="col-md-4">
              <a href="javascript:;" class="btn btn-primary btn-block btn-add-page">添加页面</a>
            </div>
          </div>
          <!-- end row -->
        </div>
      </div>
      <!-- end panel -->
    </div>
  </div>

  <div class="row">
    <div class="col-md-6 ui-sortable">
      <!-- begin table -->
      <div class="table-responsive table-bg-white">
        <table class="table">
          <thead>
            <tr>
              <th>用户ID</th>
              <th>邮箱</th>
              <th>姓名</th>
              <th>操作</th>
            </tr>
          </thead>
          <tbody id="user-list">
            <tr id="user-template" style="display:none">
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- end table -->
    </div>
    <div class="col-md-6 ui-sortable">
      <!-- begin table -->
      <div class="table-responsive table-bg-white">
        <table class="table">
          <thead>
            <tr>
              <th>页面ID</th>
              <th>页面名称</th>
              <th>页面标识</th>
              <th>操作</th>
            </tr>
          </thead>
          <tbody id="page-list">
            <tr id="page-template" style="display:none">
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- end table -->
    </div>
  </div>
</div>
