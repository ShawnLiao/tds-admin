<div id="content" class="content">

  <ol class="breadcrumb pull-right">
    <li><a href="/admin/dashboard/index">管理员首页</a></li>
    <li class="active">用户权限查看</li>
  </ol>

  <h1 class="page-header">用户权限查看<small></small></h1>

  <!-- begin panel -->
  <div class="panel panel-inverse" data-sortable-id="form-stuff-5">
    <div class="panel-heading">
        <div class="panel-heading-btn">
        </div>
        <h4 class="panel-title">查看用户权限</h4>
    </div>
    <div class="panel-body">
      <!-- begin row-->
      <div class="row">
        <div class="col-md-4">
          <form class="form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label">姓名</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="name_search" placeholder="用户的真实姓名">
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-2"><a href="javascript:;" class="btn btn-primary btn-block btn-user-search">权限查看</a></div>
      </div>
 </div>
   <!-- end row -->
</div>
  <!-- begin table -->
  <div class="table-responsive table-bg-white">
    <div>
      <input id=sort_name type="hidden" value="0">
    </div>
    <table class="table table-striped dataTable no-footer" role="grid" aria-describedby="data-table_info">
      <thead id="thd">
      <tr>
        <th id="sort">用户ID</th>
        <th id="sortctime">邮箱</th>
        <th id="sortphone">姓名</th>
        <th id="sortorder_sn">用户组名称</th>
        <th id="sortpay_fee">页卡</th>
        <th id="sortip_address">权限名称</th>
        <th id="sorttotal_fee">是否有财务权限</th>
      </tr>
      </thead>
      <tbody id="item-list1">
      <tr id="item-template1" style="display:none">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
       </tbody>
     </table>
     <div class="dataTables_info" id="data-table_info" style="margin:0 0 15px 15px; display: none;" role="status"
          aria-live="polite">显示<span>57</span>条数据中的第<span>1</span>到<span>10</span>条
     </div>
       <input id="result-table" type="hidden" value=""/>
     </div>
   </div>
   <!-- end table -->
</div>
