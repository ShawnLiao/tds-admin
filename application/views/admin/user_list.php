<div id="content" class="content">

  <ol class="breadcrumb pull-right">
    <li><a href="/admin/dashboard/index">管理员首页</a></li>
    <li class="active">用户列表</li>
  </ol>

  <h1 class="page-header">用户列表<small></small></h1>

  <!-- begin panel -->
  <div class="panel panel-inverse" data-sortable-id="form-stuff-5">
    <div class="panel-heading">
        <div class="panel-heading-btn">
        </div>
        <h4 class="panel-title">添加用户</h4>
    </div>
    <div class="panel-body">
      <!-- begin row-->
      <div class="row">
        <div class="col-md-4">
          <form class="form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label">邮箱</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="email" placeholder="用户的工作邮箱">
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <form class="form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label">姓名</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="name" placeholder="用户的真实姓名">
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <form class="form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label">管理员权限</label>
              <div class="col-md-9">
                <select class="form-control" id="is-admin" >
                  <option value="0" selected>非管理员</option>
                  <option value="1">管理员</option>
                </select>
              </div>
            </div>
          </form>
        </div>
      </div>
  <a href="javascript:;" class="btn btn-primary btn-block btn-add" style="margin-bottom: 20px">添加</a>
 </div>
   <!-- end row -->
</div>
<!-- end panel -->
  <!-- begin panel -->
  <div class="panel panel-inverse" data-sortable-id="form-stuff-5">
    <div class="panel-heading">
      <div class="panel-heading-btn">
      </div>
      <h4 class="panel-title">取消用户</h4>
    </div>
    <div class="panel-body">
      <!-- begin row-->
      <div class="row">
        <div class="col-md-4">
          <form class="form-horizontal">
            <div id="template-id" class="form-group">
              <label class="col-sm-3 control-label">邮箱</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="email_del" placeholder="用户的工作邮箱">
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <form class="form-horizontal">
            <div id="template-id" class="form-group">
              <label class="col-sm-3 control-label">姓名</label>
                <div class="col-sm-9">
                 <input type="text" class="form-control" id="name_del" placeholder="姓名">
                </div>
            </div>
          </form>
        </div>
        <div class="col-md-2"><a href="javascript:;" class="btn btn-primary btn-block btn-cancel">取消用户</a></div>
      </div>

    </div>

    <!-- end row -->

  </div>
  <!-- end panel -->
  <!-- begin panel -->
  <div class="panel panel-inverse" data-sortable-id="form-stuff-5">
    <div class="panel-heading">
      <div class="panel-heading-btn">
      </div>
      <h4 class="panel-title">密码重置</h4>
    </div>
    <div class="panel-body">
      <!-- begin row-->
      <div class="row">
        <div class="col-md-4">
          <form class="form-horizontal">
            <div id="template-id" class="form-group">
              <label class="col-sm-3 control-label">邮箱</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="email_reset" placeholder="用户的工作邮箱">
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-4">
          <form class="form-horizontal">
            <div id="template-id" class="form-group">
              <label class="col-sm-3 control-label">姓名</label>
                <div class="col-sm-9">
                 <input type="text" class="form-control" id="name_reset" placeholder="姓名">
                </div>
            </div>
          </form>
        </div>
        <div class="col-md-2"><a href="javascript:;" class="btn btn-primary btn-block btn-reset">密码重置</a></div>
      </div>

    </div>

    <!-- end row -->

  </div>
  <!-- end panel -->


  <!-- begin table -->
  <div class="table-responsive table-bg-white">
    <table class="table">
      <thead>
        <tr>
          <th>用户ID</th>
          <th>邮箱</th>
          <th>姓名</th>
          <th>是否管理员</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody id="item-list">
        <tr id="item-template" style="display:none">
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </tbody>
    </table>
  </div>
  <!-- end table -->
</div>
