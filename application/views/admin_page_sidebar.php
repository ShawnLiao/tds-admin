<!-- begin #sidebar -->
<div id="sidebar" class="sidebar">
  <!-- begin sidebar scrollbar -->
  <div data-scrollbar="true" data-height="100%">
    <!-- begin sidebar user -->
    <ul class="nav">
      <li class="nav-profile">
        <div class="image">
          <a href="javascript:;"><img src="<?php echo URL::site('',true,false);?>/static/assets/img/user-13.jpg" alt="" /></a>
        </div>
        <div class="info">
          <span class="user-name">未知</span>
          <small>你还没填写自我介绍哦~</small>
        </div>
      </li>
    </ul>
    <!-- end sidebar user -->
    <!-- begin sidebar nav -->
    <ul id="side-bar-nav" class="nav">
      <li class="nav-header">功能导航</li>
      <li>
        <a href="/admin/dashboard/index">
          <i class="fa fa-laptop"></i>
          <span>管理员首页</span>
        </a>
      </li>
      <li class="has-sub">
        <a href="javascript:;">
            <b class="caret pull-right"></b>
            <i class="fa fa-user"></i>
            <span>用户管理</span> 
        </a>
        <ul class="sub-menu">
          <li><a href="/admin/user/list">用户列表</a></li>
          <li><a href="/admin/user/permission">权限管理</a></li>
          <li><a href="/admin/user/permissionsearch">用户权限查看</a></li>
        </ul>
      </li>
      <li>
        <a href="/">
          <i class="fa fa-reply"></i>
          <span>网站首页</span>
        </a>
      </li>
      <!-- begin sidebar minify button -->
      <li>
        <a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify">
          <i class="fa fa-angle-double-left"></i>
        </a>
      </li>
      <!-- end sidebar minify button -->
    </ul>
    <!-- end sidebar nav -->
  </div>
  <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>
<!-- end #sidebar -->


