<?php
defined('SYSPATH') or die('No direct script access.');

class Api_Schedule
{
		/**
			* 查看全部列表
			*
			* @param array $query = array('status'=> '', 'user_id'=>'', 'per_page'=>,'type'=>,
			*                     'page'=>,'begin_date'=>,'end_date'=>,'gender'=>,'ad_key'=>,'zone'=>,'ad_material_id'=>)
			*                     包含ad_material_id 时 只查找属于$type单条记录
			*
			* @param bool  $flag
			*
			* @throws Exception
			* @internal param array $query
			*
			* @return array
			*/
		public static function getList($query, $flag=false)
		{
				if( !($type = Api_Ad_Material::getType($query)) )
						throw new Exception('类型不合法');
				$page_count = Arr::get($query, 'page_item_count',10);
				$page = Arr::get($query, 'page',1);	
				//字段排序规则
				$field_sort = array('chuchuId'=>'amb','title'=>'amb','newPrice'=>'amb','chuchuShopId'=>'amb','ad_material_id'=>'xp','ad_key'=>'xp','gender'=>'xp','id'=>'xp','sort'=>'xp','sort_weight'=>'xp','sortstatus'=>'xp','custom_sort'=>'xps');

				if($type == 'product') {
						$sql_query = DB::select('xp.id','xp.module','xp.ad_material_id','xp.gender','xp.author','xp.ad_key','xp.zone','xp.sort','xp.sort_weight','xp.begin_date','xp.end_date','xp.status','amb.chuchuId','amb.title','amb.chuchuShopId','amb.imgUrl','amb.newPrice','xp.ad_material_id','xp.ad_key','xp.gender','xps.custom_sort')->from(array('ad_material_products', 'amb'))
								->join(array('xb_policies','xp'), 'RIGHT')->on('amb.ad_material_id', '=', 'xp.ad_material_id')
								->join(array('xb_product_spreads', 'xps'),'RIGHT')->on('xp.id','=','xps.policy_id');

				}else if ($type == 'banner') {
                                                  if($flags = Arr::get($query, 'flags',0)){
							  $sql_query = DB::select('xp.id','xp.module','xp.package_name','tst.ad_slave_material_id','xp.gender','xp.author','xp.ad_key','xp.zone','xp.sort','xp.sort_weight','xp.begin_date','xp.end_date','xp.status','amb.title','amb.imgUrl','amb.template','amb.query','xp.ad_key','xp.gender')->from(array('ad_material_banners', 'amb'))
                                                                        ->join(array('xb_policies','xp'), 'RIGHT')->on('amb.ad_material_id', '=', 'xp.ad_material_id')->join(array('ad_material_test_relations','tst'),'RIGHT')->on('amb.ad_material_id', '=', 'tst.ad_master_material_id');
                                                  }
                                                  else{
						  $sql_query = DB::select('xp.id','xp.module','xp.package_name','xp.ad_material_id','xp.gender','xp.author','xp.ad_key','xp.zone','xp.sort','xp.sort_weight','xp.begin_date','xp.end_date','xp.status','amb.title','amb.imgUrl','amb.template','amb.query','xp.ad_key','xp.gender')->from(array('ad_material_banners', 'amb'))
		         						->join(array('xb_policies','xp'), 'RIGHT')->on('amb.ad_material_id', '=', 'xp.ad_material_id'); 
                                                  }
                                                  if( $title = Arr::get($query, 'title', null) ) {
                                                    $sql_query->where('amb.title','=',$title);
                                                  }
                                                  if( $not_title = Arr::get($query, 'not_title',null)){
                                                    $sql_query->where('amb.title','!=',$not_title);
                                                  }
                                                  if (is_array($query['selectpack'])) {
                                                    $sql_query->where('xp.package_name',' in ',$query['selectpack']);
                                                  }
                                                  if($title_name = Arr::get($query, 'title_name','')){
                                                    $sql_query->where('amb.title',' like ',$title_name."%");
                                                  }
                                                  
				}else if ($type == 'brand'){
						$sql_query = DB::select('xp.id','xp.ad_material_id','xp.begin_date','xp.end_date','admb.title','admb.template','admb.query','admb.imgUrl','xp.ad_material_id','xp.ad_key','xp.gender','xps.custom_sort')->from(array('ad_material_brands', 'admb'))
								->join(array('xb_policies','xp'), 'RIGHT')->on('amb.ad_material_id', '=', 'xp.ad_material_id');
				}

				if($id=Arr::get($query,'id'))
						$sql_query->where('xp.id','=',$id);
				if( $type = Arr::get($query, 'type') )
						$sql_query->where('xp.type', '=', $type);
				if($ad_key=Arr::get($query,'ad_key',null)){
						if(strpos($ad_key,'%')===false){
								$sql_query->where('xp.ad_key', '=', $ad_key);
						}else{
								$sql_query->where('xp.ad_key', 'like', $ad_key);
						}
				}
				if( $zone_data = Arr::get($query, 'zone', null) )
						$sql_query->where('xp.zone', '=', $zone_data);
				if( $begin_date = Arr::get($query, 'begin_date', null) )
						$sql_query->where('xp.begin_date', '>=', (int)$begin_date);
				if( $begin_date = Arr::get($query, 'begin_date_gt', null) )
						$sql_query->where('xp.begin_date', '>=', (int)$begin_date);
				if( $begin_date = Arr::get($query, 'begin_date_lt', null) )
						$sql_query->where('xp.begin_date', '<=', (int)$begin_date);
				//准备删除
				if( $end_date = Arr::get($query, 'end_date', null) )
						$sql_query->where('xp.end_date', '<=', (int)$end_date);
				if( $end_date = Arr::get($query, 'end_date_gt', null) )
						$sql_query->where('xp.end_date', '>=', (int)$end_date);
				if( $end_date = Arr::get($query, 'end_date_lt', null) )
						$sql_query->where('xp.end_date', '<=', (int)$end_date);
				//单状态查找
				if( $status = Arr::get($query, 'status', null) )
						$sql_query->where('xp.status', '=', $status);
				//多状态查找
				if( $statues = Arr::get($query, 'statues', null) )
						$sql_query->where('xp.status', 'IN', $statues);
				//非状态查找
				if( $status = Arr::get($query, 'is_not_status', null) )
						$sql_query->where('xp.status', '<>', $status);
				if( $modules = Arr::get($query, 'modules', null) ) {
						if(!is_array($modules)){
								if( strpos($modules,'%')===false){
										$sql_query->where('xp.module', '=', $modules);
								}else{
										$sql_query->where('xp.module', 'like', $modules);
								}
						}else{
								$sql_query->where('xp.module', 'IN', $modules);
						}
				}
				if( $author = Arr::get($query, 'author', null) ) {
						$sql_query->where('xp.author', '=', $author);
				}
				if( $gender = Arr::get($query, 'gender', null) ) {
						$sql_query->where('xp.gender', '=', $gender);
				}
				$ad_keys = Arr::get($query, 'ad_keys', array());
				if( !empty($ad_keys) ) {
						$sql_query->where_open();
						foreach( $ad_keys as $ad ) {
								// hotfix: 全站搜索出错, 因为ad_key有数组形式
								// 搜索的SQL会出现这样的内容: `ad_key` LIKE ('zdm-newest-maternal', 'zdm-newest-accessory', 'zdm-newest-outdoor') AND `zone` = 'productList'
								if (is_array($ad['ad_key'])) {
										foreach ($ad['ad_key'] as $key) {
												if(strpos($key,'%')===false){
														$sql_query->or_where('xp.ad_key', '=', $key)->and_where('xp.zone', '=', $ad['zone']);
												}else{
														$sql_query->or_where('xp.ad_key', 'like', $key)->and_where('xp.zone', '=', $ad['zone']);
												}
												//$xp->or_where('ad_key', 'like', $key)->and_where('zone', '=', $ad['zone']);

										}
								} else {
										if(strpos($ad['ad_key'],'%')===false){
												$sql_query->or_where('xp.ad_key', '=', $ad['ad_key'])->and_where('xp.zone', '=', $ad['zone']);
										}else{
												$sql_query->or_where('xp.ad_key', 'like', $ad['ad_key'])->and_where('xp.zone', '=', $ad['zone']);
										}
										//$xp->or_where('ad_key', 'like', $ad['ad_key'])->and_where('zone', '=', $ad['zone']);
								}
						}
						$sql_query->where_close();
				}
				
				if( $ad_material_id = Arr::get($query, 'ad_material_id', null) ) {
						$sql_query->where('xp.ad_material_id', '=', $ad_material_id);
				}
				if( $policy_id = Arr::get($query, 'policy_id', null) ) {
						$sql_query->where('xp.id', '=', $policy_id);
				}
				if( $policy_ids = Arr::get($query, 'policy_ids', null) ) {
						$sql_query->where('xp.id', 'IN', $policy_ids);
				}

				
				if($order=Arr::get($query,'order','id desc')){
				  $order = $sort_rule = explode(' ',$order);
				 /* if(isset($order[0])&& in_array($order[0],$arr_data_sort)){
				    $sort_data_falg = true;
				    $order = ['0'=>'id','1'=>'desc'];
				  }*/
				  if (isset($order[0]) && isset($order[1])) {
				    $sql_query->order_by(Arr::get($field_sort,$order[0],'xp').'.'.$order[0],$order[1]);
				  }
					  	
 				}				
			 $ret['rows'] = count($sql_query->execute('ads')->as_array());
				$ret['page'] = $page;
				$ret['page_item_count'] = $page_count;
				if($flag === false) {
				  $limit = (int)$page_count * $page - $page_count;
				  $sql_query->limit($page_count)->offset($limit);
				}
				$ret['data'] = $sql_query->execute('ads')->as_array();
				return $ret;
		}

		/**
			* 获取 xb_policies 数据
			* @param array || int
			* @return array
			*/
		public static function getPolicie($ad_key) {

				if(empty($ad_key)) {
						return array();
				}

				if (is_array($ad_key)) {
						$ad_key = implode("','", $ad_key);
				}

				$sql_po =  "select `id`,`module`,`ad_material_id`,`gender`,`ad_key`,`zone`, `status` from xb_policies
                 where  ad_key IN ('".$ad_key."')" ;
				$result  = DB::query(Database::SELECT, $sql_po)->execute('ads')->as_array();
				return $result;

		}

		/**
			*  根据ad_key zone type 查询 ad_material_id
			*
			* @param array=(ad_key ,zone, type)
			*
			* @return  array()
			*/
		public static function getProductInfo($Query){

				$sql="SELECT ad_material_id FROM xb_policies WHERE ad_key = :ad_key AND zone = :zone AND type = :type ";
				$params=array();
				if($ad_key=Arr::get($Query,'ad_key')){
						$params[':ad_key']=$ad_key;
				}
				if($zone=Arr::get($Query,'zone')){
						$params[':zone']=$zone;
				}

				if($type=Arr::get($Query,'type')){
						$params[':type']=$type;
				}
				$result = DB::query(Database::SELECT, $sql)->parameters($params)->execute('ads')->as_array();
				return $result;
		}

		/**
			*  根据ad_material_id 获取 ad_material_products 表 中 所有商品
			*/
		public static function  getProductByadid($query)
		{
				$sql_query = DB::select('admp.chuchuId')->from(array('ad_material_products','admp'));
				if (($adid = Arr::get($query,'adid',array())) && is_array($adid)) {
						$sql_query->where('admp.ad_material_id', 'IN', $adid);
				}
				$all_chuchuId = $sql_query->execute('ads')->as_array();
				return $all_chuchuId;
		}



}
