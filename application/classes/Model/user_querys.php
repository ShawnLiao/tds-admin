<?php defined('SYSPATH') or die('No direct script access.');

class Model_UserQuerys extends ORM
{
  protected $_table_name = "user_querys";

  protected $_primary_key = "id";

  protected $_db_group = "adam_user";

  protected $_belongs_to = array('user' => array());
}

