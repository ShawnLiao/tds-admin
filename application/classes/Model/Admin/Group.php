<?php defined('SYSPATH') or die('No direct script access.');
class Model_Admin_Group extends Model_Database
{
  public function accessible($user_id, $page) {
    $sql = "
      SELECT DISTINCT
        C.page
      FROM
        group_user A
      INNER JOIN
        group_page B
      ON A.group_id = B.group_id
      INNER JOIN
        page C
      ON B.page_id = C.id
      WHERE A.user_id = $user_id AND C.page = '$page'";
    $pages = DB::query(Database::SELECT, $sql)->execute("adam_user")->as_array();
    return sizeof($pages) > 0;
  }

  public function add_group($group) {
    $query = DB::insert('group', array('name'))
      ->values(array($group));
    return $query->execute("adam_user");
  }

  public function exist_user($group_id, $user_id) {
    $sql = "SELECT id FROM group_user WHERE group_id = $group_id AND user_id = $user_id";
    return sizeof(DB::query(Database::SELECT, $sql)->execute("adam_user")->as_array()) == 1;
  }

  public function exist_page($group_id, $page_id) {
    $sql = "SELECT id FROM group_page WHERE group_id = $group_id AND page_id = $page_id";
    return sizeof(DB::query(Database::SELECT, $sql)->execute("adam_user")->as_array()) == 1;
  }

  public function add_user($group_id, $user_id) {
    $query = DB::insert('group_user', array('group_id', 'user_id'))
      ->values(array($group_id, $user_id));
    return $query->execute("adam_user");
  }

  public function add_page($group_id, $page_id) {
    $query = DB::insert('group_page', array('group_id', 'page_id'))
      ->values(array($group_id, $page_id));
    return $query->execute("adam_user");
  }

  public function delete_user($group_id, $user_id) {
    $query = DB::delete('group_user')->where('group_id', '=', $group_id)
      ->where('user_id', '=', $user_id);
    return $query->execute("adam_user");
  }

  public function delete_page($group_id, $page_id) {
    $query = DB::delete('group_page')->where('group_id', '=', $group_id)
      ->where('page_id', '=', $page_id);
    return $query->execute("adam_user");
  }

  public function get_group_list() {
    $sql = "SELECT * FROM `group`";
    return DB::query(Database::SELECT, $sql)->execute("adam_user")->as_array();
  }

  public function get_group_user_list_by_id($group_id) {
    $sql = "SELECT u.id, u.email, u.name from group_user g JOIN user u ON u.id = g.user_id WHERE g.group_id = " . $group_id;
    return DB::query(Database::SELECT, $sql)->execute("adam_user")->as_array();
  }

  public function get_group_page_list_by_id($group_id) {
    $sql = "SELECT p.id, p.name, p.page from group_page g JOIN page p ON p.id = g.page_id WHERE g.group_id = " . $group_id;
    return DB::query(Database::SELECT, $sql)->execute("adam_user")->as_array();
  }
}

