<?php defined('SYSPATH') or die('No direct script access.');

class Model_Admin_Userpermissionsearch extends Model_Database
{
  public function user_earch($name) {
    $sql="select distinct t1.id as user_id,t1.email as email,t1.name as name,t3.name as group_name,t5.page as page_name,t5.name as permission_name,t1.is_finance as is_finance
    from 
    (
      select 
        id,
        name,
        email,
        if(is_finance=1,'是','否') as is_finance
      from adam_user.user
      where is_delete=0 and name='".$name."'  
    ) t1
    left join
    (
      select
        user_id,
        group_id
      from adam_user.group_user
    ) t2
    on t1.id=t2.user_id
    left join
    (
      select
        id,
        name
      from adam_user.group
    ) t3
    on t2.group_id=t3.id
    left join
    (
      select
        group_id,
        page_id
      from adam_user.group_page
    ) t4
    on t2.group_id=t4.group_id
    left join 
    (
      select
        id,
        page,
        name
      from adam_user.page
    ) t5
    on t4.page_id=t5.id
    where t3.name is not null and t5.page is not null and t5.name is not null";
    $data = DB::query(Database::SELECT, $sql)->execute("adam_user")->as_array();
    return $data;
  }

}

