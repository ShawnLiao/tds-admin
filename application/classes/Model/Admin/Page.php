<?php defined('SYSPATH') or die('No direct script access.');

class Model_Admin_Page extends Model_Database
{
  public function add_page($page, $name) {
    $query = DB::insert('page', array('page', 'name'))
      ->values(array($page, $name));
    return $query->execute("adam_user");
  }

  public function get_page_list() {
    $sql = "SELECT id, page, name FROM page order by page";
    return DB::query(Database::SELECT, $sql)->execute("adam_user")->as_array();
  }

  public function add_db_page($page_path, $page_name) {
    $sql = "select max(id) as id from page";
    $max_id = DB::query(Database::SELECT,$sql)->execute('adam_user')->as_array();
		$id=$max_id[0]['id']+1;
    $page_sql = "INSERT INTO page(id,page,name) VALUES("
    .$id.",'".$page_path."','".$page_name."');";
    return DB::query(Database::INSERT,$page_sql)->execute('adam_user');
  }
  
  public function del_db_page($page_id) {
		$sql = DB::delete('page')->where('id', '=', $page_id)
		->where('id', '=', $page_id);
		return $sql->execute("adam_user");
  }
  
  public function add_db_finance($name,$email) {
    $data_sql="select * from user where email='".$email."' and name='".$name."'";
    $data=DB::query(Database::SELECT,$data_sql)->execute('adam_user');
    if (count($data)>0) {
      $user_sql = "UPDATE user SET is_finance=1 where email='".$email."' and name='".$name."'";
      DB::query(Database::INSERT,$user_sql)->execute('adam_user');
    }
    
  }

}

