<?php defined('SYSPATH') or die('No direct script access.');

class Model_Realtime_Order extends Model_Database
{
  public function get_realtime_data($date) {
    $sql = "SELECT * FROM cc_rpt_gmv WHERE date = '" . $date . "' order by beg_ts";
    return $this->_db->query(Database::SELECT, $sql)->as_array();
  }

  public function get_realtime_not_data($date) {
    $sql = "SELECT * FROM cc_rpt_gmv_without WHERE date = '" . $date . "' order by beg_ts";
    return $this->_db->query(Database::SELECT, $sql)->as_array();
  }
}

