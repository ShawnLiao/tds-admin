<?php defined('SYSPATH') or die('No direct script access.');

class Model_Repeat_Stat extends Model_Database
{
  public function get_repeat_stat_all($days) {
    $sql = "SELECT * FROM cc_stat_repeat_purchase_rate_" . $days . "d ORDER BY date desc limit 30";
    return $this->_db->query(Database::SELECT, $sql)->as_array();
  }

  public function get_repeat_stat_new($days) {
    $sql = "SELECT * FROM cc_stat_repeat_purchase_rate_new_user WHERE days = " . $days . " ORDER BY date desc limit 30";
    return $this->_db->query(Database::SELECT, $sql)->as_array();
  }

  public function get_repeat_stat_theme($days) {
    $sql="select  
            later_day_count as days,
            begin_date as date,
            from_unixtime(unix_timestamp(begin_date)+later_day_count*86400,'%Y%m%d') as repeat_date,
            if(repurchase_count=0,0,repurchase_count/(select repurchase_count from cc_rpt_theme_brand_repurchase where begin_date=a.begin_date and later_day_count=0)) as rpr
          from cc_rpt_theme_brand_repurchase a 
          where later_day_count<>0 and later_day_count=".$days . " ORDER BY begin_date desc limit 30";
    return $this->_db->query(Database::SELECT, $sql)->as_array();
  }
  
  public function get_repeat_stat_noted($days) {
    $sql="select  
            later_day_count as days,
            begin_date as date,
            from_unixtime(unix_timestamp(begin_date)+later_day_count*86400,'%Y%m%d') as repeat_date,
            if(repurchase_count=0,0,repurchase_count/(select repurchase_count from cc_rpt_noted_brand_repurchase where begin_date=a.begin_date and later_day_count=0)) as rpr
          from cc_rpt_noted_brand_repurchase a 
          where later_day_count<>0 and later_day_count=".$days . " ORDER BY begin_date desc limit 30";
    return $this->_db->query(Database::SELECT, $sql)->as_array();
  }

  public function get_repeat_stat_single($days) {
    $sql="select  
            later_day_count as days,
            begin_date as date,
            from_unixtime(unix_timestamp(begin_date)+later_day_count*86400,'%Y%m%d') as repeat_date,
            if(repurchase_count=0,0,repurchase_count/(select repurchase_count from cc_rpt_single_sell_repurchase where begin_date=a.begin_date and later_day_count=0)) as rpr
          from cc_rpt_single_sell_repurchase a 
          where later_day_count<>0 and later_day_count=".$days . " ORDER BY begin_date desc limit 30";
    return $this->_db->query(Database::SELECT, $sql)->as_array();
  }
  
  public function get_repeat_stat_99repurchase($days,$start,$end) {
    $sql="select  
            later_day_count as days,
            begin_date as date,
            from_unixtime(unix_timestamp(begin_date)+later_day_count*86400,'%Y%m%d') as repeat_date,
            repurchase_count as rpt_count,
            (select repurchase_count from cc_rpt_99_repurchase where begin_date=a.begin_date and later_day_count=0) as ori_count
          from cc_rpt_99_repurchase a 
          where later_day_count<>0 and later_day_count=".$days . " and from_unixtime(unix_timestamp(begin_date)+later_day_count*86400,'%Y%m%d') >= ".$start." and from_unixtime(unix_timestamp(begin_date)+later_day_count*86400,'%Y%m%d') <= ".$end." ORDER BY begin_date desc";
    return $this->_db->query(Database::SELECT, $sql)->as_array();
  }
  
  public function get_repeat_stat_snapuprepurchase($days,$start,$end) {
    $sql="select  
            later_day_count as days,
            begin_date as date,
            from_unixtime(unix_timestamp(begin_date)+later_day_count*86400,'%Y%m%d') as repeat_date,
            repurchase_count as rpt_count,
            (select repurchase_count from cc_rpt_snapup_repurchase where begin_date=a.begin_date and later_day_count=0) as ori_count
          from cc_rpt_snapup_repurchase a 
          where later_day_count<>0 and later_day_count=".$days . " and from_unixtime(unix_timestamp(begin_date)+later_day_count*86400,'%Y%m%d') >= ".$start." and from_unixtime(unix_timestamp(begin_date)+later_day_count*86400,'%Y%m%d') <= ".$end." ORDER BY begin_date desc";
    return $this->_db->query(Database::SELECT, $sql)->as_array();
  }

}


