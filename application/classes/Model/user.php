<?php defined('SYSPATH') or die('No direct script access.');

class Model_User extends ORM
{
  protected $_table_name = "user";

  protected $_primary_key = "id";

  protected $_db_group = "adam_user";

  protected $_has_one = array('user_profile' => array());
}
