<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Sessions_Sessioninfo extends Controller
{
    private $user_select = array(
        array("col" => "user_id", "table" => "", "as" => "user_id"),
        array("col" => "nick_name", "table" => "", "as" => "nick_name"),
        array("col" => "ctime", "table" => "", "as" => "ctime"),
        array("col" => "phone_number", "table" => "", "as" => "phone_number"),
        array("col" => "job", "table" => "", "as" => "job"),
        array("col" => "gender", "table" => "", "as" => "gender"),
        array("col" => "isshangjia", "table" => "", "as" => "isshangjia")
    );

    private $action_select = array(
        array("col" => "timestamp", "table" => "", "as" => "timestamps"),
        array("col" => "uid", "table" => "", "as" => "uid"),
        array("col" => "deviceid", "table" => "", "as" => "deviceid"),
        array("col" => "action", "table" => "", "as" => "action"),
        array("col" => "content", "table" => "", "as" => "content"),
        array("col" => "phone_number", "table" => "", "as" => "phone_number")
    );


    private function build_user_info_sql($query_options)
    {
        $sb = new SqlBuilder;
        $table = "usertableinfo";
        $where_cond = array();
        if ($this->has_user_id_option($query_options)) {
            $where_cond[] =
                array("user_id", "=", $this->get_user_id($query_options));
        }

        if ($this->has_user_phone_number_option($query_options)) {
            $where_cond[] =
                array("phone_number", "=", $this->get_phone_number($query_options));
        }

        $sb->select($this->user_select);
        $sb->from($table, "");
        $sb->where($where_cond);
        return $sb->build();
    }

    private function build_user_action_sql($query_options)
    {
        $sb = new SqlBuilder;
        $table = "user_session";
        $where_cond = array();
        if ($this->has_user_id_option($query_options)) {
            $where_cond[] =
                array("uid", "=", $this->get_user_id($query_options));
        }

        if ($this->has_user_phone_number_option($query_options)) {
            $where_cond[] =
                array("phone_number", "=", $this->get_phone_number($query_options));
        }
        if ($this->has_date_interval_option($query_options)) {
            $starttime = strtotime($this->get_date_interval_start($query_options));
            $endtime = strtotime($this->get_date_interval_end($query_options));
            $where_cond[] =
                array("timestamp", ">=", $starttime);
            $where_cond[] =
                array("timestamp", "<=", $endtime);
        }

        $sb->select($this->action_select);
        $sb->from($table, "");
        $sb->where($where_cond);
        return $sb->build();
    }

    private function build_user_action_count_sql($query_options)
    {
        $sb = new SqlBuilder;
        $table = "user_session";
        $where_cond = array();
        if ($this->has_user_id_option($query_options)) {
            $where_cond[] =
                array("uid", "=", $this->get_user_id($query_options));
        }

        if ($this->has_user_phone_number_option($query_options)) {
            $where_cond[] =
                array("phone_number", "=", $this->get_phone_number($query_options));
        }
        if ($this->has_date_interval_option($query_options)) {
            $starttime = strtotime($this->get_date_interval_start($query_options));
            $endtime = strtotime($this->get_date_interval_end($query_options));
            $where_cond[] =
                array("timestamp", ">=", $starttime);
            $where_cond[] =
                array("timestamp", "<=", $endtime);
        }

        $sb->from($table, "");
        $sb->where($where_cond);
        return 'select count(*)' . $sb->build();
    }


    private function get_date_interval_start($options)
    {
        return $options['options']['date_interval']['start'];
    }

    private function get_date_interval_end($options)
    {
        return $options['options']['date_interval']['end'];
    }

    private function get_user_id($options)
    {
        return $options['options']['uid'];
    }

    private function get_phone_number($options)
    {
        return $options['options']['phone_number'];
    }

    private function has_date_interval_option($options)
    {
        return isset($options['options']['date_interval']);
    }

    private function has_user_id_option($options)
    {
        return isset($options['options']['uid']);
    }

    private function has_user_phone_number_option($options)
    {
        return isset($options['options']['phone_number']);
    }

    public function action_search()
    {
        $query_options = $_POST;
        $select_array = array();
        if (isset($query_options['options']['uid'])) {
            $select_array["user.user_id"] = (int)$query_options['options']['uid'];
        } else {
            $select_array["user.phone_number"] = $query_options['options']['phone_number'];
        }
        $select_array['timestamp'] = array('$gt' => strtotime((int)$query_options['options']['date_interval']['start']),
            '$lte' => strtotime((int)$query_options['options']['date_interval']['end']));
        $page_item_count = $query_options['page_item_count'];
        ini_set('mongo.long_as_object', 1);
//        $m = new MongoClient("mongodb://10.30.24.171:12345");

        $m = new MongoClient("mongodb://10.30.21.5:8888");
        $collection = $m->userBev->newaction;
        $lines = $collection->find($select_array)->timeout(-1);
        $lines->timeout(-1);
        $rows = $lines->count();
        $cursor1 = $lines->sort(array("timestamp" => 1))->limit($page_item_count);
        $cursor2 = $collection->find($select_array, array("user" => 1, "_id" => 0))->limit(1);
        $ret['code'] = 0;
        $ret['data1'] = iterator_to_array($cursor1);
        $ret['data2'] = iterator_to_array($cursor2);
        $ret['rows'] = $rows;
        $ret['page_item_count'] = $page_item_count;
        exit(json_encode($ret));
    }


    public function action_page()
    {

        $query_options = $_POST;
        $page_item_count = (int)$query_options['count_per_page'];
        $page = $query_options['page'];
        $select_array = array();
        if (isset($query_options['options']['uid'])) {
            $select_array["user.user_id"] = (int)$query_options['options']['uid'];
        } else {
            $select_array["user.phone_number"] = $query_options['options']['phone_number'];
        }
        $select_array['timestamp'] = array('$gt' => strtotime((int)$query_options['options']['date_interval']['start']),
            '$lte' => strtotime((int)$query_options['options']['date_interval']['end']));
        ini_set('mongo.long_as_object', 1);
//        $m = new MongoClient("mongodb://10.30.24.171:12345");
        $m = new MongoClient("mongodb://10.30.21.5:8888");

        $collection = $m->userBev->newaction;
        $cursor1 = $collection->find($select_array)->sort(array("timestamp" => 1))->limit($page_item_count)->skip($page_item_count * ($page - 1));
//        $cursor2 = $collection->find($select_array, array("uid" => 1, "_id" => 0))->limit(1);
        $ret['code'] = 0;
        $ret['data'] = iterator_to_array($cursor1);
//        $ret['data2'] = iterator_to_array($cursor2);
        $ret['rows'] = $query_options['rows'];
        $ret['page'] = $page;
        $ret['page_item_count'] = $page_item_count;

        exit(json_encode($ret));
    }

}
