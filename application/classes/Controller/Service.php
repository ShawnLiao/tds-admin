<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Service extends Controller {

    public function action_getserver(){
        $sql = "SELECT id, server_name as name FROM server_info";
        $result = DB::query(Database::SELECT, $sql)->execute('role')->as_array();
        $ret['data'] = $result;
        $ret['code'] = 0;
        echo json_encode($ret);
    }
}
