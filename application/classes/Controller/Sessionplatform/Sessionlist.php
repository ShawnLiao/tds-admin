<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Sessionplatform_Sessionlist extends Controller
{
    public function action_search()
    {

        $query_options = $_POST;

        $select_array = array();

        if(isset($query_options['options']['uid'])){
            $select_array["uid.user_id"] = (int)$query_options['options']['uid'];
        }else{
            $select_array["uid.phone_number"] = $query_options['options']['phone_number'];
        }

        $select_array['timestamp'] = array('$gt' => strtotime((int)$query_options['options']['date_interval']['start']),
            '$lte' => strtotime((int)$query_options['options']['date_interval']['end']));

        $page_item_count = $query_options['page_item_count'];

        ini_set('mongo.long_as_object', 1);

//        $m = new MongoClient("mongodb://10.30.24.240:12345");
        $m = new MongoClient("mongodb://10.30.21.5:8888");
        $collection = $m->userBev->movie;

        $lines=$collection->find($select_array)->timeout(-1);


        $lines->timeout(-1);

        $rows = $lines->count();

        $cursor1 = $lines->sort(array("timestamp" => 1))->limit($page_item_count);

        $cursor2 = $collection->find($select_array, array("uid" => 1, "_id" => 0))->limit(1);

        $ret['code'] = 0;
        $ret['data1'] = iterator_to_array($cursor1);
        $ret['data2'] = iterator_to_array($cursor2);
        $ret['rows'] = $rows;
        $ret['page_item_count'] = $page_item_count;
        exit(json_encode($ret));
    }

    public function action_page()
    {

        $query_options = $_POST;

        $page_item_count = (int)$query_options['count_per_page'];

        $page = $query_options['page'];

        $select_array = array();

        if(isset($query_options['options']['uid'])){
            $select_array["uid.user_id"] = (int)$query_options['options']['uid'];
        }else{
            $select_array["uid.phone_number"] = $query_options['options']['phone_number'];
        }

        $select_array['timestamp'] = array('$gt' => strtotime((int)$query_options['options']['date_interval']['start']),
            '$lte' => strtotime((int)$query_options['options']['date_interval']['end']));

        ini_set('mongo.long_as_object', 1);

//        $m = new MongoClient("mongodb://10.30.24.240:12345");
        $m = new MongoClient("mongodb://10.30.21.5:8888");
        $collection = $m->userBev->movie;

        $cursor1 = $collection->find($select_array)->sort(array("timestamp" => 1))->limit($page_item_count)->skip($page_item_count * ($page-1));

//        $cursor2 = $collection->find($select_array, array("uid" => 1, "_id" => 0))->limit(1);

        $ret['code'] = 0;
        $ret['data'] = iterator_to_array($cursor1);
//        $ret['data2'] = iterator_to_array($cursor2);
        $ret['rows'] = $query_options['rows'];
        $ret['page'] = $page;
        $ret['page_item_count'] = $page_item_count;

        exit(json_encode($ret));
    }

}
