<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Sessionplatform_Form extends Controller_Layout {

  public $styles = array(
    'static/assets/plugins/bootstrap-datepicker/css/datepicker.css' => 'screen',
    'static/assets/plugins/bootstrap-datepicker/css/datepicker3.css' => 'screen',
    'static/assets/plugins/ionRangeSlider/css/ion.rangeSlider.css' => 'screen',
    'static/assets/plugins/ionRangeSlider/css/ion.rangeSlider.skinNice.css' => 'screen',
    'static/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css' => 'screen',
    'static/assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css' => 'screen',
    'static/assets/plugins/password-indicator/css/password-indicator.css' => 'screen',
    'static/assets/plugins/bootstrap-combobox/css/bootstrap-combobox.css' => 'screen',
    'static/assets/plugins/bootstrap-select/bootstrap-select.min.css' => 'screen',
    'static/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css' => 'screen',
    'static/assets/plugins/jquery-tag-it/css/jquery.tagit.css' => 'screen',
    'static/assets/plugins/DataTables/css/data-table.css' => 'screen',
    'static/css/customer.css' => 'screen'
  );

  public $scripts = array(
    'static/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
    'static/assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
    'static/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
    'static/assets/plugins/masked-input/masked-input.min.js',
    'static/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
    'static/assets/plugins/password-indicator/js/password-indicator.js',
    'static/assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
    'static/assets/plugins/bootstrap-select/bootstrap-select.min.js',
    'static/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
    'static/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
    'static/assets/plugins/jquery-tag-it/js/tag-it.min.js',
    'static/assets/js/form-plugins.demo.min.js',
    'static/assets/js/dashboard-v2.min.js',
    'static/assets/js/apps.min.js',
    'static/highcharts/maps/highmaps.js',
    'static/highcharts/modules/data.js',
    'static/highcharts/modules/exporting.js',
    'static/js/cn-china.js',

  );

  public function action_sessionlist()
  {
    $this->template->page_title = 'Adam 数据平台 - session平台';

    $scripts = array(
      'static/js/sessionplatform/sessionlist.js',
      'static/js/index.js',
    );

    $this->template->page_styles = array_merge($this->template->styles, $this->styles);
    $this->template->page_scripts = array_merge($this->scripts, $scripts);

    $this->template->page_content = View::factory('sessionplatform/sessionlist');

    $this->set_side_bar_nav_index(12, 0);
  }



}
