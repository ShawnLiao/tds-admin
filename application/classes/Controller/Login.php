<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Login extends Controller {
  public function action_index()
  {
    if (Auth::instance()->logged_in()) {
      $this->redirect('/', 302);
    }
    $this->response->body(View::factory('login'));
  }
}

