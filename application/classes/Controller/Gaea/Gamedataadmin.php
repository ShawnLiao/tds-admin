<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Gaea_Gamedataadmin extends Controller {

	private $mapServer = array(
		array('col' => 'id', 'table' => '', 'as' => 'id'),
		array('col' => 'user_id', 'table' => '', 'as' => 'user_id'),
		array('col' => 'server_id', 'table' => '', 'as' => 'server_id'),
		array('col' => 'role_name', 'table' => '', 'as' => 'role_name'),
		array('col' => 'level', 'table' => '', 'as' => 'level'),
		array('col' => 'case nobility
											when 0 then \'男爵\'
											when 1 then \'子爵\'
											when 2 then \'伯爵\'
											when 3 then \'候爵\'
											when 4 then \'公爵\'
										end', 'table' => '', 'as' => 'nobility'),
		array('col' => 'case country 
											when 0 then \'金狮\' 
											when 1 then \'战熊\'
											when 2 then \'火狐\'
											when 3 then \'独角兽\'
											when 4 then \'巨象\'
											when 5 then \'雪兔\'
										end', 'table' => '', 'as' => 'country'),
		array('col' => 'case sex
											when 0 then \'男\'
											when 1 then \'女\'
										end', 'table' => '', 'as' => 'sex'),
	);

	private $tableName = 'role_info';

  public function action_search(){
    $options = $this->request->query();
		$page_item_count = $options['page_item_count'];
    $res = $this->get_result($options);
		$rows = count($res);
		$res = array_slice($res,0,$page_item_count);
		$res = $this->get_role_token($res);
		$res = $this->get_town_list($res);
    $ret['code'] = 0;
    $ret['data'] = $res;
    $ret['rows'] = $rows;
    $ret['page_item_count'] = $page_item_count;
		$ret['page'] = 1;
    exit(json_encode($ret));
  }

	public function action_page(){
		$options = $this->request->query();
		$page_item_count = $options['page_item_count'];
		$page = $options['page'];
		$start = ($page - 1) * $page_item_count;
		$rows = $options['rows'];
		$res = $this->get_result($options, $start, $page_item_count);
		$res = $this->get_role_token($res);
    $ret['code'] = 0;
    $ret['data'] = $res;
    $ret['rows'] = $rows;
    $ret['page_item_count'] = $page_item_count;
		$ret['page'] = $page;
    exit(json_encode($ret));
	}

	public function action_deleteRole(){
		$options = $this->request->query();
		$roleId = $options['role_id'];
		$sql = "DELETE FROM role_info WHERE id = $roleId";
		$result = DB::query(Database::DELETE, $sql)->execute('role');
		$sql = "DELETE FROM town_info WHERE role_id = $roleId";
		$result = DB::query(Database::DELETE, $sql)->execute('role');
    $ret['code'] = 0;
    exit(json_encode($ret));
	}

	private function get_role_token($res){
		for ($i = 0 ; $i < count($res); $i++){
			$roleId = $res[$i]['id'];
			$sql = "SELECT token FROM role_token WHERE role_id = $roleId";
			$roleToken = DB::query(Database::SELECT, $sql)->execute('role')->as_array();
			$res[$i]['token'] = (count($roleToken) === 0 ? '':$roleToken['0']['token']);
		}
		return $res;
	}

	private function get_town_list($res){
		for ($i = 0 ; $i < count($res); $i++){
			$roleId = $res[$i]['id'];
			$sql = "SELECT id FROM town_info WHERE role_id = $roleId";
			$townRes = DB::query(Database::SELECT, $sql)->execute('role')->as_array();
			$townList = array();
			foreach ($townRes as $town){
				array_push($townList, $town['id']);
			}
			$res[$i]['townList'] = (count($townList) === 0 ? '':$townList);
		}
		return $res;
	}

  private function get_result($options, $start = 0, $count = 0){
		$sql = new SqlBuilder;
		$sql->select($this->mapServer);
		$sql->from($this->tableName, '');

		$where_cond = array();
		if ($options['role_id'] !== ''){
			$where_cond[] = array('id', '=', $this->get_role_id($options));
		}
		if ($options['server_id'] !== ''){
			$where_cond[] = array('server_id', '=', $this->get_server_id($options));
		}
		if ($options['country'] !== ''){
			$where_cond[] = array('country', '=', $this->get_country($options));
		}
		$sql->where($where_cond);
		
		$finalSql = $sql->build();
		if ($count != 0){
			$finalSql = $finalSql." limit $start, $count";
		}
		$res = DB::query(Database::SELECT, $finalSql)->execute('role')->as_array();
		return $res;
  }

	private function get_role_id($options){
		return "'".$options['role_id']."'";
	}

	private function get_server_id($options){
		return "'".$options['server_id']."'";
	}

	private function get_country($options){
		return "'".$options['country']."'";
	}
}
