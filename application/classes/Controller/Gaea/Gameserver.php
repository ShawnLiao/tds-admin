<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Gaea_Gameserver extends Controller {

	private $mapServer = array(
		array('col' => 'host_ip', 'table' => '', 'as' => 'host_ip'),
		array('col' => 'port', 'table' => '', 'as' => 'port'),
		array('col' => 'status', 'table' => '', 'as' => 'status'),
		array('col' => 'map_type', 'table' => '', 'as' => 'map_type'),
		array('col' => 'map_id', 'table' => '', 'as' => 'map_id'),
		array('col' => 'game_version', 'table' => '', 'as' => 'game_version'),
	);

  public function action_search(){
    $options = $this->request->query();
		$page_item_count = $options['page_item_count'];
    $res = $this->get_result($options);
    $ret['code'] = 0;
    $ret['data'] = array_slice($res,0,$page_item_count);
    $ret['rows'] = count($res);
    $ret['page_item_count'] = $page_item_count;
		$ret['page'] = 1;
    exit(json_encode($ret));
  }

	public function action_page(){
		$options = $this->request->query();
		$page_item_count = $options['page_item_count'];
		$page = $options['page'];
		$start = ($page - 1) * $page_item_count;
		$rows = $options['rows'];
		$res = $this->get_result($options, $start, $page_item_count);
    $ret['code'] = 0;
    $ret['data'] = $res;
    $ret['rows'] = $rows;
    $ret['page_item_count'] = $page_item_count;
		$ret['page'] = $page;
    exit(json_encode($ret));
	}

  private function get_result($options, $start = 0, $count = 0){
		$sql = new SqlBuilder;
		$sql->select($this->mapServer);
		$sql->from('map_server', '');

		$where_cond = array();
		if ($options['host_ip_list'] !== ''){
			$where_cond[] = array('host_ip', '=', $this->get_host_ip($options));
		}
		if ($options['status'] !== ''){
			$where_cond[] = array('status', '=', $this->get_status($options));
		}
		$sql->where($where_cond);
		
		$finalSql = $sql->build();
		if ($count != 0){
			$finalSql = $finalSql." limit $start, $count";
		}
		$res = DB::query(Database::SELECT, $finalSql)->execute('role')->as_array();
		return $res;
  }

	private function get_status($options){
		return "'".$options['status']."'";
	}

	private function get_host_ip($options){
		return "'".$options['host_ip_list']."'";
	}
}
