<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Gaea_Serverdomain extends Controller {

	private $mapServer = array(
		array('col' => 'distinct host_ip', 'table' => '', 'as' => 'host_ip'),
	);

  public function action_search(){
    $options = $this->request->query();
		$page_item_count = $options['page_item_count'];
    $res = $this->get_result($options);
		$rows = count($res);
		$res = array_slice($res,0,$page_item_count);
		$res = $this->get_domain($res);
    $ret['code'] = 0;
    $ret['data'] = $res;
    $ret['rows'] = $rows;
    $ret['page_item_count'] = $page_item_count;
		$ret['page'] = 1;
    exit(json_encode($ret));
  }

	public function action_page(){
		$options = $this->request->query();
		$page_item_count = $options['page_item_count'];
		$page = $options['page'];
		$start = ($page - 1) * $page_item_count;
		$rows = $options['rows'];
		$res = $this->get_result($options, $start, $page_item_count);
		$res = $this->get_domain($res);
    $ret['code'] = 0;
    $ret['data'] = $res;
    $ret['rows'] = $rows;
    $ret['page_item_count'] = $page_item_count;
		$ret['page'] = $page;
    exit(json_encode($ret));
	}

	public function action_changeHostDomain(){
		$options = $this->request->query();
		$hostIp = $options['host_ip'];
		$hostDomain = $options['host_domain'];
		$sql = "SELECT * FROM map_server_domain WHERE host_ip = '$hostIp'";
		$res = DB::query(Database::SELECT, $sql)->execute('role')->as_array();
		if (count($res) === 0){
			$sql = "INSERT INTO map_server_domain values(DEFAULT, '$hostIp', '$hostDomain')";
			$res = DB::query(Database::INSERT, $sql)->execute('role');
		} else {
			$sql = "UPDATE map_server_domain set host_domain = '$hostDomain' WHERE host_ip = '$hostIp'";
			$res = DB::query(Database::UPDATE, $sql)->execute('role');
		}
    $ret['code'] = 0;
    exit(json_encode($ret));
	}

  private function get_domain($res){
    for ($i = 0 ; $i < count($res); $i++){
      $hostIp = $res[$i]['host_ip'];
      $sql = "SELECT host_domain FROM map_server_domain WHERE host_ip = '$hostIp'";
      $hostDomain = DB::query(Database::SELECT, $sql)->execute('role')->as_array();
      if (count($hostDomain) === 0) $res[$i]['host_domain'] = '';
      else $res[$i]['host_domain'] = $hostDomain[0]['host_domain'];
    }
    return $res;
  }

  private function get_result($options, $start = 0, $count = 0){
		$sql = new SqlBuilder;
		$sql->select($this->mapServer);
		$sql->from('map_server', '');

		$finalSql = $sql->build();
		if ($count != 0){
			$finalSql = $finalSql." limit $start, $count";
		}
		$res = DB::query(Database::SELECT, $finalSql)->execute('role')->as_array();
		return $res;
  }

	private function get_status($options){
		return "'".$options['status']."'";
	}

	private function get_host_ip($options){
		return "'".$options['host_ip_list']."'";
	}
}
