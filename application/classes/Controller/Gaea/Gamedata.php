<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Gaea_Gamedata extends Controller {

  public function action_search(){
    $options = $this->request->query();
		$roleID = $options['role_id'];
    $dbName = $this->get_roleDBName($roleID);
		$roleData = $this->get_roleResult($roleID, $dbName);
		$townList = $this->get_town_list($roleID);
		$townID = $townList[0];
		$dbName = $this->get_townDBName($townID);
		$townData = $this->get_townResult($townID, $dbName);
    $ret['code'] = 0;
    $ret['roleData'] = $roleData;
		$ret['townData'] = $townData;
		$ret['townList'] = $townList;
    exit(json_encode($ret));
  }

  public function action_searchTown(){
    $options = $this->request->query();
		$townID = $options['town_id'];
		$dbName = $this->get_townDBName($townID);
		$townData = $this->get_townResult($townID, $dbName);
    $ret['code'] = 0;
		$ret['townData'] = $townData;
    exit(json_encode($ret));
  }

	private function get_roleDBName($roleID){
		$sql = "SELECT db_id FROM role_info WHERE id = $roleID";
		$dbIDRes = DB::query(Database::SELECT, $sql)->execute('role')->as_array();
		$dbID = $dbIDRes[0]['db_id'];
		$sql = "SELECT db_name FROM db_info where id = $dbID";
		$dbNameRes = DB::query(Database::SELECT, $sql)->execute('role')->as_array();
		return $dbNameRes[0]['db_name'];
	}

  private function get_roleResult($roleID, $dbName){
		$m = new MongoClient($dbName);
		$collection = $m->gaea->roleData;
		$datas = $collection->find(array('roleId' => $roleID), array('roleData'))->limit(1);  
		foreach ($datas as $data){                                                            
			  return json_decode($data['roleData'], true);
		}
		return null;
  }

	private function get_town_list($roleID){
		$sql = "SELECT id FROM town_info WHERE role_id = $roleID";
		$townList = DB::query(Database::SELECT, $sql)->execute('role')->as_array();
		$res = array();
		foreach($townList as $town){
			array_push($res, $town['id']);
		}
		return $res;
	}

	private function get_townDBName($townID){
		$sql = "SELECT db_id FROM town_info WHERE id = $townID";
		$dbIDRes = DB::query(Database::SELECT, $sql)->execute('role')->as_array();
		$dbID = $dbIDRes[0]['db_id'];
		$sql = "SELECT db_name FROM db_info where id = $dbID";
		$dbNameRes = DB::query(Database::SELECT, $sql)->execute('role')->as_array();
		return $dbNameRes[0]['db_name'];
	}

  private function get_townResult($townID, $dbName){
		$m = new MongoClient($dbName);
		$collection = $m->gaea->townData;
		$datas = $collection->find(array('townId' => $townID), array('townData'))->limit(1);  
		foreach ($datas as $data){                                                            
			  return json_decode($data['townData'], true);
		}
		return null;
  }

}
