<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Layout extends Controller_Template {
  public $template = 'layout';

  public function set_side_bar_nav_index($l1, $l2) {
    $this->template->level_one_index = $l1;
    $this->template->level_two_index = $l2;
  }

  public function before() {
    parent::before();

    #if (!Auth::instance()->logged_in()) {
    #  $this->redirect('/login', 302);
    #}

    #$user_id = Auth::instance()->get_user_id();
    #$page = $this->request->uri();

    #$group_model = Model::factory("Admin_Group");
    $this->template->page_content = null;
    #if (!$group_model->accessible($user_id, $page)) {
    #  $this->template->page_content = View::factory('no_permission');
    #}
    $this->template->page_title = '';

    $this->template->styles = array();
    $this->template->scripts = array();

    $this->template->level_one_index = 0;
    $this->template->level_two_index = 0;
    #$this->template->user_name = Auth::instance()->get_name();
  }

  public function after() {
    // Load css
    $this->load_styles();
    // Load js
    $this->load_scripts();
    // Load views
    $this->load_views();

    parent::after();
  }

  private function load_styles() {
    // Base Styles
    $styles = array(
      'static/assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css' => 'screen',
      'static/assets/plugins/bootstrap/css/bootstrap.min.css' => 'screen',
      'static/assets/plugins/font-awesome/css/font-awesome.min.css' => 'screen',
      'static/assets/css/animate.min.css' => 'screen',
      'static/assets/css/style.min.css' => 'screen',
      'static/assets/css/style-responsive.min.css' => 'screen',
      'static/assets/css/theme/default.css' => 'screen',
    );

    $this->template->base_styles = array_merge($this->template->styles, $styles);
  }

  private function load_scripts() {
    // Pace script
    $pace_scripts = array(
      'static/assets/plugins/pace/pace.min.js'
    );

    // Base scripts
    $base_scripts = array(
      'static/assets/plugins/jquery/jquery-1.9.1.min.js',
      'static/assets/plugins/jquery/jquery-migrate-1.1.0.min.js',
      'static/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js',
      'static/assets/plugins/bootstrap/js/bootstrap.min.js',
      'static/assets/plugins/slimscroll/jquery.slimscroll.min.js',
      'static/assets/plugins/jquery-cookie/jquery.cookie.js',
      'static/js/logout.js'
    );

    // IE scripts
    $ie_scripts = array(
      'static/assets/crossbrowserjs/html5shiv.js',
      'static/assets/crossbrowserjs/respond.min.js',
      'static/assets/crossbrowserjs/excanvas.min.js'
    );

    $this->template->pace_scripts = array_merge($this->template->scripts, $pace_scripts);
    $this->template->base_scripts = array_merge($this->template->scripts, $base_scripts);
    $this->template->ie_scripts = array_merge($this->template->scripts, $ie_scripts);
  }

  private function load_views() {
    // Page loader view
    $this->template->page_loader = View::factory('page_loader');

    // Page header view
    $this->template->page_header = View::factory('page_header');

    // Page sidebar view
    $this->template->page_sidebar = View::factory('page_sidebar');

    #$admin_enter = "";
    #if (Auth::instance()->is_admin()) {
    #  $admin_enter = View::factory('admin_enter');
    #}
    #$this->template->page_sidebar->bind('admin_enter', $admin_enter);

    #$industryAdmin_enter = "";
    #if (Auth::instance()->is_industryAdmin()) {
    #  $industryAdmin_enter = View::factory('industryAdmin_enter');
    #}
    #$this->template->page_sidebar->bind('industryAdmin_enter', $industryAdmin_enter);

    // Init script view
    $this->template->init_script = View::factory('init_script')
      ->bind('level_one_index', $this->template->level_one_index)
      ->bind('level_two_index', $this->template->level_two_index);
    #  ->bind('user_name', $this->template->user_name);
  }
}

