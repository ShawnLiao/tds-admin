<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Auth extends Controller {

  public function action_changepassword() {
    $post = $this->request->post();

    $ret = array();
    if (!Auth::instance()->check_password($post['password'])) {
      $ret['code'] = 1;
      $ret['msg'] = "旧密码不正确";
      echo(json_encode($ret));
      exit;
    }

    $success = Auth::instance()->change_password($post['new_password']);
    if ($success) {
      Auth::instance()->logout();
      $ret['code'] = 0;
    } else {
      $ret['code'] = 1;
      $ret['msg'] = "修改密码失败";
    }
    echo(json_encode($ret));
  }

  public function action_register() {
    $post = $this->request->post();
    $ret = array();
    if (Auth::instance()->register($post['email'], $post['name'], "QAZxcvbnm", (bool)$post['is_admin'])) {
      $flag = $this->Sendemail($post['email']);
      if(1==$flag) $ret['flag'] = "邮件发送成功";
      else $ret['flag'] = "邮件发送失败";
      $ret['code'] = 0;
    } else {
      $ret['msg'] = "添加用户失败";
      $ret['code'] = 1;
    }
    echo json_encode($ret);
  }

  public function Sendemail($email) {
    $to = $email;//收件人  
    $subject = "亚当权限开通";//邮件主题  
    $message = " 
    已开通~~~

    用户名是大家的工作邮箱，初始密码是：QAZxcvbnm

    登陆地址是：adam.culiu.org/login

    首次登录请先修改密码，账号请不要借给他人使用。

    密码修改地址：请点击我

    请加入数据平台用户群：485534921

    可在群中反馈问题

    如有疑问请联系姜忠涛、唐焱、魏红亮

    登陆权限问题请联系姜忠涛

    谢谢！";//邮件正文  
    ini_set('SMTP','smtp.exmail.qq.com');//发件SMTP服务器  
    ini_set('smtp_port',25);//发件SMTP服务器端口
    $from = "jiangzt@chuchujie.com";
    $headers = "From: $from";  
    $flag=mail($to,$subject,$message,$headers);
    if(1==$flag) return 1;
    else return 0;   
  }

  public function action_login() {
    $post = $this->request->post();
    $success = Auth::instance()->login($post['email'], $post['password']);

    $ret = array();
    if ($success) {
      $ret['code'] = 0;
    } else {
      $ret['code'] = 1;
      $ret['msg'] = "用户名或密码不正确";
    }

    echo(json_encode($ret));
  }

  public function action_logout() {
    Auth::instance()->logout();

    $ret['code'] = 0;
    echo(json_encode($ret));
  }
}



