<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Admin extends Controller {

  public function action_test() {
    $group_model = Model::factory('Admin_Group');
    var_dump($group_model->exist_user(1, 1));
  }

  public function action_add_group() {
    $post = $this->request->post();
    $group = $post['group'];

    $ret = array();
    $group_model = Model::factory('Admin_Group');
    $ret['data'] = array('id' => $group_model->add_group($group)[0], 'group' => $group);
    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function action_group_delete_user() {
    $post = $this->request->post();

    $group_id = $post['group_id'];
    $user_id = $post['user_id'];

    $group_model = Model::factory('Admin_Group');
    $group_model->delete_user($group_id, $user_id);
    $ret = array();
    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function action_group_delete_page() {
    $post = $this->request->post();

    $group_id = $post['group_id'];
    $page_id = $post['page_id'];

    $group_model = Model::factory('Admin_Group');
    $group_model->delete_page($group_id, $page_id);
    $ret = array();
    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function action_group_add_page() {
    $post = $this->request->post();

    $group_id = $post['group_id'];
    $page_id = $post['page_id'];

    $group_model = Model::factory('Admin_Group');
    if (!$group_model->exist_page($group_id, $page_id)) {
      $group_model->add_page($group_id, $page_id);
    }
    $ret = array();
    $ret['code'] = 0;
    echo json_encode($ret);
  }
  
  public function action_db_add_page() {
    $post = $this->request->post();

    $path_name = $post['path_name'];
    $page_name = $post['page_name'];
  
    $dbpage_model = Model::factory('Admin_Page');
    $dbpage_model->add_db_page($path_name, $page_name);
   
    $ret = array();
    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function action_deluser() {
    $post = $this->request->post();
    $ret = array();
    $email_del=$post['email_del'];
    $name_del=$post['name_del'];
    $dbuser_model = Model::factory('Admin_User');
    $dbuser_model->del_db_user($email_del,$name_del);

    $ret = array();
    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function action_resetpassword() {
    $post = $this->request->post();
    $ret = array();
    $email_set=$post['email_reset'];
    $name_set=$post['name_reset'];
    $reset_model = Model::factory('Admin_User');
    $reset_model->reset_password($email_set,$name_set);
    //$re_model = factory('Auth_User');
    $flag=$this->Sendemail($post['email_reset']);
    if($flag==1) $ret['flag'] = "密码重置邮件发送成功";
    else $ret['flag'] = "密码重置邮件发送失败";
    $ret['code'] = 0;
    echo json_encode($ret);
  }
  public function Sendemail($email) {
    $to = $email;//收件人  
    $subject = "亚当password重置";//邮件主题  
    $message = "
    已重置~~~

    用户名是大家的工作邮箱，密码是：QAZxcvbnm

    登录请先修改密码，账号请不要借给他人使用

    请加入数据平台用户群：485534921 

    可在群中反馈问题

    如有疑问请联系姜忠涛、唐焱、魏红亮

    登陆权限问题请联系姜忠涛

    谢谢！";//邮件正文  
    ini_set('SMTP','smtp.exmail.qq.com');//发件SMTP服务器  
    ini_set('smtp_port',25);//发件SMTP服务器端口
    $from = "jiangzt@chuchujie.com";
    $headers = "From: $from";
    $flag=mail($to,$subject,$message,$headers);
    if(1==$flag) return 1;
    else return 0;    
  }

  public function action_db_del_page() {
    $post = $this->request->post();

    $page_id = $post['page_id'];
    $dbpage_model = Model::factory('Admin_Page');

    $dbpage_model->del_db_page($page_id);
   
    $ret = array();
    $ret['code'] = 0;
    echo json_encode($ret);
  }
  
  public function action_db_add_finance() {
    $post = $this->request->post();

    $name = $post['name'];
    $email = $post['email'];
    $finance_model = Model::factory('Admin_Page');

    $finance_model->add_db_finance($name,$email);

    $ret = array();
    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function action_group_add_user() {
    $post = $this->request->post();
    $group_id = $post['group_id'];
    $name = $post['name'];
    //$group_id = 1;
    //$name = "魏红亮";

    $user_model = Model::factory('Admin_User');
    $user_id = $user_model->get_user_id_by_name($name);

    $ret = array();
    if (!$user_id) {
      $ret['msg'] = "用户名不存在";
      $ret['code'] = -1;
      echo json_encode($ret);
      exit;
    }

    $group_model = Model::factory('Admin_Group');
    if (!$group_model->exist_user($group_id, $user_id)) {
      $group_model->add_user($group_id, $user_id);
    }
    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function action_userlist() {
    $model = Model::factory('Admin_User');
    $ret = array();
    $ret['user_list'] = $model->get_user_list();
    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function action_group_list() {
    $model = Model::factory('Admin_Group');
    $ret = array();
    $ret['group_list'] = $model->get_group_list();
    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function action_page_list() {
    $model = Model::factory('Admin_Page');
    $ret = array();
    $ret['page_list'] = $model->get_page_list();
    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function action_group_user_list() {
    $post = $this->request->post();
    $model = Model::factory('Admin_Group');
    $ret = array();
    $ret['group_user_list'] = $model->get_group_user_list_by_id($post['group_id']);
    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function action_group_page_list() {
    $post = $this->request->post();
    $model = Model::factory('Admin_Group');
    $ret = array();
    $ret['group_page_list'] = $model->get_group_page_list_by_id($post['group_id']);
    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function action_userSearch() {
    $post = $_POST;

    $name = $post['options']['name'];
    $search_model = Model::factory('Admin_Userpermissionsearch');

    $data=$search_model->user_earch($name);

    $ret = array();
    $ret['code'] = 0;
    $ret['data'] = $data;
    echo json_encode($ret);
  }

}
