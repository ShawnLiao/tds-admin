<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Shops_Shops extends Controller {

  private $select_detail = array(
    array("col" => "date" , "table" => "" , "as" => "day"),
    array("col" => "shop_id" , "table" => "" , "as" => "shop_id"),
    array("col" => "shop_title" , "table" => "" , "as" => "shop_title"),
    array("col" => "c1_name" , "table" => "" , "as" => "c1"),
    array("col" => "c2_name" , "table" => "" , "as" => "c2"),
    array("col" => "detailed_address" , "table" => "" , "as" => "detailed_address"),
    array("col" => "open_days" , "table" => "" , "as" => "open_days"),
    array("col" => "last_online_time" , "table" => "" , "as" => "last_online_time"),
    array("col" => "online_pdt_cnt" , "table" => "" , "as" => "online_pdt_cnt"),
    array("col" => "last_pdt_new_cnt" , "table" => "" , "as" => "last_pdt_new_cnt"),
    array("col" => "frt_pdt_cnt" , "table" => "" , "as" => "frt_pdt_cnt"),
    array("col" => "cast(frt_add_pdt_fee as decimal(20,2))" , "table" => "" , "as" => "frt_add_pdt_fee"),
    array("col" => "frt_add_uv" , "table" => "" , "as" => "frt_add_uv"),
    array("col" => "frt_add_od_cnt" , "table" => "" , "as" => "frt_add_od_cnt"),
    array("col" => "frt_add_pdt_cnt" , "table" => "" , "as" => "frt_add_pdt_cnt"),
    array("col" => "cast(frt_add_price_per_od as decimal(20,2))" , "table" => "" , "as" => "frt_add_price_per_od"),
    array("col" => "cast(frt_pay_pdt_fee as decimal(20,2))" , "table" => "" , "as" => "frt_pay_pdt_fee"),
    array("col" => "frt_pay_uv" , "table" => "" , "as" => "frt_pay_uv"),
    array("col" => "frt_pay_od_cnt" , "table" => "" , "as" => "frt_pay_od_cnt"),
    array("col" => "frt_pay_pdt_cnt" , "table" => "" , "as" => "frt_pay_pdt_cnt"),
    array("col" => "cast(frt_pay_price_per_od as decimal(20,2))" , "table" => "" , "as" => "frt_pay_price_per_od"),
    array("col" => "cast(new_pay_total_fee as decimal(20,2))" , "table" => "" , "as" => "new_pay_total_fee"),
    array("col" => "new_u_pay_cnt" , "table" => "" , "as" => "new_u_pay_cnt"),
    array("col" => "cast(new_add_total_fee as decimal(20,2))" , "table" => "" , "as" => "new_add_total_fee"),
    array("col" => "new_u_add_cnt" , "table" => "" , "as" => "new_u_add_cnt"),
    array("col" => "new_cart_add_cnt" , "table" => "" , "as" => "new_cart_add_cnt"),
    array("col" => "new_u_cart_cnt" , "table" => "" , "as" => "new_u_cart_cnt"),
    array("col" => "new_fav_add_cnt" , "table" => "" , "as" => "new_fav_add_cnt"),
    array("col" => "new_u_fav_cnt" , "table" => "" , "as" => "new_u_fav_cnt"),
    array("col" => "last_30d_od_cnt" , "table" => "" , "as" => "last_30d_od_cnt"),
    array("col" => "cast(last_30d_od_fee as decimal(20,2))" , "table" => "" , "as" => "last_30d_od_fee"),
    array("col" => "last_90d_od_cnt" , "table" => "" , "as" => "last_90d_od_cnt"),
    array("col" => "cast(last_90d_od_fee as decimal(20,2))" , "table" => "" , "as" => "last_90d_od_fee"),
    array("col" => "pdt_uv" , "table" => "" , "as" => "pdt_uv"),
    array("col" => "pdt_uniq_cnt" , "table" => "" , "as" => "pdt_uniq_cnt"),
    array("col" => "fav_shop_cnt" , "table" => "" , "as" => "fav_shop_cnt"),
    array("col" => "fav_shop_uv_cnt" , "table" => "" , "as" => "fav_shop_uv_cnt"),
    array("col" => "fav_pdt_cnt" , "table" => "" , "as" => "fav_pdt_cnt"),
    array("col" => "fav_pdt_uv_cnt" , "table" => "" , "as" => "fav_pdt_uv_cnt"),
    array("col" => "cart_add_cnt" , "table" => "" , "as" => "cart_add_cnt"),
    array("col" => "cart_uniq_uv" , "table" => "" , "as" => "cart_uniq_uv"),
    array("col" => "pay_uniq_pdt_cnt" , "table" => "" , "as" => "pay_uniq_pdt_cnt"),
    array("col" => "cast(pay_gmv as decimal(20,2))" , "table" => "" , "as" => "pay_gmv"),
    array("col" => "cast(pay_dct_gmv as decimal(20,2))" , "table" => "" , "as" => "pay_dct_gmv"),
    array("col" => "pay_uv" , "table" => "" , "as" => "pay_uv"),
    array("col" => "pay_pdt_cnt" , "table" => "" , "as" => "pay_pdt_cnt"),
    array("col" => "pay_od_cnt" , "table" => "" , "as" => "pay_od_cnt"),
    array("col" => "cast(pay_price_per_od as decimal(20,2))" , "table" => "" , "as" => "pay_price_per_od"),
    array("col" => "add_uniq_pdt_cnt" , "table" => "" , "as" => "add_uniq_pdt_cnt"),
    array("col" => "cast(add_gmv as decimal(20,2))" , "table" => "" , "as" => "add_gmv"),
    array("col" => "cast(add_dct_gmv as decimal(20,2))" , "table" => "" , "as" => "add_dct_gmv"),
    array("col" => "add_uv" , "table" => "" , "as" => "add_uv"),
    array("col" => "add_pdt_cnt" , "table" => "" , "as" => "add_pdt_cnt"),
    array("col" => "add_od_cnt" , "table" => "" , "as" => "add_od_cnt"),
    array("col" => "cast(add_price_per_od as decimal(20,2))" , "table" => "" , "as" => "add_price_per_od"),
    array("col" => "good_rates_cnt" , "table" => "" , "as" => "good_rates_cnt"),
    array("col" => "eft_good_rates_cnt" , "table" => "" , "as" => "eft_good_rates_cnt"),
    array("col" => "rates_cnt" , "table" => "" , "as" => "rates_cnt"),
    array("col" => "cast(good_rates_ratio as decimal(20,2))" , "table" => "" , "as" => "good_rates_ratio"),
    array("col" => "cast(eft_good_rates_ratio as decimal(20,2))" , "table" => "" , "as" => "eft_good_rates_ratio"),
    array("col" => "eft_rates_cnt" , "table" => "" , "as" => "eft_rates_cnt"),
    array("col" => "rfd_od_cnt" , "table" => "" , "as" => "rfd_od_cnt"),
  );

  private $select_avg = array(
    array("col" => "date" , "table" => "" , "as" => "day"),
    array("col" => "0" , "table" => "" , "as" => "shop_id"),
    array("col" => "0" , "table" => "" , "as" => "shop_title"),
    array("col" => "0" , "table" => "" , "as" => "c1"),
    array("col" => "0" , "table" => "" , "as" => "c2"),
    array("col" => "0" , "table" => "" , "as" => "detailed_address"),
    array("col" => "avg(open_days)" , "table" => "" , "as" => "open_days"),
    array("col" => "avg(last_online_time)" , "table" => "" , "as" => "last_online_time"),
    array("col" => "avg(online_pdt_cnt)" , "table" => "" , "as" => "online_pdt_cnt"),
    array("col" => "avg(last_pdt_new_cnt)" , "table" => "" , "as" => "last_pdt_new_cnt"),
    array("col" => "avg(frt_pdt_cnt)" , "table" => "" , "as" => "frt_pdt_cnt"),
    array("col" => "avg(frt_add_pdt_fee)" , "table" => "" , "as" => "frt_add_pdt_fee"),
    array("col" => "avg(frt_add_uv)" , "table" => "" , "as" => "frt_add_uv"),
    array("col" => "avg(frt_add_od_cnt)" , "table" => "" , "as" => "frt_add_od_cnt"),
    array("col" => "avg(frt_add_pdt_cnt)" , "table" => "" , "as" => "frt_add_pdt_cnt"),
    array("col" => "cast(sum(frt_add_pdt_fee)/sum(frt_add_od_cnt) as decimal(20,2))" , "table" => "" , "as" => "frt_add_price_per_od"),
    array("col" => "avg(frt_pay_pdt_fee)" , "table" => "" , "as" => "frt_pay_pdt_fee"),
    array("col" => "avg(frt_pay_uv)" , "table" => "" , "as" => "frt_pay_uv"),
    array("col" => "avg(frt_pay_od_cnt)" , "table" => "" , "as" => "frt_pay_od_cnt"),
    array("col" => "avg(frt_pay_pdt_cnt)" , "table" => "" , "as" => "frt_pay_pdt_cnt"),
    array("col" => "cast(sum(frt_pay_pdt_fee)/sum(frt_pay_od_cnt) as decimal(20,2))" , "table" => "" , "as" => "frt_pay_price_per_od"),
    array("col" => "avg(new_pay_total_fee)" , "table" => "" , "as" => "new_pay_total_fee"),
    array("col" => "avg(new_u_pay_cnt)" , "table" => "" , "as" => "new_u_pay_cnt"),
    array("col" => "avg(new_add_total_fee)" , "table" => "" , "as" => "new_add_total_fee"),
    array("col" => "avg(new_u_add_cnt)" , "table" => "" , "as" => "new_u_add_cnt"),
    array("col" => "avg(new_cart_add_cnt)" , "table" => "" , "as" => "new_cart_add_cnt"),
    array("col" => "avg(new_u_cart_cnt)" , "table" => "" , "as" => "new_u_cart_cnt"),
    array("col" => "avg(new_fav_add_cnt)" , "table" => "" , "as" => "new_fav_add_cnt"),
    array("col" => "avg(new_u_fav_cnt)" , "table" => "" , "as" => "new_u_fav_cnt"),
    array("col" => "avg(last_30d_od_cnt)" , "table" => "" , "as" => "last_30d_od_cnt"),
    array("col" => "avg(last_30d_od_fee)" , "table" => "" , "as" => "last_30d_od_fee"),
    array("col" => "avg(last_90d_od_cnt)" , "table" => "" , "as" => "last_90d_od_cnt"),
    array("col" => "avg(last_90d_od_fee)" , "table" => "" , "as" => "last_90d_od_fee"),
    array("col" => "avg(pdt_uv)" , "table" => "" , "as" => "pdt_uv"),
    array("col" => "avg(pdt_uniq_cnt)" , "table" => "" , "as" => "pdt_uniq_cnt"),
    array("col" => "avg(fav_shop_cnt)" , "table" => "" , "as" => "fav_shop_cnt"),
    array("col" => "avg(fav_shop_uv_cnt)" , "table" => "" , "as" => "fav_shop_uv_cnt"),
    array("col" => "avg(fav_pdt_cnt)" , "table" => "" , "as" => "fav_pdt_cnt"),
    array("col" => "avg(fav_pdt_uv_cnt)" , "table" => "" , "as" => "fav_pdt_uv_cnt"),
    array("col" => "avg(cart_add_cnt)" , "table" => "" , "as" => "cart_add_cnt"),
    array("col" => "avg(cart_uniq_uv)" , "table" => "" , "as" => "cart_uniq_uv"),
    array("col" => "avg(pay_uniq_pdt_cnt)" , "table" => "" , "as" => "pay_uniq_pdt_cnt"),
    array("col" => "avg(pay_gmv)" , "table" => "" , "as" => "pay_gmv"),
    array("col" => "avg(pay_dct_gmv)" , "table" => "" , "as" => "pay_dct_gmv"),
    array("col" => "avg(pay_uv)" , "table" => "" , "as" => "pay_uv"),
    array("col" => "avg(pay_pdt_cnt)" , "table" => "" , "as" => "pay_pdt_cnt"),
    array("col" => "avg(pay_od_cnt)" , "table" => "" , "as" => "pay_od_cnt"),
    array("col" => "cast(sum(pay_gmv)/sum(pay_od_cnt) as decimal(20,2))" , "table" => "" , "as" => "pay_price_per_od"),
    array("col" => "avg(add_uniq_pdt_cnt)" , "table" => "" , "as" => "add_uniq_pdt_cnt"),
    array("col" => "avg(add_gmv)" , "table" => "" , "as" => "add_gmv"),
    array("col" => "avg(add_dct_gmv)" , "table" => "" , "as" => "add_dct_gmv"),
    array("col" => "avg(add_uv)" , "table" => "" , "as" => "add_uv"),
    array("col" => "avg(add_pdt_cnt)" , "table" => "" , "as" => "add_pdt_cnt"),
    array("col" => "avg(add_od_cnt)" , "table" => "" , "as" => "add_od_cnt"),
    array("col" => "cast(sum(add_gmv)/sum(add_od_cnt) as decimal(20,2))" , "table" => "" , "as" => "add_price_per_od"),
    array("col" => "avg(good_rates_cnt)" , "table" => "" , "as" => "good_rates_cnt"),
    array("col" => "avg(eft_good_rates_cnt)" , "table" => "" , "as" => "eft_good_rates_cnt"),
    array("col" => "avg(rates_cnt)" , "table" => "" , "as" => "rates_cnt"),
    array("col" => "avg(good_rates_ratio)" , "table" => "" , "as" => "good_rates_ratio"),
    array("col" => "avg(eft_good_rates_ratio)" , "table" => "" , "as" => "eft_good_rates_ratio"),
    array("col" => "avg(eft_rates_cnt)" , "table" => "" , "as" => "eft_rates_cnt"),
    array("col" => "avg(rfd_od_cnt)" , "table" => "" , "as" => "rfd_od_cnt"),
  );

  private $select_collect_s = array(
    array("col" => "t1.day" , "table" => "" , "as" => "day"),
    array("col" => "t1.shop_id" , "table" => "" , "as" => "shop_id"),
    array("col" => "t1.shop_title" , "table" => "" , "as" => "shop_title"),
    array("col" => "t1.c1_name" , "table" => "" , "as" => "c1"),
    array("col" => "t1.c2_name" , "table" => "" , "as" => "c2"),
    array("col" => "t1.detailed_address" , "table" => "" , "as" => "detailed_address"),
    array("col" => "t1.open_days" , "table" => "" , "as" => "open_days"),
    array("col" => "t1.last_online_time" , "table" => "" , "as" => "last_online_time"),
    array("col" => "t1.online_pdt_cnt" , "table" => "" , "as" => "online_pdt_cnt"),
    array("col" => "t1.last_pdt_new_cnt" , "table" => "" , "as" => "last_pdt_new_cnt"),
    array("col" => "t2.frt_pdt_cnt" , "table" => "" , "as" => "frt_pdt_cnt"),
    array("col" => "t2.frt_add_pdt_fee" , "table" => "" , "as" => "frt_add_pdt_fee"),
    array("col" => "t2.frt_add_uv" , "table" => "" , "as" => "frt_add_uv"),
    array("col" => "t2.frt_add_od_cnt" , "table" => "" , "as" => "frt_add_od_cnt"),
    array("col" => "t2.frt_add_pdt_cnt" , "table" => "" , "as" => "frt_add_pdt_cnt"),
    array("col" => "t2.frt_add_price_per_od" , "table" => "" , "as" => "frt_add_price_per_od"),
    array("col" => "t2.frt_pay_pdt_fee" , "table" => "" , "as" => "frt_pay_pdt_fee"),
    array("col" => "t2.frt_pay_uv" , "table" => "" , "as" => "frt_pay_uv"),
    array("col" => "t2.frt_pay_od_cnt" , "table" => "" , "as" => "frt_pay_od_cnt"),
    array("col" => "t2.frt_pay_pdt_cnt" , "table" => "" , "as" => "frt_pay_pdt_cnt"),
    array("col" => "t2.frt_pay_price_per_od" , "table" => "" , "as" => "frt_pay_price_per_od"),
    array("col" => "t2.new_pay_total_fee" , "table" => "" , "as" => "new_pay_total_fee"),
    array("col" => "t2.new_u_pay_cnt" , "table" => "" , "as" => "new_u_pay_cnt"),
    array("col" => "t2.new_add_total_fee" , "table" => "" , "as" => "new_add_total_fee"),
    array("col" => "t2.new_u_add_cnt" , "table" => "" , "as" => "new_u_add_cnt"),
    array("col" => "t2.new_cart_add_cnt" , "table" => "" , "as" => "new_cart_add_cnt"),
    array("col" => "t2.new_u_cart_cnt" , "table" => "" , "as" => "new_u_cart_cnt"),
    array("col" => "t2.new_fav_add_cnt" , "table" => "" , "as" => "new_fav_add_cnt"),
    array("col" => "t2.new_u_fav_cnt" , "table" => "" , "as" => "new_u_fav_cnt"),
    array("col" => "t1.last_30d_od_cnt" , "table" => "" , "as" => "last_30d_od_cnt"),
    array("col" => "cast(t1.last_30d_od_fee as decimal(20,2))" , "table" => "" , "as" => "last_30d_od_fee"),
    array("col" => "t1.last_90d_od_cnt" , "table" => "" , "as" => "last_90d_od_cnt"),
    array("col" => "cast(t1.last_90d_od_fee as decimal(20,2))" , "table" => "" , "as" => "last_90d_od_fee"),
    array("col" => "t2.pdt_uv" , "table" => "" , "as" => "pdt_uv"),
    array("col" => "t2.pdt_uniq_cnt" , "table" => "" , "as" => "pdt_uniq_cnt"),
    array("col" => "t2.fav_shop_cnt" , "table" => "" , "as" => "fav_shop_cnt"),
    array("col" => "t2.fav_shop_uv_cnt" , "table" => "" , "as" => "fav_shop_uv_cnt"),
    array("col" => "t2.fav_pdt_cnt" , "table" => "" , "as" => "fav_pdt_cnt"),
    array("col" => "t2.fav_pdt_uv_cnt" , "table" => "" , "as" => "fav_pdt_uv_cnt"),
    array("col" => "t2.cart_add_cnt" , "table" => "" , "as" => "cart_add_cnt"),
    array("col" => "t2.cart_uniq_uv" , "table" => "" , "as" => "cart_uniq_uv"),
    array("col" => "t2.pay_uniq_pdt_cnt" , "table" => "" , "as" => "pay_uniq_pdt_cnt"),
    array("col" => "t2.pay_gmv" , "table" => "" , "as" => "pay_gmv"),
    array("col" => "t2.pay_dct_gmv" , "table" => "" , "as" => "pay_dct_gmv"),
    array("col" => "t2.pay_uv" , "table" => "" , "as" => "pay_uv"),
    array("col" => "t2.pay_pdt_cnt" , "table" => "" , "as" => "pay_pdt_cnt"),
    array("col" => "t2.pay_od_cnt" , "table" => "" , "as" => "pay_od_cnt"),
    array("col" => "t2.pay_price_per_od" , "table" => "" , "as" => "pay_price_per_od"),
    array("col" => "t2.add_uniq_pdt_cnt" , "table" => "" , "as" => "add_uniq_pdt_cnt"),
    array("col" => "t2.add_gmv" , "table" => "" , "as" => "add_gmv"),
    array("col" => "t2.add_dct_gmv" , "table" => "" , "as" => "add_dct_gmv"),
    array("col" => "t2.add_uv" , "table" => "" , "as" => "add_uv"),
    array("col" => "t2.add_pdt_cnt" , "table" => "" , "as" => "add_pdt_cnt"),
    array("col" => "t2.add_od_cnt" , "table" => "" , "as" => "add_od_cnt"),
    array("col" => "t2.add_price_per_od" , "table" => "" , "as" => "add_price_per_od"),
    array("col" => "t2.good_rates_cnt" , "table" => "" , "as" => "good_rates_cnt"),
    array("col" => "t2.eft_good_rates_cnt" , "table" => "" , "as" => "eft_good_rates_cnt"),
    array("col" => "t2.rates_cnt" , "table" => "" , "as" => "rates_cnt"),
    array("col" => "t2.good_rates_ratio" , "table" => "" , "as" => "good_rates_ratio"),
    array("col" => "t2.eft_good_rates_ratio" , "table" => "" , "as" => "eft_good_rates_ratio"),
    array("col" => "t2.eft_rates_cnt" , "table" => "" , "as" => "eft_rates_cnt"),
    array("col" => "t2.rfd_od_cnt" , "table" => "" , "as" => "rfd_od_cnt"),
  );

  private $select_collect_base = array(
    array("col" => "date" , "table" => "" , "as" => "day"),
    array("col" => "shop_id" , "table" => "" , "as" => "shop_id"),
    array("col" => "shop_title" , "table" => "" , "as" => "shop_title"),
    array("col" => "c1_name" , "table" => "" , "as" => "c1_name"),
    array("col" => "c2_name" , "table" => "" , "as" => "c2_name"),
    array("col" => "detailed_address" , "table" => "" , "as" => "detailed_address"),
    array("col" => "open_days" , "table" => "" , "as" => "open_days"),
    array("col" => "last_online_time" , "table" => "" , "as" => "last_online_time"),
    array("col" => "online_pdt_cnt" , "table" => "" , "as" => "online_pdt_cnt"),
    array("col" => "last_pdt_new_cnt" , "table" => "" , "as" => "last_pdt_new_cnt"),
    array("col" => "last_30d_od_cnt" , "table" => "" , "as" => "last_30d_od_cnt"),
    array("col" => "last_30d_od_fee" , "table" => "" , "as" => "last_30d_od_fee"),
    array("col" => "last_90d_od_cnt" , "table" => "" , "as" => "last_90d_od_cnt"),
    array("col" => "last_90d_od_fee" , "table" => "" , "as" => "last_90d_od_fee"),
  );
  private $select_collect = array(
    array("col" => "shop_id" , "table" => "" , "as" => "shop_id"),
    array("col" => "sum(frt_pdt_cnt)" , "table" => "" , "as" => "frt_pdt_cnt"),
    array("col" => "cast(sum(frt_add_pdt_fee) as decimal(20,2))" , "table" => "" , "as" => "frt_add_pdt_fee"),
    array("col" => "sum(frt_add_uv)" , "table" => "" , "as" => "frt_add_uv"),
    array("col" => "sum(frt_add_od_cnt)" , "table" => "" , "as" => "frt_add_od_cnt"),
    array("col" => "sum(frt_add_pdt_cnt)" , "table" => "" , "as" => "frt_add_pdt_cnt"),
    array("col" => "cast(sum(frt_add_pdt_fee)/sum(frt_add_od_cnt) as decimal(20,2))" , "table" => "" , "as" => "frt_add_price_per_od"),
    array("col" => "cast(sum(frt_pay_pdt_fee) as decimal(20,2))", "table" => "" , "as" => "frt_pay_pdt_fee"),
    array("col" => "sum(frt_pay_uv)" , "table" => "" , "as" => "frt_pay_uv"),
    array("col" => "sum(frt_pay_od_cnt)" , "table" => "" , "as" => "frt_pay_od_cnt"),
    array("col" => "sum(frt_pay_pdt_cnt)" , "table" => "" , "as" => "frt_pay_pdt_cnt"),
    array("col" => "cast(sum(frt_pay_pdt_fee)/sum(frt_pay_od_cnt) as decimal(20,2))" , "table" => "" , "as" => "frt_pay_price_per_od"),
    array("col" => "cast(sum(new_pay_total_fee) as decimal(20,2))" , "table" => "" , "as" => "new_pay_total_fee"),
    array("col" => "sum(new_u_pay_cnt)" , "table" => "" , "as" => "new_u_pay_cnt"),
    array("col" => "cast(sum(new_add_total_fee) as decimal(20,2))" , "table" => "" , "as" => "new_add_total_fee"),
    array("col" => "sum(new_u_add_cnt)" , "table" => "" , "as" => "new_u_add_cnt"),
    array("col" => "sum(new_cart_add_cnt)" , "table" => "" , "as" => "new_cart_add_cnt"),
    array("col" => "sum(new_u_cart_cnt)" , "table" => "" , "as" => "new_u_cart_cnt"),
    array("col" => "sum(new_fav_add_cnt)" , "table" => "" , "as" => "new_fav_add_cnt"),
    array("col" => "sum(new_u_fav_cnt)" , "table" => "" , "as" => "new_u_fav_cnt"),
    array("col" => "sum(pdt_uv)" , "table" => "" , "as" => "pdt_uv"),
    array("col" => "sum(pdt_uniq_cnt)" , "table" => "" , "as" => "pdt_uniq_cnt"),
    array("col" => "sum(fav_shop_cnt)" , "table" => "" , "as" => "fav_shop_cnt"),
    array("col" => "sum(fav_shop_uv_cnt)" , "table" => "" , "as" => "fav_shop_uv_cnt"),
    array("col" => "sum(fav_pdt_cnt)" , "table" => "" , "as" => "fav_pdt_cnt"),
    array("col" => "sum(fav_pdt_uv_cnt)" , "table" => "" , "as" => "fav_pdt_uv_cnt"),
    array("col" => "sum(cart_add_cnt)" , "table" => "" , "as" => "cart_add_cnt"),
    array("col" => "sum(cart_uniq_uv)" , "table" => "" , "as" => "cart_uniq_uv"),
    array("col" => "sum(pay_uniq_pdt_cnt)" , "table" => "" , "as" => "pay_uniq_pdt_cnt"),
    array("col" => "cast(sum(pay_gmv) as decimal(20,2))" , "table" => "" , "as" => "pay_gmv"),
    array("col" => "cast(sum(pay_dct_gmv) as decimal(20,2))" , "table" => "" , "as" => "pay_dct_gmv"),
    array("col" => "sum(pay_uv)" , "table" => "" , "as" => "pay_uv"),
    array("col" => "sum(pay_pdt_cnt)" , "table" => "" , "as" => "pay_pdt_cnt"),
    array("col" => "sum(pay_od_cnt)" , "table" => "" , "as" => "pay_od_cnt"),
    array("col" => "cast(sum(pay_gmv)/sum(pay_od_cnt) as decimal(20,2))" , "table" => "" , "as" => "pay_price_per_od"),
    array("col" => "sum(add_uniq_pdt_cnt)" , "table" => "" , "as" => "add_uniq_pdt_cnt"),
    array("col" => "cast(sum(add_gmv) as decimal(20,2))" , "table" => "" , "as" => "add_gmv"),
    array("col" => "cast(sum(add_dct_gmv) as decimal(20,2))" , "table" => "" , "as" => "add_dct_gmv"),
    array("col" => "sum(add_uv)" , "table" => "" , "as" => "add_uv"),
    array("col" => "sum(add_pdt_cnt)" , "table" => "" , "as" => "add_pdt_cnt"),
    array("col" => "sum(add_od_cnt)" , "table" => "" , "as" => "add_od_cnt"),
    array("col" => "cast(sum(add_gmv)/sum(add_od_cnt) as decimal(20,2))" , "table" => "" , "as" => "add_price_per_od"),
    array("col" => "sum(good_rates_cnt)" , "table" => "" , "as" => "good_rates_cnt"),
    array("col" => "sum(eft_good_rates_cnt)" , "table" => "" , "as" => "eft_good_rates_cnt"),
    array("col" => "sum(rates_cnt)" , "table" => "" , "as" => "rates_cnt"),
    array("col" => "cast(sum(good_rates_ratio) as decimal(20,2))" , "table" => "" , "as" => "good_rates_ratio"),
    array("col" => "cast(sum(eft_good_rates_ratio) as decimal(20,2))" , "table" => "" , "as" => "eft_good_rates_ratio"),
    array("col" => "sum(eft_rates_cnt)" , "table" => "" , "as" => "eft_rates_cnt"),
    array("col" => "sum(rfd_od_cnt)" , "table" => "" , "as" => "rfd_od_cnt"),
  );

  public function action_download() {
    $options = $this->request->query();
    $yesterday = date("Ymd",time()-24*60*60);
    if($options['detail_or_collect']=="collect"){
     if (isset($options['cname1'])){
      $ary_c1=array();
      $ary_c1=explode(",",$options['cname1']);
      $query_options['cname1']= $ary_c1;
     }
    }else{
     if (isset($options['cname1'])){
      $query_options['cname1']=$options['cname1'];
     }
    }
    if (isset($options['date_start'])) $query_options['shop_date_interval']['start'] = $options['date_start'];
    if (isset($options['date_end'])) $query_options['shop_date_interval']['end'] = $options['date_end'];
   // if (isset($options['cname1'])) $query_options['cname1'] = $options['cname1'] ;
    if (isset($options['cname2'])) $query_options['cname2'] = $options['cname2'] ;
    if (isset($options['shop_id_list'])) $query_options['shop_id_list'] = $options['shop_id_list'] ;
    if (isset($options['shop_name'])) $query_options['shop_name'] = $options['shop_name'] ;
    if (isset($options['detail_or_collect'])) $query_options['detail_or_collect'] = $options['detail_or_collect'] ;
    if (isset($options['detailchoose'])) $query_options['detailchoose'] = $options['detailchoose'] ;

    $res_data = $this->get_result($query_options);

    /*$cid1_cid2_name = DB::query(Database::SELECT,"select c1,c1_name,c2,c2_name from cc_mid_category_l1_l2 where date = ".$yesterday)->execute()->as_array();
    for ($i = 0; $i < count($res_data); $i++) {
      foreach ($cid1_cid2_name as $category) {
        if ($res_data[$i]['c1'] == $category['c1']) $res_data[$i]['c1'] = $category['c1_name'];
        if ($res_data[$i]['c2'] == $category['c2']) $res_data[$i]['c2'] = $category['c2_name'];
    }
   }*/
    $rows = count($res_data);

    $this->response->headers("Content-type", "text/csv");
    $this->response->headers("Content-Disposition", "attachment; filename=download.csv");
    $this->response->headers("Cache-Control", "must-revalidate,post-check=0,pre-check=0");
    $this->response->headers("Expires", "0");
    $this->response->headers("Pragma", "public");
    $str = "日期, 店铺ID, 店铺名称, 一级类目, 二级类目, 所在地（物流）, 开店时长, 最近一次上架（首发）时间, 在线商品数, 最近上新款的商品数, 首发商品数, 首发商品下单金额, 首发商品下单uv, 首发商品下单笔数, 首发商品下单件数, 首发商品下单客单, 首发商品付款金额, 首发商品付款UV, 首发商品付款笔数, 首发商品付款件数, 首发商品付款客单价, 新客付款金额, 新客付款UV, 新客下单金额, 新客下单UV, 新客加购次数, 新客加购UV, 新客收藏次数, 新客收藏UV,近30天付款单数, 近30天付款金额, 近90天付款单数, 近90天付款金额,IPV-UV, 被浏览商品数,店铺收藏次数, 店铺收藏UV, 商品收藏次数, 商品收藏UV, 商品加购次数, 商品加购UV, 付款商品数, 付款金额, 付款用户实际支付金额, 付款UV, 付款件数, 付款笔数, 付款客单价, 下单商品数, 下单金额, 下单用户实际支付金额,下单UV, 下单件数, 下单笔数, 下单客单价, 好评数, 有效好评数, 评价数, 好评率, 有效好评率, 有效评价数, 退款订单数\n";
    $str = iconv('utf-8','gb2312//IGNORE',$str);
    echo $str;
    foreach ($res_data as $item){
      $str = implode(',',$item);
      $str = iconv('utf-8','gb2312//IGNORE',$str."\n");
      echo $str;
    }
  }


  public function action_shopgraph(){
    $query_options = $this->request->query();
    $page_item_count = $query_options['page_item_count'];

    $res = $this->get_result($query_options['shop_options']);

    $ret['code'] = 0;
    $ret['data'] = $res;
    exit(json_encode($ret));
  }

  public function action_categorygraph(){
    $query_options = $this->request->query();
    $page_item_count = $query_options['page_item_count'];

    if (!isset($query_options['shop_options']['cname1'])){
      $sql = "select sc1_name,sc2_name from cc_stat_top_shop where shop_id = ".$query_options['shop_options']['shop_id_list']." and date = ".date("Ymd",strtotime("-1 day"));
      $res = DB::query(Database::SELECT,$sql)->execute()->as_array();
      $query_options['shop_options']['cname1'] = $res['0']['sc1_name'];
      $query_options['shop_options']['cname2'] = $res['0']['sc2_name'];
    }
    $options['shop_date_interval'] = $query_options['shop_options']['shop_date_interval'];
    if (isset($query_options['shop_options']['cname1'])) $options['cname1'] = $query_options['shop_options']['cname1'];
    if (isset($query_options['shop_options']['cname2'])) $options['cname2'] = $query_options['shop_options']['cname2'];
    $options['detail_or_collect'] = "avg";
    $res = $this->get_result($options);

    $ret['code'] = 0;
    $ret['data'] = $res;
    exit(json_encode($ret));
  }

  public function action_col(){
   // $query_options = $this->request-query();
    $res = DB::query(Database::SELECT,"select * from cc_stat_shop_col")->execute()->as_array();
    $ret['code'] = 0;
    $ret['data'] = $res;
    //echo $res[2];exit;
    exit(json_encode($ret));
  }

  public function action_page(){
    $query_options = $this->request->query();
    $options=$query_options['shop_options'];
    $page_item_count = $query_options['count_per_page'];
    $page = $query_options['page'];
    $page_begin = ($page-1)*$page_item_count;
    $yesterday = date("Ymd", time() - 24 * 60 * 60);

    $user_id = ORM::factory("user")->where('email', '=', Auth::instance()->get_user())->find()->id;
    $insert_query_sql = new InsertQuerys;
    $id = $insert_query_sql->insert_query($user_id,'','Industry_Shops');

    $res_data = $this->get_result($query_options['shop_options']);


    /*$cid1_cid2_name = DB::query(Database::SELECT,"select c1,c1_name,c2,c2_name from cc_mid_category_l1_l2 where date = ".$yesterday)->execute()->as_array();
     for ($i = 0; $i < count($res_data); $i++){
      foreach ($cid1_cid2_name as $category) {
       if ($res_data[$i]['c1'] == $category['c1']) $res_data[$i]['c1'] = $category['c1_name'];
       if ($res_data[$i]['c2'] == $category['c2']) $res_data[$i]['c2'] = $category['c2_name'];
     }
    }*/
 
   if($options['detail_or_collect'] == "collect"){
    if($options['detailchoose'] == "1"){
      for ($i = 0; $i < count($res_data); $i++) {
       $res_data[$i]['c2'] = "--";
      }
     }else if($options['detailchoose'] == "2"){
      for ($i = 0; $i < count($res_data); $i++) {
       $res_data[$i]['c1'] = "--";
     }      
    }  
   } 

    $rows = count($res_data);
    $res = array_slice($res_data,$page_begin,$page_item_count);

    $ret['code'] = 0;
    $ret['data'] = $res;
    $ret['rows'] = $rows;
    $ret['page_item_count'] = $page_item_count;
    $ret['page'] = $page;
    exit(json_encode($ret));
  }
  public function action_search(){
    $yesterday = date("Ymd", time() - 24 * 60 * 60);
    $query_options = $this->request->query();
    $options=$query_options['shop_options'];
    $page_item_count = $query_options['page_item_count'];

    /* if($options['detail_or_collect']=='collect'){
     if($this->has_c1_option($options)){
       if(count($options['cname1'])==1){
       $options['cname1']=$this->get_c1_option($options);
      }else{
      $options['cname1']=$this->get_c1_more_option($options);
      }     
     }
    }*/

    $user_id = ORM::factory("user")->where('email', '=', Auth::instance()->get_user())->find()->id;
    $insert_query_sql = new InsertQuerys;
    $id = $insert_query_sql->insert_query($user_id,'','Industry_Shops');
    
    $res_data = $this->get_result($options);
    
    /*$cid1_cid2_name = DB::query(Database::SELECT,"select c1,c1_name,c2,c2_name from cc_mid_category_l1_l2 where date = ".$yesterday)->execute()->as_array();
     for ($i = 0; $i < count($res_data); $i++) {
     foreach ($cid1_cid2_name as $category) {
       if ($res_data[$i]['c1'] == $category['c1']) $res_data[$i]['c1'] = $category['c1_name'];
       if ($res_data[$i]['c2'] == $category['c2']) $res_data[$i]['c2'] = $category['c2_name'];
     }  
    }*/
    
   if($options['detail_or_collect'] == "collect"){
     if($options['detailchoose'] == "1"){ 
      for ($i = 0; $i < count($res_data); $i++) {
        $res_data[$i]['c2'] = "--";
      }
     }else if($options['detailchoose'] == "2"){
      for ($i = 0; $i < count($res_data); $i++) {
        $res_data[$i]['c1'] = "--";
     }      
    }     
   }
    $rows = count($res_data);
    $res = array_slice($res_data,0,$page_item_count);
     
   
    $ret['code'] = 0;
    $ret['data'] = $res;
    $ret['rows'] = $rows;
    $ret['page_item_count'] = $page_item_count;
    exit(json_encode($ret));
  }

  private function get_result($options){
    $res_data="";
    if($options['detail_or_collect'] == "collect"){
      $res_data = DB::query(Database::SELECT,$this->build_sql($options,"shop_collect")." order by shop_id")->execute()->as_array();
    }
    else if($options['detail_or_collect'] == "detail"){
      $res_data = DB::query(Database::SELECT,$this->build_sql($options,"shop_detail"))->execute()->as_array();
    }
    else if($options['detail_or_collect'] == "avg"){
      $res_data = DB::query(Database::SELECT,$this->build_sql($options,"shop_avg"))->execute()->as_array();
    }

    return $res_data;
  }

  private function get_category(){
    return "select c2,c1_name,c2_name from cc_mid_category_l1_l2 ";
  }

  private function build_sql($options,$table_name){
    $begin_s = $options['shop_date_interval']['start'];
    $end_s = $options['shop_date_interval']['end'];
    $where_cond = array();
    $where_cond_base = array();    
   // print_r($options);exit;
    if($this->has_c1_option($options)){
     $c1_value=$options['cname1'];
     $c1_length=count($options['cname1']);
    }

    if($this->has_c2_option($options)){
      $cid2 = DB::query(Database::SELECT,"select c2 from cc_mid_category_l1_l2 where c2_name = ".$this->get_c2_option($options))->execute()->as_array();
      $where_cond[] =
        array("c2","=",$cid2[0]['c2']);
      $where_cond_base[] =
        array("c2","=",$cid2[0]['c2']); 
    }
    if($this->has_c1_option($options)){
     if($options['detail_or_collect'] == "collect"){
      if(count($options['cname1'])==1){
      $cid1 = DB::query(Database::SELECT,"select c1 from cc_mid_category_l1_l2 where c1_name = ".$this->get_c1_option($options)." limit 1")->execute()->as_array();
      $where_cond[] =
        array("c1","=",$cid1[0]['c1']);
       $where_cond_base[] =
        array("c1","=",$cid1[0]['c1']);
     }else if(count($options['cname1'])>1){
      $cid3 = DB::query(Database::SELECT,"select c1 from cc_mid_category_l1_l2 where c1_name in ".$this->get_c1_more_option($options))->execute()->as_array();
      $where_cond[] =
        array("c1","in",$this->get_c1_more($cid3));
      $where_cond_base[] =
        array("c1","in",$this->get_c1_more($cid3));
     }
    }else{
      $cid4 = DB::query(Database::SELECT,"select c1 from cc_mid_category_l1_l2 where c1_name = ".$this->get_c1_detail_option($options)." limit 1")->execute()->as_array();
      $where_cond[] =
        array("c1","=",$cid4[0]['c1']);
      $where_cond_base[] =
        array("c1","=",$cid4[0]['c1']);
     }
   }

    if ($this->has_shop_id($options)){
      $where_cond[] =
        array("shop_id","in",$this->get_shop_id($options));
      $where_cond_base[] =
        array("shop_id","in",$this->get_shop_id($options));
    }
    if ($this->has_shop_name($options)){
      $where_cond[] =
        array("shop_title","like",$this->get_shop_name($options));
      $where_cond_base[] =
        array("shop_title","like",$this->get_shop_name($options));
    }
    $where_cond[] =
      array("date",">= from_unixtime(",$begin_s.",'%Y%m%d')");
    $where_cond[] =
      array("date","<= from_unixtime(",$end_s.",'%Y%m%d')");
    $where_cond_base[] =
      array("date","= from_unixtime(",$end_s.",'%Y%m%d')");

    $group_by_cond = array("shop_id");
    $group_by_cond_c1=array("c1");
    $group_by_cond_c1_c2=array("c1","c2");

    $group_by_cond_avg = array("date");

    $order_by_cond = array("date","shop_id");
    $order_by_cond_avg = array("date");
    $order_by_cond_collect = array("shop_id");

    if ($options['detail_or_collect'] == "collect"){
      if($options['detailchoose']=="0"){
        $s_rpt_shop_collect = new SqlBuilder;
        $this->select_collect_base[0]['col'] ="'".date('Ymd',$begin_s)."~".date('Ymd',$end_s)."'";
        $s_rpt_shop_collect->select($this->select_collect);
        $s_rpt_shop_collect->from("cc_rpt_shop_summary","");
        $s_rpt_shop_collect->where($where_cond);
        $s_rpt_shop_collect->group_by($group_by_cond);
        $s_rpt_shop_collect->order_by($order_by_cond_collect);
      }else if($options['detailchoose']=="1"){//group_by一级类目
        $s_rpt_shop_collect = new SqlBuilder;
        $s_rpt_shop_collect->select($this->select_collect);
        $s_rpt_shop_collect->from("cc_rpt_shop_summary","");
        $s_rpt_shop_collect->where($where_cond);
        $s_rpt_shop_collect->group_by($group_by_cond_c1);
        $s_rpt_shop_collect->order_by($order_by_cond_collect);
      }else{//group_by二级类目
        $s_rpt_shop_collect = new SqlBuilder;
        $s_rpt_shop_collect->select($this->select_collect);
        $s_rpt_shop_collect->from("cc_rpt_shop_summary","");
        $s_rpt_shop_collect->where($where_cond);
        $s_rpt_shop_collect->group_by($group_by_cond_c1_c2);
        $s_rpt_shop_collect->order_by($order_by_cond_collect);
      }
    }
    else if ($options['detail_or_collect'] == "detail"){
      $s_rpt_shop_detail = new SqlBuilder;
      $s_rpt_shop_detail->select($this->select_detail);
      $s_rpt_shop_detail->from("cc_rpt_shop_summary","");
      $s_rpt_shop_detail->where($where_cond);
      $s_rpt_shop_detail->order_by($order_by_cond);
    }
    else if ($options['detail_or_collect'] == "avg"){
      $s_rpt_shop_avg = new SqlBuilder;
      $s_rpt_shop_avg->select($this->select_avg);
      $s_rpt_shop_avg->from("cc_rpt_shop_summary","");
      $s_rpt_shop_avg->where($where_cond);
      $s_rpt_shop_avg->group_by($group_by_cond_avg);
      $s_rpt_shop_avg->order_by($order_by_cond_avg);
    }

    // print_r($s_base_info);exit;
    if ($table_name == "shop_detail" ) return $s_rpt_shop_detail->build();
    if ($table_name == "shop_collect" ){
       $rpt_shop_collect = new SqlBuilder;
       $rpt_shop_collect->select($this->select_collect_s);

       $rpt_shop_collect_base = new SqlBuilder;
       $rpt_shop_collect_base->select($this->select_collect_base);
       $rpt_shop_collect_base->from("cc_rpt_shop_summary","");
       $rpt_shop_collect_base->where($where_cond_base);
 
       $sql_collect=$rpt_shop_collect->build()."from (".$rpt_shop_collect_base->build().") t1 left join (".$s_rpt_shop_collect->build().") t2 on t1.shop_id=t2.shop_id";
       return $sql_collect;
    }
    if ($table_name == "shop_avg" ) return $s_rpt_shop_avg->build();
  }

  private function has_c1_option($options){
    return isset($options['cname1']);
  }

  private function get_c1_option($options){
    return "'".$options['cname1'][0]."'";
  }
  
  private function get_c1_detail_option($options){
    return "'".$options['cname1']."'";
  }

  private function get_c1_more_option($options)
  {  
    $ary=array();
    for($i=0;$i<count($options['cname1']);$i++){
       $cur=$options['cname1'][$i];
       $cur_str="'".$cur."'";
      array_push($ary,$cur_str);  
      }
      $str=implode(',', $ary);
      return "(".$str.")";
    
  }
  private function get_c1_more($options)
  {
    $ary=array();
    for($i=0;$i<count($options);$i++){
      $cur=$options[$i]['c1'];
      $cur_str="'".$cur."'";
      array_push($ary,$cur_str);
    }
    $str=implode(',', $ary);
    return "(".$str.")";

  }
  private function has_c2_option($options){
    return isset($options['cname2']);
  }

  private function get_c2_option($options){
    return "'".$options['cname2']."'";
  }

  private function has_shop_id($options){
    return isset($options['shop_id_list']);
  }

  private function get_shop_id($options){
    return "(".$options['shop_id_list'].")";
  }

  private function has_shop_name($options){
    return isset($options['shop_name']);
  }

  private function get_shop_name($options){
    return "'%".$options['shop_name']."%'";
  }


}
