<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Shops_Shopweekly extends Controller {

  private $select_shop = array(
    array("col" => "date" , "table" => "" , "as" => "date"),
    array("col" => "shop_title" , "table" => "" , "as" => "shop_title"),
    array("col" => "shop_id" , "table" => "" , "as" => "shop_id"),
    array("col" => "shop_cname1" , "table" => "" , "as" => "shop_cname1"),
    array("col" => "shop_cname2" , "table" => "" , "as" => "shop_cname2"),
   // array("col" => "shop_cname1" , "table" => "" , "as" => "shop_cname1"),
   // array("col" => "shop_cname2" , "table" => "" , "as" => "shop_cname1"),
    array("col" => "pdt_uv" , "table" => "" , "as" => "pdt_uv"),
    array("col" => "pv" , "table" => "" , "as" => "pv"),
    array("col" => "cast(pay_gmv as decimal(15,2))" , "table" => "" , "as" => "pay_gmv"),
    array("col" => "pay_uv" , "table" => "" , "as" => "pay_uv"),
    array("col" => "pay_od_cnt" , "table" => "" , "as" => "pay_od_cnt"),
    array("col" => "cast(pay_price_per_od as decimal(15,2))" , "table" => "" , "as" => "pay_price_per_od"),
    array("col" => "cast(add_gmv as decimal(15,2))" , "table" => "" , "as" => "add_gmv"),
    array("col" => "add_uv" , "table" => "" , "as" => "add_uv"),
    array("col" => "add_od_cnt" , "table" => "" , "as" => "add_od_cnt"),
    array("col" => "cast(add_price_per_od as decimal(15,2))" , "table" => "" , "as" => "add_price_per_od"),
    array("col" => "CONCAT(cast(percent_ratio*100 as decimal(15,6)),'%')" , "table" => "" , "as" => "percent_ratio"),
    array("col" => "CONCAT(cast(eft_good_rates_ratio*100 as decimal(15,2)),'%')" , "table" => "" , "as" => "eft_good_rates_ratio"),
  );

  public function action_download() {
    $options = $this->request->query();
    if (isset($options['date_start'])) $query_options['shop_date_interval']['start'] = $options['date_start'];
    if (isset($options['date_end'])) $query_options['shop_date_interval']['end'] = $options['date_end'];
    if (isset($options['cname1'])) $query_options['cname1'] = $options['cname1'] ;
    if (isset($options['cname2'])) $query_options['cname2'] = $options['cname2'] ;
    if (isset($options['shop_id'])) $query_options['shop_id_list'] = $options['shop_id'] ;
    if (isset($options['shop_name'])) $query_options['shop_name'] = $options['shop_name'] ;

    $res_data ="";
    $final_sql = $this->build_sql($query_options);
    $res_data = DB::query(Database::SELECT,$final_sql." order by date")->execute()->as_array();

    $this->response->headers("Content-type", "text/csv");
    $this->response->headers("Content-Disposition", "attachment; filename=download.csv");
    $this->response->headers("Cache-Control", "must-revalidate,post-check=0,pre-check=0");
    $this->response->headers("Expires", "0");
    $this->response->headers("Pragma", "public");
    $str = "日期, 店铺名称, 店铺ID, 一级类目, 二级类目, UV, PV, 付款金额, 付款UV, 付款笔数, 付款客单价, 下单金额, 下单UV,  下单笔数, 下单客单价, 转化率, 有效好评率\n";
    $str = iconv('utf-8','gb2312//IGNORE',$str);
    echo $str;
    foreach ($res_data as $item){
      $str = implode(',',$item);
      $str = iconv('utf-8','gb2312//IGNORE',$str."\n");
      echo $str;
    }
  }


  public function action_page(){
    $query_options = $this->request->query();
    $page_item_count1 = $query_options['page_item_count'];
    $page = $query_options['page'];
    $page_begin = ($page-1)*$page_item_count1;
    $rows = $query_options['rows'];
    //print_r ($query_options); exit();
    $user_id = ORM::factory("user")->where('email', '=', Auth::instance()->get_user())->find()->id;
    $insert_query_sql = new InsertQuerys;
    $id = $insert_query_sql->insert_query($user_id,'','Industry_Shops_Weekly');

    $final_sql = $this->build_sql($query_options['shop_options']);
    $res = DB::query(Database::SELECT,$final_sql." order by date limit ".$page_begin.",".$page_item_count1)->execute()->as_array();
    //print_r ($res); exit();
    //$rows = count($res);
    $ret['code'] = 0;
    $ret['data'] = $res;
    $ret['rows'] = $rows;
    $ret['page_item_count'] = $page_item_count1;
    $ret['page'] = $page;
    exit(json_encode($ret));
  }
  public function action_search(){

    $query_options = $this->request->query();
    $page_item_count1 = $query_options['page_item_count'];
    //print_r ( $page_item_count); exit();
    $user_id = ORM::factory("user")->where('email', '=', Auth::instance()->get_user())->find()->id;
    $insert_query_sql = new InsertQuerys;
    $id = $insert_query_sql->insert_query($user_id,'','Industry_Shops_Weekly');

    $final_sql = $this->build_sql($query_options['shop_options']);
    $res = DB::query(Database::SELECT,$final_sql." order by date")->execute()->as_array();
    $rows = count($res);
    $ress=array_slice($res,0,$page_item_count1);
    $ret['code'] = 0;
    $ret['data'] = $ress;
    $ret['rows'] = $rows;
    $ret['page_item_count'] = $page_item_count1;
    exit(json_encode($ret));
  }



  private function build_sql($options){
    $s_shop_sql = new SqlBuilder;
    $begin_s = $options['shop_date_interval']['start'];
    $end_s = $options['shop_date_interval']['end'];
    $where_cond = array();

    if ($this->has_c2_option($options)){
      $cid2 = DB::query(Database::SELECT,"select c2 from cc_mid_category_l1_l2 where c2_name = ".$this->get_c2_option($options))->execute()->as_array();
      $where_cond[] =
        array("c2","=",$cid2[0]['c2']);
    }
    if ($this->has_c1_option($options)){
      $cid1 = DB::query(Database::SELECT,"select c1 from cc_mid_category_l1_l2 where c1_name = ".$this->get_c1_option($options)." limit 1")->execute()->as_array();
      $where_cond[] =
        array("c1","=",$cid1[0]['c1']);
    }
    if ($this->has_shop_id($options))
      $where_cond[] =
      array("shop_id","in",$this->get_shop_id($options));
    if ($this->has_shop_name($options))
      $where_cond[] =
      array("shop_title","like",$this->get_shop_name($options));

    $where_cond[] =
      array("date",">= from_unixtime(",$begin_s.",'%Y%m%d')");
    $where_cond[] =
      array("date","<= from_unixtime(",$end_s.",'%Y%m%d')");

    $s_shop_sql->select($this->select_shop);
    $s_shop_sql->from("cc_stat_shop_weekly","");
    $s_shop_sql->where($where_cond);

    $return_shop_sql = $s_shop_sql->build();
    return $return_shop_sql;
  }

  private function has_c1_option($options){
    return isset($options['cname1']);
  }

  private function get_c1_option($options){
    return "'".$options['cname1']."'";
  }

  private function has_c2_option($options){
    return isset($options['cname2']);
  }

  private function get_c2_option($options){
    return "'".$options['cname2']."'";
  }

  private function has_shop_id($options){
    return isset($options['shop_id_list']);
  }

  private function get_shop_id($options){
    return "(".$options['shop_id_list'].")";
  }

  private function has_shop_name($options){
    return isset($options['shop_name']);
  }

  private function get_shop_name($options){
    return "'%".$options['shop_name']."%'";
  }


}
