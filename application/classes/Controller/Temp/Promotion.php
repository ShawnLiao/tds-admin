<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Temp_Promotion extends Controller {
  public function action_overview() {
    $model = Model::factory('Promotion');

    $ret = array();
    $ret['order_pay'] = $model->get_gmv_pay("20150902", "20150912");
    $ret['code'] = 0;

    echo json_encode($ret);
  }

}
