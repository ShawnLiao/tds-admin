<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Temp_Realtime extends Controller {

  private $charts = array("gmv", "acc_gmv", "pay_gmv", "acc_pay_gmv", "order", "acc_order", "price", "acc_price");

  public $email_list = array(
    // 数据组
    "tangy@culiu.org",
    "wangyj@culiu.org",
    "weihl@chuchujie.com",
    "zhoul@culiu.org",
    "liaoyx@chuchujie.com",
    "guosw@chuchujie.com",
    "yanghz@chuchujie.com",

    // 管理层
    "lvjj@culiu.org",
    "zhangc@culiu.org",
    "happy@culiu.org",
    "bojc@culiu.org",
    "baiss@culiu.org",

    // 产品
    "zhongc@culiu.org",
    "guoc@culiu.org",
    "wangw@culiu.org",

    // 其他
    "mengk@culiu.org",
  );

  public function action_order() {
    //if (!in_array(Auth::instance()->get_user(), $this->email_list)) {
      //$ret = array();
      //$ret['code'] = 1;
      //$ret['msg'] = "您没有权限访问该数据，申请权限，请联系王宇杰";
      //echo json_encode($ret);
      //exit;
    //}

    $model = Model::factory('Realtime_Order');

    $ret = $this->init();

    //$this->build_data($model, $ret, $this->date('20150909'), '20150909', '2015-09-09（909）', false);
    //$this->build_data($model, $ret, $this->date('20150910'), '20150910', '2015-09-10（910）', false);
    //$this->build_data($model, $ret, $this->date('20150911'), '20150911', '2015-09-11（911）', false);
    //$this->build_data($model, $ret, $this->date('20151009'), '20151009', '2015-10-09（1009）', false);
    //$this->build_data($model, $ret, $this->date('20151109'), '20151109', '2015-11-09（1109）', false);
    //$this->build_data($model, $ret, $this->date('20151111'), '20151111', '2015-11-11（1111）', false);
    //$this->build_data($model, $ret, $this->lastweek(), $this->str_lastweek(), $this->label_lastweek(), false);
    //$this->build_data($model, $ret, $this->last11(), $this->str_last11(), $this->label_last11(), false);
    //$this->build_data($model, $ret, $this->now99(), $this->str_now99(), $this->label_now99(), false);
    $this->build_data($model, $ret, $this->yestoday(), $this->str_yestoday(), $this->label_yestoday(), false);
    $this->build_data($model, $ret, $this->today(), $this->str_today(), $this->label_today(), true);

    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function date($d) {
    return strtotime($d);
  }

  public function today() {
    return strtotime(date('Y-m-d', time()));
  }

  public function yestoday() {
    return strtotime(date('Y-m-d', time() - 86400));
  }

  public function lastweek() {
    return strtotime(date('Y-m-d', time() - 86400 * 7));
  }

  public function now99() {
    return strtotime(date('Y-m-d', time() - 86400 * 63));
  }

  public function last11() {
    return strtotime(date('Y-m-d', time() - 86400 * 366));
  }

  public function str_today() {
    return date('Ymd', time());
  }

  public function str_yestoday() {
    return date('Ymd', time() - 86400);
  }

  public function str_lastweek() {
    return date('Ymd', time() - 86400 * 7);
  }

  public function str_now99() {
    return date('Ymd', time() - 86400 * 63);
  }

  public function str_last11() {
    return date('Ymd', time() - 86400 * 366);
  }

  public function label_today() {
    return date('Y-m-d', time()) . "（今日）";
  }

  public function label_yestoday() {
    return date('Y-m-d', time() - 86400) . "（昨日）";
  }

  public function label_lastweek() {
    return date('Y-m-d', time() - 86400 * 7) . "（上周）";
  }

  public function label_now99() {
    return date('Y-m-d', time() - 86400 * 63) . "（16年99）";
  }

  public function label_last11() {
    return date('Y-m-d', time() - 86400 * 366) . "（15年双12）";
  }

  public function init() {
    $ret = array();

    foreach ($this->charts as $chart) {
      $ret[$chart] = array('timestamp' => array(), 'series' => array());
    }

    $today = strtotime(date('Y-m-d', time()));
    for ($i = 0; $i < 288; $i++) {
      foreach ($this->charts as $chart) {
        $ret[$chart]['timestamp'][] = date('H;i', $today + $i * 300);
      }
    }

    return $ret;
  }

  public function init_past_data(&$data) {
    for ($i = 0; $i < 288; $i++) {
      $data[] = 0;
    }
  }

  public function init_today_data(&$data, $len) {
    for ($i = 0; $i < $len; $i++) {
      $data[] = 0;
    }
  }

  public function build_data($model, &$ret, $date, $str_date, $label, $is_today) {
    $query_result = $model->get_realtime_data($str_date);

    $series = array();

    foreach ($this->charts as $chart) {
      $series[$chart] = array('name' => $label, 'data' => array());
      if ($is_today) {
        $len = (end($query_result)['beg_ts'] - $this->today()) / 300 + 1;
        $this->init_today_data($series[$chart]['data'], $len);
      } else {
        $this->init_past_data($series[$chart]['data']);
      }
    }

    foreach ($query_result as $result) {
      $idx = (intval($result['beg_ts']) - $date) / 300;

      $series['gmv']['data'][$idx] = floatval($result['total_fee']);
      $series['pay_gmv']['data'][$idx] = floatval($result['pay_fee']);
      $series['order']['data'][$idx] = floatval($result['order_count']);
      if ($series['order']['data'][$idx] == 0) {
        $series['price']['data'][$idx] = 0;
      } else {
        $series['price']['data'][$idx] = $series['gmv']['data'][$idx] / $series['order']['data'][$idx];
      }
    }

    for ($i = 0; $i < count($series['gmv']['data']); $i++) {
      if (0 == $i) {
        $series['acc_gmv']['data'][$i] = $series['gmv']['data'][$i];
        $series['acc_pay_gmv']['data'][$i] = $series['pay_gmv']['data'][$i];
        $series['acc_order']['data'][$i] = $series['order']['data'][$i];
      } else {
        $series['acc_gmv']['data'][$i] = $series['acc_gmv']['data'][$i - 1] + $series['gmv']['data'][$i];
        $series['acc_pay_gmv']['data'][$i] = $series['acc_pay_gmv']['data'][$i - 1] + $series['pay_gmv']['data'][$i];
        $series['acc_order']['data'][$i] = $series['acc_order']['data'][$i - 1] + $series['order']['data'][$i];
      }
      if ($series['acc_order']['data'][$i] == 0) {
        $series['acc_price']['data'][$i] = 0;
      } else {
        $series['acc_price']['data'][$i] = $series['acc_gmv']['data'][$i] / $series['acc_order']['data'][$i];
      }
    }
    foreach ($this->charts as $chart) {
      $ret[$chart]['series'][] = $series[$chart];
    }
  }
}
