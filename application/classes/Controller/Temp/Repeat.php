<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Temp_Repeat extends Controller {

  private $charts = array(
    "rpr1" =>   array('name' => '次日复购率',   'days' => 1),
    "rpr2" =>   array('name' => '二日复购率',   'days' => 2),
    "rpr3" =>   array('name' => '三日复购率',   'days' => 3), 
    "rpr4" =>   array('name' => '四日复购率',   'days' => 4), 
    "rpr5" =>   array('name' => '五日复购率',   'days' => 5), 
    "rpr6" =>   array('name' => '六日复购率',   'days' => 6), 
    "rpr7" =>   array('name' => '七日复购率',   'days' => 7), 
    "rpr14" =>  array('name' => '十四日复购率', 'days' => 14),
    "rpr30" =>  array('name' => '三十日复购率', 'days' => 30)
  );

  public function action_all() {
    $model = Model::factory('Repeat_Stat');
  
    $ret = $this->init();

    $this->build_data_all($model, $ret);

    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function action_new() {
    $model = Model::factory('Repeat_Stat');
  
    $ret = $this->init();

    $this->build_data_new($model, $ret);

    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function init() {
    $ret = array();

    foreach ($this->charts as $key => $value) {
      $ret[$key] = array('date' => array(), 'series' => array());
    }

    return $ret;
  }

  public function action_theme() {
    $model = Model::factory('Repeat_Stat');

    $ret = $this->init();

    $this->build_data_theme($model, $ret);

    $ret['code'] = 0;
    echo json_encode($ret);
  }
  
  public function action_noted() {
    $model = Model::factory('Repeat_Stat');

    $ret = $this->init();

    $this->build_data_noted($model, $ret);

    $ret['code'] = 0;
    echo json_encode($ret);
  }
  
  public function action_single() {
    $model = Model::factory('Repeat_Stat');

    $ret = $this->init();

    $this->build_data_single($model, $ret);

    $ret['code'] = 0;
    echo json_encode($ret);
  }
  
  public function action_99repurchase() {
    $model = Model::factory('Repeat_Stat');
    $query_options = $this->request->query();
    $start = $query_options['options']['date_interval']['start'];
    $end = $query_options['options']['date_interval']['end'];
    $start = date('Ymd',$start);
    $end = date('Ymd',$end);
    $ret = $this->init();

    $this->build_data_99repurchase($model, $ret,$start,$end);

    $ret['code'] = 0;
    echo json_encode($ret);
  }
  
  public function action_snapuprepurchase() {
    $model = Model::factory('Repeat_Stat');
    $query_options = $this->request->query();
    $start = $query_options['options']['date_interval']['start'];
    $end = $query_options['options']['date_interval']['end'];
    $start = date('Ymd',$start);
    $end = date('Ymd',$end);

    $ret = $this->init();

    $this->build_data_snapuprepurchase($model, $ret,$start,$end);

    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function build_data_all($model, &$ret) {
    foreach ($this->charts as $key => $value) {
      $query_result = $model->get_repeat_stat_all($value['days']);

      $series = array('name' => $value['name'], 'data' => array());
      foreach (array_reverse($query_result) as $result) {
        $ret[$key]['date'][] = $result['date'] . " / " . $result['repeat_date'];
        $series['data'][] = floatval($result['rpr']);
      }
      $ret[$key]['series'][] = $series;
    }
  }

  public function build_data_new($model, &$ret) {
    foreach ($this->charts as $key => $value) {
      $query_result = $model->get_repeat_stat_new($value['days']);

      $series = array('name' => $value['name'], 'data' => array());
      foreach (array_reverse($query_result) as $result) {
        $ret[$key]['date'][] = $result['date'] . " / " . $result['repeat_date'];
        $series['data'][] = floatval($result['rpr']);
      }
      $ret[$key]['series'][] = $series;
    }
  }
  
  public function build_data_theme($model, &$ret) {
    foreach ($this->charts as $key => $value) {
      $query_result = $model->get_repeat_stat_theme($value['days']);

      $series = array('name' => $value['name'], 'data' => array());
      foreach (array_reverse($query_result) as $result) {
        $ret[$key]['date'][] = $result['date'] . " / " . $result['repeat_date'];
        $series['data'][] = floatval($result['rpr']);
      }
      $ret[$key]['series'][] = $series;
    }
  }
  
  public function build_data_noted($model, &$ret) {
    foreach ($this->charts as $key => $value) {
      $query_result = $model->get_repeat_stat_noted($value['days']);

      $series = array('name' => $value['name'], 'data' => array());
      foreach (array_reverse($query_result) as $result) {
        $ret[$key]['date'][] = $result['date'] . " / " . $result['repeat_date'];
        $series['data'][] = floatval($result['rpr']);
      }
      $ret[$key]['series'][] = $series;
    }
  }
  
  public function build_data_single($model, &$ret) {
    foreach ($this->charts as $key => $value) {
      $query_result = $model->get_repeat_stat_single($value['days']);

      $series = array('name' => $value['name'], 'data' => array());
      foreach (array_reverse($query_result) as $result) {
        $ret[$key]['date'][] = $result['date'] . " / " . $result['repeat_date'];
        $series['data'][] = floatval($result['rpr']);
      }
      $ret[$key]['series'][] = $series;
    }
  }

  public function build_data_99repurchase($model, &$ret,$sart,$end) {
    foreach ($this->charts as $key => $value) {
      $query_result = $model->get_repeat_stat_99repurchase($value['days'],$sart,$end);

      $series = array('name' => $value['name'], 'data' => array());
      foreach (array_reverse($query_result) as $result) {
        $ret[$key]['date'][] = $result['date'] . " / " . $result['repeat_date']."  [".$result['rpt_count'] . "/" . $result['ori_count']."]";
        if($result['ori_count']!=0){
          $series['data'][] = floatval($result['rpt_count']/$result['ori_count']);
        }
        else{
          $series['data'][] = 0;
        }
      }
      $ret[$key]['series'][] = $series;
    }
  }
  
  public function build_data_snapuprepurchase($model, &$ret,$sart,$end) {
    foreach ($this->charts as $key => $value) {
      $query_result = $model->get_repeat_stat_snapuprepurchase($value['days'],$sart,$end);

      $series = array('name' => $value['name'], 'data' => array());
      foreach (array_reverse($query_result) as $result) {
        $ret[$key]['date'][] = $result['date'] . " / " . $result['repeat_date']."  [".$result['rpt_count'] . "/" . $result['ori_count']."]";
        if($result['ori_count']!=0){
          $series['data'][] = floatval($result['rpt_count']/$result['ori_count']);
        }
        else{
          $series['data'][] = 0;
        }
      }
      $ret[$key]['series'][] = $series;
    }
  }

}
