<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Temp_Category extends Controller {
  private $charts = array("gmv", "order_count", "user_count", "shop_count", "price", "product_count");

  public function action_dailygmv() {
    if (!isset($this->request->query()["cid"])){
      $ret['code']=0;
      exit(json_encode($ret));
    }
    $cid = $this->request->query()["cid"];
    $model = Model::factory('Category_Stat');

    $ret = $this->init();

    $this->build_data($model, $ret, $cid,0);

    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function action_dailygmv_without_special() {
    if (!isset($this->request->query()["cid"])){
      $ret['code']=0;
      exit(json_encode($ret));
    }
    $cid = $this->request->query()["cid"];
    $model = Model::factory('Category_Stat');

    $ret = $this->init();
    $this->build_data($model, $ret, $cid,1);

    $ret['code'] = 0;
    echo json_encode($ret);
  }

  public function init() {
    $ret = array();

    foreach ($this->charts as $chart) {
      $ret[$chart] = array('date' => array(), 'series' => array());
    }

    return $ret;
  }

  public function build_data($model, &$ret, $cid,$flag) {
    if ($flag == 0){
      $query_result = $model->get_category_stat($cid);
    }
    else if ($flag == 1){
      $query_result = $model->get_category_stat_without_special($cid);
    }


    $total_gmv = array('name' => '销售总额', 'data' => array());
    $total_pay_gmv = array('name' => '用户付款总额', 'data' => array());
    $total_order_count = array('name' => '总订单数', 'data' => array());
    $total_user_count = array('name' => '支付买家数', 'date' => array());
    $total_shop_count = array('name' => '支付商家数', 'date' => array());
    $total_product_count = array('name' => '支付商品数', 'date' => array());
    $price_per_order = array('name' => '客单价', 'date' => array());

    foreach (array_reverse($query_result) as $result) {
      foreach ($this->charts as $chart) {
        $ret[$chart]['date'][] = $result['date'];
      }

      $total_gmv['data'][] = floatval($result['total_price']);
      $total_pay_gmv['data'][] = floatval($result['total_discount_price']);
      $total_order_count['data'][] = intval($result['order_count']);
      $total_user_count['data'][] = intval($result['user_count']);
      $total_shop_count['data'][] = intval($result['shop_count']);
      $total_product_count['data'][] = intval($result['product_count']);
      $price_per_order['data'][] = intval($result['total_price']) / intval($result['order_count']);
    }

    $ret['gmv']['series'][] = $total_gmv;
    $ret['gmv']['series'][] = $total_pay_gmv;
    $ret['order_count']['series'][] = $total_order_count;
    $ret['user_count']['series'][] = $total_user_count;
    $ret['shop_count']['series'][] = $total_shop_count;
    $ret['product_count']['series'][] = $total_product_count;
    $ret['price']['series'][] = $price_per_order;

  }
}
