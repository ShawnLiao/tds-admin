<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard extends Controller_Layout {

  public $styles = array(
    'static/assets/plugins/bootstrap-datepicker/css/datepicker.css' => 'screen',
    'static/assets/plugins/bootstrap-datepicker/css/datepicker3.css' => 'screen',
    'static/assets/plugins/ionRangeSlider/css/ion.rangeSlider.css' => 'screen',
    'static/assets/plugins/ionRangeSlider/css/ion.rangeSlider.skinNice.css' => 'screen',
    'static/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css' => 'screen',
    'static/assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css' => 'screen',
    'static/assets/plugins/password-indicator/css/password-indicator.css' => 'screen',
    'static/assets/plugins/bootstrap-combobox/css/bootstrap-combobox.css' => 'screen',
    'static/assets/plugins/bootstrap-select/bootstrap-select.min.css' => 'screen',
    'static/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css' => 'screen',
    'static/assets/plugins/jquery-tag-it/css/jquery.tagit.css' => 'screen',
  );

  public $scripts = array(
    'static/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
    'static/assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
    'static/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
    'static/assets/plugins/masked-input/masked-input.min.js',
    'static/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
    'static/assets/plugins/password-indicator/js/password-indicator.js',
    'static/assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',
    'static/assets/plugins/bootstrap-select/bootstrap-select.min.js',
    'static/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
    'static/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',
    'static/assets/plugins/jquery-tag-it/js/tag-it.min.js',
    'static/assets/js/form-plugins.demo.min.js',
    'static/assets/js/dashboard-v2.min.js',
    'static/assets/js/apps.min.js',
    'static/highcharts/highcharts.js',
    'static/highcharts/modules/data.js',
    'static/highcharts/modules/exporting.js',

  );

  public function action_monthly()
  {
    $this->template->page_title = 'Adam 数据平台 - 月级GMV';

    $scripts = array(
      'static/js/dashboard_monthly.js'
    );

    if (null == $this->template->page_content) {
      $this->template->page_content = View::factory('dashboard/monthly');
    }

    $this->template->page_styles = array_merge($this->template->styles, $this->styles);
    $this->template->page_scripts = array_merge($this->scripts, $scripts);

    $this->set_side_bar_nav_index(1, 4);
  }

  public function action_daily_without_virtual()
  {
    $this->template->page_title = 'Adam 数据平台 - 天级GMV（不包括虚拟商品）';

    $scripts = array(
      'static/js/dashboard_daily_without_virtual.js'
    );

    if (null == $this->template->page_content) {
      $this->template->page_content = View::factory('dashboard/daily_without_virtual');
    }

    $this->template->page_styles = array_merge($this->template->styles, $this->styles);
    $this->template->page_scripts = array_merge($this->scripts, $scripts);

    $this->set_side_bar_nav_index(1, 3);
  }

  public function action_daily()
  {
    $this->template->page_title = 'Adam 数据平台 - 天级GMV';

    $scripts = array(
      'static/js/dashboard.js'
    );

    if (null == $this->template->page_content) {
      $this->template->page_content = View::factory('dashboard/daily');
    }
    $this->template->page_styles = array_merge($this->template->styles, $this->styles);
    $this->template->page_scripts = array_merge($this->scripts, $scripts);

    $this->set_side_bar_nav_index(1, 2);
  }

  public function action_index()
  {
    $this->template->page_title = 'Rabirroo数据平台 - 首页';

    $scripts = array(
    );

    $this->template->page_content = View::factory('dashboard/index');
    $this->template->page_styles = array_merge($this->template->styles, $this->styles);
    $this->template->page_scripts = array_merge($this->scripts, $scripts);
  }

}
