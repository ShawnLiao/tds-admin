<?php defined('SYSPATH') OR die('No direct access allowed.');

class Auth_Mysql extends Auth {
  public function is_admin() {
    $email = $this->get_user();

    $user = ORM::factory("user")
      ->where('email', '=', $email)
      ->find();

    if ($user->is_admin == 1) {
      return TRUE;
    }

    return FALSE;
  }

  public function is_industryAdmin() {
    $email = $this->get_user();

    $user = ORM::factory("user")
      ->where('email', '=', $email)
      ->find();

    if ($user->is_industryAdmin == 1) {
      return TRUE;
    }

    return FALSE;
  }

  public function get_user_id() {
    $email = $this->get_user();

    $user = ORM::factory("user")
      ->where('email', '=', $email)
      ->find();

    if ($user->loaded()) {
      return $user->id;
    }

    return FALSE;
  }

  public function get_name() {
    $email = $this->get_user();

    $user = ORM::factory("user")
      ->where('email', '=', $email)
      ->find();

    if ($user->loaded()) {
      return $user->name;
    }

    return FALSE;
  }

  public function change_password($new_password) {
    $email = $this->get_user();

    // TODO: Check if is logged in
    $user = ORM::factory("user")
      ->where('email', '=', $email)
      ->find();

    $user->password = $this->hash($new_password);

    return $user->save();
  }

  public function register($email, $name, $password, $is_admin) {
    if ($this->exist_user($email)) {
      return FALSE;
    }

    $user = ORM::factory("user");
    $user->email = $email;
    $user->name = $name;
    $user->password = $this->hash($password);
    $user->is_admin = $is_admin;

    return $user->save();
  }

  protected function exist_user($email) {
    $user = ORM::factory("user")
      ->where('email', '=', $email)
      ->find();

    if ($user->loaded()) {
      return TRUE;
    }

    return FALSE;
  }

  protected function _login($username, $password, $remember) {
    if (is_string($password)) {
      $password = $this->hash($password);
    }

    $user = ORM::factory("user")
      ->where('email', '=', $username)
      ->where('password', '=', $password)
      ->find();

    if ($user->loaded()) {
      return $this->complete_login($username);
    }

    return FALSE;
  }

  public function password($username) {
    $user = ORM::factory("user")
      ->where('email', '=', $username)
      ->find();

    if ($user->loaded()) {
      return $user->password;
    }

    return FALSE;
  }

  public function check_password($password) {
    $username = $this->get_user();
    return ($this->hash($password) == $this->password($username));
  }

}
