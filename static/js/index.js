// 显示错误
var showError = function(msg,error_panel,error_template) {
  $(error_panel).children().remove();
  var error = $(error_template).clone().css("display", "block");
  error.children(":eq(1)").text(msg);
  error.appendTo($(error_panel));
  $(error_template).fadeOut(8000);
}

// 清除列表
var clearItemList = function(item_list) {
  $(item_list).children(".item").remove();
}

// 列表显示
// tableId为原生写法
var handleSearchResults = function(data, item_template, item_list, sortOrNot, tableId, total_template, total_list) {
  var total_flag = 0;

  for (var idx=0;idx<data.length;idx++) {
    i = 0;
		var html = $(item_template).clone().css("display", "").addClass("item");
		for (var key in data[idx]){
			html.children("td").eq(i++).text(data[idx][key]);
		}
		html.appendTo($(item_list));
  }

  if(sortOrNot==1){

    var oTab = document.getElementById(tableId);
    var tBody = oTab.tBodies[total_flag];
    var tHead = oTab.tHead;
    var oThs = tHead.rows[0].cells;
    var oRows = tBody.rows;
    for (var i = 0; i < oThs.length; i++) {
      var oTh = oThs[i];
      if (oTh.className === "sorting") {
        oTh.index = i;
        oTh.onclick = function () {
          sortList.call(this, this.index,oRows,tBody);
        }
      }
    }
  }
}

// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
Date.prototype.Format = function (fmt) { //author: meizz
  var o = {
    "M+": this.getMonth() + 1, //月份
    "d+": this.getDate(), //日
    "h+": this.getHours(), //小时
    "m+": this.getMinutes(), //分
    "s+": this.getSeconds(), //秒
    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
    "S": this.getMilliseconds() //毫秒
  };
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1,(this.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o){
    if (new RegExp("(" + k + ")").test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ?
                        (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    }
  }
  return fmt;
}

var monthStart = function(month){
  mon = $("#month").val();
  var myDate = new Date();
  DateString = myDate.Format("yyyyMMdd");
  var yy = DateString.substr(0,4);
  mon.toString();
  var dd = "00";
  return yy+mon+dd;

}

var monthEnd = function(month){
  month = $("#month").val();
  var myDate2 = new Date();
  myDate2.setMonth(month)
  DateString = myDate2.Format("yyyyMMdd");
  var yy = DateString.substring(0,6);
  var dd = "00";
  return yy+dd;
}

var timestamp2Time = function(timestamp) {
  if (null == timestamp) return timestamp
    return new Date(parseInt(timestamp) * 1000).Format("yyyyMMdd");
}

var date2Timestamp = function(date_str) {
  var date = new Date();
  date.setFullYear(date_str.substring(6,10));
  date.setDate(1);
  date.setMonth(date_str.substring(0,2)-1);
  date.setDate(date_str.substring(3,5));
  date.setHours(0);
  date.setMinutes(0);
  date.setSeconds(0);
  return Date.parse(date)/1000;
}

var toDate = function(date_str) {
  return date_str.substring(6,10) + date_str.substring(0,2) + date_str.substring(3,5);
}

var showPageInfo = function(rows, count_per_page, page, res_page_options) {
  $(res_page_options.data_table_info).css("display", "");
  // Show page info
  $(res_page_options.data_table_info).children("span").eq(0).text(rows);
  var beg = (page - 1) * count_per_page + 1;
  var end = page * count_per_page;
  end = end > rows ? rows : end;
  $(res_page_options.data_table_info).children("span").eq(1).text(beg);
  $(res_page_options.data_table_info).children("span").eq(2).text(end);
}

var createPageBtnAll = function(rows, count_per_page, page, page_number, page_container,res_page_options) {
  for (var i = 0; i < page_number; i++) {
    var page_btn = $(res_page_options.page_btn_template).clone().css("display", "").addClass("item");
    page_btn.text(i+1);
    if (i+1 == page) {
      page_btn.addClass("current");
    }
    page_btn.appendTo(page_container);
  }
}

var createPageBtnPart = function(rows, count_per_page, page, page_number, page_container,res_page_options) {
  if (page <= 3) {
    for (var i = 0; i < page + 1; i++) {
      var page_btn = $(res_page_options.page_btn_template).clone().css("display", "").addClass("item");
      page_btn.text(i+1);
      if (i+1 == page) {
        page_btn.addClass("current");
      }
      page_btn.appendTo(page_container);
    }

    var page_btn = $(res_page_options.page_btn_template).clone().css("display", "").addClass("item").addClass("disabled");
    page_btn.text("...");
    page_btn.appendTo(page_container);

    page_btn = $(res_page_options.page_btn_template).clone().css("display", "").addClass("item");
    page_btn.text(page_number);
    page_btn.appendTo(page_container);
    return;
  }

  if (page >= page_number - 2) {
    var page_btn = $(res_page_options.page_btn_template).clone().css("display", "").addClass("item");
    page_btn.text(1);
    page_btn.appendTo(page_container);

    page_btn = $(res_page_options.page_btn_template).clone().css("display", "").addClass("item").addClass("disabled");
    page_btn.text("...");
    page_btn.appendTo(page_container);

    for (var i = page - 2; i < page_number; i++) {
      var page_btn = $(res_page_options.page_btn_template).clone().css("display", "").addClass("item");
      page_btn.text(i+1);
      if (i+1 == page) {
        page_btn.addClass("current");
      }
      page_btn.appendTo(page_container);
    }
    return;
  }

  var page_btn = $(res_page_options.page_btn_template).clone().css("display", "").addClass("item");
  page_btn.text(1);
  page_btn.appendTo(page_container);

  page_btn = $(res_page_options.page_btn_template).clone().css("display", "").addClass("item").addClass("disabled");
  page_btn.text("...");
  page_btn.appendTo(page_container);

  for (var i = page - 2; i < page + 1; i++) {
    var page_btn = $(res_page_options.page_btn_template).clone().css("display", "").addClass("item");
    page_btn.text(i+1);
    if (i+1 == page) {
      page_btn.addClass("current");
    }
    page_btn.appendTo(page_container);
  }

  page_btn = $(res_page_options.page_btn_template).clone().css("display", "").addClass("item").addClass("disabled");
  page_btn.text("...");
  page_btn.appendTo(page_container);

  page_btn = $(res_page_options.page_btn_template).clone().css("display", "").addClass("item");
  page_btn.text(page_number);
  page_btn.appendTo(page_container);
}

var createPageBtn = function(rows, count_per_page, page,res_page_options) {
  $(res_page_options.data_table_paginate).css("display", "");
  var page_container = $(res_page_options.page_btn_template).parent();
  page_container.children(".item").remove();

  var page_number = rows / count_per_page;
  if (page_number > Math.floor(page_number)) {
    page_number = Math.ceil(page_number)
  } else {
    page_number = Math.floor(page_number)
  }

  if (page_number <= 5) {
    createPageBtnAll(rows, count_per_page, page, page_number, page_container,res_page_options);
  } else {
    createPageBtnPart(rows, count_per_page, page, page_number, page_container,res_page_options);
  }

  showOrDisableNextPrevBtn(rows, count_per_page, page, page_number,res_page_options);
}

var showOrDisableNextPrevBtn = function(rows, count_per_page, page, page_number,res_page_options) {
  if (page == 1) {
    $(res_page_options.data_table_previous).addClass("disabled");
  } else {
    $(res_page_options.data_table_previous).removeClass("disabled");
  }

  if (page == page_number) {
    $(res_page_options.data_table_next).addClass("disabled");
  } else {
    $(res_page_options.data_table_next).removeClass("disabled");
  }
}

var handleResultPageShow = function(rows, count_per_page, page, res_page_options) {
  $(res_page_options.result_row).val(rows);
  $(res_page_options.result_count_per_page).val(count_per_page);

  showPageInfo(rows, count_per_page, page,res_page_options);

  createPageBtn(rows, count_per_page, page,res_page_options);
}

var getbrandtype = function(options) {
  // Ajax请求
  $.ajax({
    type: "get",
    url: "/service/brandtype",
    data: null,
    dataType: "json",
    contentType: 'application/json;charset=utf-8',
    success: function(res) {
      if (0 == res.code) {
        handlebrandtype(res.data,options);
      } else {
        alert(res.msg);
      }
    },
    error: function(data) {
      alert(data.responseText);
    }
  });
}

var handlebrandtype = function(data,options) {
  $(options).children(":gt(0)").remove();
  for (var idx in data) {
    $("<option/>").text(data[idx].type_name).appendTo($(options));
  }
}

var getServer = function(server) {
  // Ajax请求
  $.ajax({
    type: "get",
    url: "/service/getserver",
    data: null,
    dataType: "json",
    contentType: 'application/json;charset=utf-8',
    success: function(res) {
      if (0 == res.code) {
        handleServer(res.data, server);
      } else {
        alert(res.msg);
      }
    },
    error: function(data) {
      alert(data.responseText);
    }
  });
}

var handleServer = function(data, server) {
  $(server).children(":gt(0)").remove();
  for (var idx in data) {
    $("<option/>").val(data[idx].id).text(data[idx].name).appendTo($(server));
  }
}

var sc1Select = function(d,e) {
  cname1 = d.data.cname1;
  cname2 = d.data.cname2;
  var value = $(cname1).find("option:selected").val();
  if ("0" == value) {
    $(cname2).children(":gt(0)").remove();
    return;
  }

  var name = $(cname1).find("option:selected").text();
  $.ajax({
    type: "get",
    url: "/service/c1c2",
    data: {
      c1_name: name
    },
    dataType: "json",
    contentType: 'application/json;charset=utf-8',
    success: function(res) {
      if (0 == res.code) {
        handleshopC1C2(res.data,cname2);
      } else {
        alert(res.msg);
      }
    },
    error: function(data) {
      alert(data.responseText);
    }
  });
}

var handleshopC1C2 = function(data,cname2) {
  $(cname2).children(":gt(0)").remove();
  for (var idx in data) {
    $("<option/>").val(data[idx].c2).text(data[idx].c2_name).appendTo($(cname2));
  }
}

var pc1Select = function(d,e) {
  cname1 = d.data.cname1;
  cname2 = d.data.cname2;
  cname3 = d.data.cname3;
  var value = $(cname1).find("option:selected").val();
  if ("0" == value) {
    $(cname2).children(":gt(0)").remove();
    $(cname3).children(":gt(0)").remove();
    return;
  }

  var name = $(cname1).find("option:selected").text();

  $.ajax({
    type: "get",
    url: "/service/c1c2",
    data: {
      c1_name: name
    },
    dataType: "json",
    contentType: 'application/json;charset=utf-8',
    success: function(res) {
      if (0 == res.code) {
        handlepC1C2(res.data,cname2);
      } else {
        alert(res.msg);
      }
    },
    error: function(data) {
      alert(data.responseText);
    }
  });
}

var handlepC1C2 = function(data,cname2) {
  $(cname2).children(":gt(0)").remove();
  for (var idx in data) {
    $("<option/>").val(data[idx].c2).text(data[idx].c2_name).appendTo($(cname2));
  }
}

var pc2Select = function(d,e) {
  cname1 = d.data.cname1;
  cname2 = d.data.cname2;
  cname3 = d.data.cname3;
  var value = $(cname2).find("option:selected").val();
  if ("0" == value) {
    $(cname3).children(":gt(0)").remove();
    return;
  }

  var name = $(cname2).find("option:selected").text();

  $.ajax({
    type: "get",
    url: "/service/c2c3",
    data: {
      c2_name: name
    },
    dataType: "json",
    contentType: 'application/json;charset=utf-8',
    success: function(res) {
      if (0 == res.code) {
        handlepC2C3(res.data,cname3);
      } else {
        alert(res.msg);
      }
    },
    error: function(data) {
      alert(data.responseText);
    }
  });
}

var handlepC2C3 = function(data,cname3) {
  $(cname3).children(":gt(0)").remove();
  for (var idx in data) {
    $("<option/>").val(data[idx].c3).text(data[idx].c3_name).appendTo($(cname3));
  }
}

//table列表单列汇总求和
/*colTotal：实现列表单列汇总
 * column:列的索引，从0开始
 * tbodyId:需要求和的容器id
 * trId：汇总表头id
 * */
var colTotal = function (column, tbodyId, trId) {
  var trs = document.getElementById(tbodyId).getElementsByTagName('tr');
  var start = 0, end = trs.length;
  var total = 0;
  for (var i = start; i < end; i++) {
    var td = trs[i].getElementsByTagName('td')[column];
    var t = parseFloat(td.innerHTML);
    if (t) {
      total += t;
    }
  }
  document.getElementById(trId).getElementsByTagName('th')[column].innerHTML = total.toFixed(2);
};

//设置初始查询时间
function dateFirstSetting(dateInterval,beginId,endId){
  var today = new Date();
  today.setHours(0);
  today.setMinutes(0);
  today.setSeconds(0);
  today.setMilliseconds(0);
  var oneday = 1000 * 60 * 60 * 24;
  var yesterday = new Date(today - oneday);
  yesterday = Date.parse(yesterday) / 1000;
  if(endId==""){
    $(beginId).val(new Date(parseInt(yesterday - 86400 * dateInterval) * 1000).Format("MM/dd/yyyy"));
  }else {
    $(beginId).val(new Date(parseInt(yesterday - 86400 * dateInterval) * 1000).Format("MM/dd/yyyy"));
    $(endId).val(new Date(parseInt(yesterday) * 1000).Format("MM/dd/yyyy"));
  }
}

var handleSourceCategories = function (data, cname) {
  for (var idx in data) {
    $("<option/>").val(data[idx].c1).text(data[idx].track_name).appendTo($(cname));
  }
}
var getSourceCategories = function (cname) {
  $.ajax({
    type: "get",
    url: "/stat/source/selectc1",
    data: null,
    dataType: "json",
    contentType: 'application/json;charset=utf-8',
    success: function (res) {
      if (0 == res.code) {
        handleSourceCategories(res.data,cname);
      } else {
        alert(res.msg);
      }
    },
    error: function (data) {
      alert(data.responseText);
    }
  });
}

var handleSourceCategories = function (data, cname) {
  for (var idx in data) {
    $("<option/>").val(data[idx].c1).text(data[idx].track_name).appendTo($(cname));
  }
}
var getSourceCategories = function (cname) {
  $.ajax({
    type: "get",
    url: "/sources/sources/selectc1",
    data: null,
    dataType: "json",
    contentType: 'application/json;charset=utf-8',
    success: function (res) {
      if (0 == res.code) {
        handleSourceCategories(res.data,cname);
      } else {
        alert(res.msg);
      }
    },
    error: function (data) {
      alert(data.responseText);
    }
  });
}
/*
* @colSort实现表格排序
* @tableId:table标签的id
* */
function colSort(tableId){
  var oTab = document.getElementById(tableId);
  var tBody = oTab.tBodies[0];
  var tHead = oTab.tHead;
  var oThs = tHead.rows[0].cells;
  var oRows = tBody.rows;

  for (var i = 0; i < oThs.length; i++) {
    var oTh = oThs[i];
    if (oTh.className === "sorting") {
      oTh.index = i;
      oTh.onclick = function () {
        sortList.call(this, this.index,oRows,tBody);
      }
    }
  }
}


function listToArray(likeAry) {
  var ary = [];
  try {
    ary = Array.prototype.slice.call(likeAry, 0);
  } catch (e) {
    for (var i = 0; i < likeAry.length; i++) {
      ary[ary.length] = likeAry[i];
    }
  }
  return ary;
}

function isset(Id){
  return document.getElementById(Id);
}

function sortList(index,oRows,tBody) {
  //a、将类数组转换为数组
  var ary = listToArray(oRows);
  //b、给数组进行排序(默认都是从小到大)
  ary.sort(function (a, b) {
    //获取到当前行和下一行第三列的内容,按照内容的大小进行排序
    var curIn = a.cells[index].innerHTML;
    var nexIn = b.cells[index].innerHTML;
    //对于数字我们直接的相减,对于非数字,用localeCompare
    var curInNum = parseFloat(curIn);
    var nexInNum = parseFloat(nexIn);
    if (isNaN(curInNum)) {
      return curIn.localeCompare(nexIn);
    }
    return curInNum - nexInNum;
  });
  //e、实现升降序切换
  //添加一个标识flag,记录当前列的排列顺序
  //第一次点击 前-乱序 ->升序(asc)
  //第二次点击 前-升序 ->降序(desc)
  //第三次点击 前-降序 ->升序
  if (this.flag === "asc") {
    ary.reverse();
    this.flag = "desc";
    $(this).removeClass().addClass("sorting_desc");
  } else {
    this.flag = "asc";

    $(this).removeClass().addClass("sorting_asc");
  }
  //c、按照最新排列的顺序把我们的tr重新的添加到页面中
  var frg = document.createDocumentFragment();
  for (var i = 0; i < ary.length; i++) {
    frg.appendChild(ary[i]);
  }
  tBody.appendChild(frg);
  frg = null;

}
//绑定table数据
function bindTable(titleData,data,theadId,tbodyId,page_item_count){
  var str1="";
  str1+="<tr>";
  console.log(titleData);
  for(var i=0;i<titleData.length;i++){
    var cur=titleData[i];
    str1+="<th val="+i+">"+cur+"</th>";
  }
  str1+="</tr>";
  $(theadId).append(str1);

  var str2="";
  var arry=[];
  for(var a in data[0]){
    arry.push(a);
  }
  //console.log(arry);
  if (page_item_count == null) page_item_count = titleData.length;
  if (titleData.length < page_item_count) page_item_count = titleData.length;
  for(var i=0;i<page_item_count;i++){
    var cur=data[i];
    str2+="<tr class='item'>";
    for(var j in cur){
      str2+="<td val="+j+">"+cur[j]+"</td>";
    }

    str2+="</tr>"
  }
  $(tbodyId).append(str2);

}


//清空列表
function clearList(theadId,tbody){
  $(theadId).children().remove();
  $(tbody).children().remove();
}

$(function(){
});


$(document).ready(function(){
    //手机预览
    var width = 340; //弹出窗口的宽度;
    var height = 640; //弹出窗口的高度;
    var top = (window.screen.availHeight - height) / 2; //窗口的垂直位置;
    var left = (window.screen.availWidth - width) / 2; //窗口的水平位置;
    $('.open_product_show').live('click', function () {
        window.open($(this).attr('url'), '', 'height=' + height + ',width=' + width + ',top=' + top + ',left=' + left + ', toolbar=no,menubar=no,scrollbars=yes, resizable=no,location=no, status=no')
    });
});
