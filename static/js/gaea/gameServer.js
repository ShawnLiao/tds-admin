// 判断是否有条件，日期条件是必须的
var hasOptions = function() {
  if ( "" == $("#date-start").val() || "" == $("#date-end").val()) {
    return false;
  }
  if("0" == $("#cname1").find("option:selected").val()&&"" == $("#shop-name").val()&&"" == $("#shop-id-list").val()){
    return false;
  }
  return true;
}

// 检查输入合法性
var checkInputs = function() {
	return true;
  if (!hasOptions()) {
    showError("查询条件必须包含日期,且至少包含一项查询条件(最底层类目、id、名称)","#error-panel","#error-template");
    return false;
  }
  return true;
};

// 构建shop条件
var buildOptions = function(data) {
	data.status = $("#status").find("option:selected").val();
	data.host_ip_list = $("#host-ip").val();
}

var buildPageSetting = function(data) {
	data.page_item_count = $("#page-item-count option:selected").val();
}


// 查询按钮点击事件
var searchClick = function() {
  $(".btn-search").attr("disabled","true");
  // 构建查询条件
	var query_options = {};
  buildOptions(query_options);
	buildPageSetting(query_options);
  clearItemList("#item-list"); 
  // Ajax请求
  $.ajax({
    type: "get",
    url: "/gaea/gameserver/search",
    data:query_options,
    async:false,
    dataType: "json",
    contentType: 'application/json;charset=utf-8',
    success: function(res) {
      if (0 == res.code) {
        $("#result-table").val(res.table);
        handleSearchResults(res.data,"#item-template","#item-list");
				handleResultPage(parseInt(res.rows), parseInt(res.page_item_count), parseInt(res.page));
      } else {
        alert(res.msg);
      }
      $(".btn-search").removeAttr("disabled");
    },
    error: function(data) {
      //alert(data.responseText);
      showError("抱歉，数据异常，请选择其他查询时间条件", "#error-panel", "#error-template");
      $(".btn-search").removeAttr("disabled");
    }
  });

}

var handleResultPage = function(rows, count_per_page, page) {
  res_page_options = {
    result_row : "#result-rows",
    result_count_per_page : "#result-count-per-page",
    page_btn_template : "#page-btn-template",
    data_table_previous : "#data-table_previous",
    data_table_next : "#data-table_next",
    data_table_paginate : "#data-table_paginate",
    data_table_info : "#data-table_info1",
  }
  handleResultPageShow(rows,count_per_page,page,res_page_options);
  $(res_page_options.page_btn_template).parent().children(".item").click(pageBtnClick);
}

var pageBtnClick = function(e) {
  page_option = {
  table: $("#result-table").val(),
  rows: $("#result-rows").val(),
  count_per_page: $("#result-count-per-page").val(),
  page: $(e.target).text()
  }

  buildPageSetting(page_option);
  buildOptions(page_option);
  clearItemList("#item-list");
  $.ajax({
    type: "get",
    url: "/gaea/gameserver/page",
    data: page_option,
    dataType: "json",
    contentType: 'application/json;charset=utf-8',
    success: function(res) {
      if (0 == res.code) {
        $("#result-table").val(res.table);
       // dataContainer=res.data;
        handleSearchResults(res.data,"#item-template","#item-list");
        handleResultPage(parseInt(res.rows), parseInt(res.page_item_count), parseInt(res.page));
      } else {
        alert(res.msg);
      }
    },
    error: function(data) {
      alert(data.responseText);
    }
  });
}

var pagePrevNextBtnClick = function(e) {
  if ($(e.target).hasClass("disabled")) {
    return;
  }
  var cur_page = parseInt($("#data-table_paginate").children("span").children(".current").text());
  var page = 0;
  if ($(e.target).text() == "Previous") {
    page = cur_page - 1
  } else if ($(e.target).text() == "Next") {
    page = cur_page + 1
  }
  page_option = {
  table: $("#result-table").val(),
  rows: $("#result-rows").val(),
  count_per_page: $("#result-count-per-page").val(),
  page: page
  }
  buildPageSetting(page_option);
  buildOptions(page_option);
  clearItemList("#item-list");
  $.ajax({
    type: "get",
    url: "/gaea/gameserver/page",
    data: page_option,
    dataType: "json",
    contentType: 'application/json;charset=utf-8',
    success: function(res) {
      if (0 == res.code) {
        $("#result-table").val(res.table);
        handleSearchResults(res.data,"#item-template","#item-list");
        handleResultPage(parseInt(res.rows), parseInt(res.page_item_count), parseInt(res.page));
      } else {
        alert(res.msg);
      }
    },
    error: function(data) {
      alert(data.responseText);
    }
  });
}

var bindEvent = function () {
  $(".btn-search").click(searchClick);
	$("#data-table_previous").click(pagePrevNextBtnClick);
	$("#data-table_next").click(pagePrevNextBtnClick);
}

$(function(){
  bindEvent();
});
