// 判断是否有条件，日期条件是必须的
var hasOptions = function() {
  if ( "" == $("#date-start").val() || "" == $("#date-end").val()) {
    return false;
  }
  if("0" == $("#cname1").find("option:selected").val()&&"" == $("#shop-name").val()&&"" == $("#shop-id-list").val()){
    return false;
  }
  return true;
}

// 检查输入合法性
var checkInputs = function() {
  return true;
  if (!hasOptions()) {
    showError("查询条件必须包含日期,且至少包含一项查询条件(最底层类目、id、名称)","#error-panel","#error-template");
    return false;
  }
  return true;
};

// 构建shop条件
var buildOptions = function(data) {
  data.role_id = $("#role-id").val();
}

var buildPageSetting = function(data) {
  data.page_item_count = $("#page-item-count option:selected").val();
}

// 查询按钮点击事件
var searchClick = function() {
  // 构建查询条件
  var query_options = {};
  buildOptions(query_options);
	if (query_options.role_id == '0') return;
  $(".btn-search").attr("disabled","true");
  // Ajax请求
  $.ajax({
    type: "get",
    url: "/gaea/gamedata/search",
    data:query_options,
    async:false,
    dataType: "json",
    contentType: 'application/json;charset=utf-8',
    success: function(res) {
      if (0 == res.code) {
				//$('#roleData').html(syntaxHighlight(res.roleData));
				$('#roleData').html(JSON.stringify(res.roleData, undefined, 4));
				//$('#townData').html(syntaxHighlight(res.townData));
				$('#townData').html(JSON.stringify(res.roleData, undefined, 4));
				handleTownList(res.townList, '#town-id');
      } else {
        alert(res.msg);
      }
      $(".btn-search").removeAttr("disabled");
    },
    error: function(data) {
      //alert(data.responseText);
      showError("抱歉，数据异常，请选择其他查询时间条件", "#error-panel", "#error-template");
      $(".btn-search").removeAttr("disabled");
    }
  });
}

var test = function(){
	var query_options = {};
	query_options.town_id = $("#town-id").find("option:selected").val();
  $.ajax({
    type: "get",
    url: "/gaea/gamedata/searchTown",
    data:query_options,
    async:false,
    dataType: "json",
    contentType: 'application/json;charset=utf-8',
    success: function(res) {
      if (0 == res.code) {
				//$('#townData').html(syntaxHighlight(res.townData));
				$('#townData').html(JSON.stringify(res.roleData, undefined, 4));
      } else {
        alert(res.msg);
      }
    },
    error: function(data) {
      //alert(data.responseText);
      showError("抱歉，数据异常，请选择其他查询时间条件", "#error-panel", "#error-template");
    }
  });
}

var handleTownList = function(data, key){
	$(key).children().remove();
	console.log(data);
	for (var idx in data) {
		$("<option/>").val(data[idx]).text(data[idx]).appendTo($(key));
	}
}

var bindEvent = function () {
  $(".btn-search").click(searchClick);
	$("#town-id").change(test);
}

var syntaxHighlight = function (json) {
  if (typeof json != 'string') {
    json = JSON.stringify(json, undefined, 4);
  }
  json = json.replace(/&/g, '&').replace(/</g, '<').replace(/>/g, '>');
  return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function(match) {
  var cls = 'number';
    if (/^"/.test(match)) {
      if (/:$/.test(match)) {
      cls = 'key';
      } else {
        cls = 'string';
      }
    } else if (/true|false/.test(match)) {
      cls = 'boolean';
    } else if (/null/.test(match)) {
      cls = 'null';
    }
    return '<span class="' + cls + '">' + match + '</span>';
  });
}

$(function(){
  bindEvent();
	searchClick();
});
