var bindEvent = function() {
  $("#btn-login").click(loginClick);
  $("#username").keypress(keypress);
  $("#password").keypress(keypress);
}

var keypress = function(event) {
  if (event.keyCode == "13") {
    loginClick();    
  }
}


var loginClick = function() {
  if($('#rememberMe').is(':checked')){
    document.cookie = "username="+$('#username').val().trim()+";expires=19-Aug-2017 07:52:19 GMT +(+8)";
    document.cookie = "password="+$('#password').val().trim()+";expires=19-Aug-2017 07:52:19 GMT +(+8)";
  }
  else{
    cleanCookie("username");
    cleanCookie("password");
  }

  $.post(
    "/api/auth/login",
    {
      email : $("#username").val(),
      password : $("#password").val()
    },
    function(data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          window.location.href = "/";
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("登录失败!");
      }
    }
  )
}

//设置cookie
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + escape(cvalue) + "; " + expires;
}
//获取cookie
function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1);
      if (c.indexOf(name) != -1){
        var cnameValue = unescape(c.substring(name.length, c.length));
        return cnameValue;
      } 
  }
  return "";
} 
 
function cleanCookie (c_name) {     //使cookie过期
     document.cookie = c_name + "=" + ";expires=Thu, 01-Jan-70 00:00:01 GMT";
}

$(function(){
  //获取cookie
  var cusername = getCookie('username');
  var cpassword = getCookie('password');
  if(cusername != "" && cpassword != ""){
    $("#username").val(cusername);
    $("#password").val(cpassword);
  }
  
  bindEvent();
})
