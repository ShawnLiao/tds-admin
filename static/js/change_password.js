var bindChangePasswordEvent = function() {
  $("#btn-submit").click(submitClick);
}

var submitClick = function() {

  var old_password = $("#old-password").val();
  var new_password = $("#new-password").val();
  var new_password_confirm = $("#new-password-confirm").val();

  if ("" == old_password || "" == new_password || "" == new_password_confirm) {
    alert("密码不能为空");
    return;
  }

  if (new_password != new_password_confirm) {
    alert("新密码和确认密码不一致");
    return;
  }

  $.post(
    "/api/auth/changepassword",
    {
      password: old_password,
      new_password: new_password
    },
    function(data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          window.location.href = "/login";
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("修改密码失败!");
      }
    }
  )
}

$(function(){
  bindChangePasswordEvent();
})


