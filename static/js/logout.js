var bindLogoutEvent = function() {
  $("#btn-logout").click(logoutClick);
}

var logoutClick = function() {
  $.post(
    "/api/auth/logout",
    {},
    function(data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          window.location.href = "/login";
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("退出失败!");
      }
    }
  )
}

$(function(){
  bindLogoutEvent();
})

