var handleSearchResults = function(data) {
  $("#item-list").children(".item").remove();
  for (var idx in data.user_list) {
    var html = $("#item-template").clone().css("display", "").addClass("item");
    html.children("td").eq(0).text(data.user_list[idx].id);
    html.children("td").eq(1).text(data.user_list[idx].email);
    html.children("td").eq(2).text(data.user_list[idx].name);
    html.children("td").eq(3).text(data.user_list[idx].is_admin);
    html.children("td").eq(4).text(data.user_list[idx].is_delete);
    html.appendTo($("#item-list"));
  }

}

var loadData = function() {
  $.ajax({
    type: "get",
    url: "/api/admin/userlist",
    dataType: "json",
    contentType: 'application/json;charset=utf-8',
    success: function(res) {
      if (0 == res.code) {
        handleSearchResults(res);
      } else {
        alert(res.msg);
      }
    },
    error: function(data) {
      alert(data.responseText);
    }
  });
}

var bindEvent = function() {
  $(".btn-add").click(addUser);
  $(".btn-cancel").click(DelUser);
	$(".btn-reset").click(ReSetPassword);
}

var addUser = function() {
  $.post(
    "/api/auth/register",
    {
      email: $("#email").val(),
      name: $("#name").val(),
      is_admin: $("#is-admin").find("option:selected").val()
    },
    function(data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          loadData();
					alert(json_data.flag);
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("添加用户失败!");
      }
    }
  );
}

var DelUser = function() {
  $.post(
    "/api/admin/deluser",
    {
      email_del: $("#email_del").val(),
      name_del: $("#name_del").val()
    },
    function(data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          loadData();
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("删除用户失败!");
      }
    }
  );
}

var ReSetPassword = function() {
  $.post(
    "/api/admin/resetpassword",
    {
      email_reset: $("#email_reset").val(),
      name_reset: $("#name_reset").val()
    },
    function(data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          loadData();
          alert(json_data.flag);
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("删除用户失败!");
      }
    }
  );
}

$(function(){
  bindEvent();
  loadData();
});
