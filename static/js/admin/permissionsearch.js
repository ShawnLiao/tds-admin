var bindEvent = function() {
  $(".btn-user-search").click(userSearch);
}
 // 判断是否有输入姓名条件
 var hasOptions = function() {
   if ("" == $("#name_search").val()) {
     return false;
   }
 
   return true;
 }
 
 // 检查输入合法性
 var checkInputs = function() {
   if (!hasOptions()) {
     showError("请输入要查询的姓名","#error-panel","#error-template");
     return false;
   }
   return true;
 };


// 构建条件
var buildOptions = function(data) {
   data.options = {};
   if ("" != $("#name_search").val()){
     data.options.name=$("#name_search").val();
   }
}
var userSearch = function() {
  // 检查输入
  if (!checkInputs()) {
    return;
  }

  var name = $("#name_search").val();
  var query_options = {};
  buildOptions(query_options);
	clearItemList("#item-list1");

  // Ajax请求
  $.ajax({
    type: "post",
    url: "/api/admin/userSearch",
    data: query_options,
    dataType: "json",
    //contentType: 'application/json;charset=utf-8',
    success: function (res) {
      if (0 == res.code) {
			  //alert(res.data[0].name);
        handleUserSearchResults(res.data,"#item-template1","#item-list1");
      } else {
        alert(res.msg);
      }
      $(".btn-user-search").removeAttr("disabled");
    },
    error: function (data) {
      alert(data.responseText);
      $(".btn-user-search").removeAttr("disabled");
    }
  });

}

var handleUserSearchResults = function(data,item_template,item_list) {
  for (var idx in data) {
	  //alert("哈哈哈");
    var html = $(item_template).clone().css("display", "").addClass("item");
    html.children("td").eq(0).text(data[idx].user_id);
    html.children("td").eq(1).text(data[idx].email);
    html.children("td").eq(2).text(data[idx].name);
    html.children("td").eq(3).text(data[idx].group_name);
    html.children("td").eq(4).text(data[idx].page_name);
    html.children("td").eq(5).text(data[idx].permission_name);
    html.children("td").eq(6).text(data[idx].is_finance);
    html.appendTo($(item_list));
  }
 
}

$(function(){
  bindEvent();
});

