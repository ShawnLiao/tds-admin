var bindEvent = function() {
  $(".btn-add-group").click(addGroup);
  $("#group").change(groupChange);
  $(".btn-add-user").click(groupAddUser);
  $(".btn-add-page").click(groupAddPage);
	$(".btn-add-db-page").click(dbAddPage);
  $(".btn-del-db-page").click(dbDelPage);
	$(".btn-add-db-finance").click(dbAddfinance);
}

var groupDelPage = function() {
  $.post(
    "/api/admin/group_delete_page",
    {
      group_id: $("#group").find("option:selected").val(),
      page_id: $(this).parent().parent().children(":eq(0)").text()
    },
    function (data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          loadGroupPageData();
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("删除页面失败!");
      }
    }
  );
}

var groupDelUser = function() {
  $.post(
    "/api/admin/group_delete_user",
    {
      group_id: $("#group").find("option:selected").val(),
      user_id: $(this).parent().parent().children(":eq(0)").text()
    },
    function (data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          loadGroupUserData();
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("删除页面失败!");
      }
    }
  );
}

var groupAddPage = function() {
  $.post(
    "/api/admin/group_add_page",
    {
      group_id: $("#group").find("option:selected").val(),
      page_id: $("#page").find("option:selected").val()
    },
    function (data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          loadGroupPageData();
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("添加页面失败!");
      }
    }
  );
}

var dbAddPage = function() {
  $.post(
    "/api/admin/db_add_page",
    {
      path_name : $("#path_name").val(),
      page_name : $("#page_name").val()
    },
    function (data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          loadPage();
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("数据库添加页面失败!");
      }
    }
  );
}

var dbDelPage = function() {
  $.post(
    "/api/admin/db_del_page",
    {
			page_id : $("#del_page_name").find("option:selected").val()
    },
    function (data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          loadPage();
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("数据库删除页面失败!");
      }
    }
  );
}

var dbAddfinance = function() {
  var name = $("#add_finance_name").val();
  var email = $("#add_finance_email").val();
  $.post(
    "/api/admin/db_add_finance",
    {
      name : name,
      email : email
    },
    function (data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          alert("添加财务权限成功");
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("添加财务权限失败!");
      }
    }
  );
}


var groupAddUser = function() {
  var name = $("#name").val();
  $.post(
    "/api/admin/group_add_user",
    {
      group_id: $("#group").find("option:selected").val(),
      name : name
    },
    function (data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          loadGroupUserData();
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("添加用户失败!");
      }
    }
  );
}

var groupChange = function() {
  loadGroupUserData();
  loadGroupPageData();
}

var addGroup = function() {
  $.post(
    "/api/admin/add_group",
    {
      group: $("#add_group").val()
    },
    function (data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          var id = json_data.data.id;
          var group = json_data.data.group;
          appendGroup(id, group);
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("添加用户组失败!");
      }
    }
  );
}

var appendPage = function(id, page, name) {
  var option = $("<option/>");
  option.text(name + " (" + page + ")");
  option.val(id);
  $("#page").append(option);
}

var appendDelPage = function(id, page, name) {
  var option = $("<option/>");
  option.text(name + " (" + page + ")");
  option.val(id);
  $("#del_page_name").append(option);
}

var appendGroup = function(id, group) {
  var option = $("<option/>");
  option.text(group);
  option.val(id);
  $("#group").append(option);
}

var initPageSelect = function(page_list) {
  for (var idx in page_list) {
    appendPage(page_list[idx].id, page_list[idx].page, page_list[idx].name);
  }
  $("#page").children("option").eq(0).attr("selected",true);
}

var initDelPageSelect = function(page_list) {
  for (var idx in page_list) {
    appendDelPage(page_list[idx].id, page_list[idx].page, page_list[idx].name);
  }
  $("#del_page_name").children("option").eq(0).attr("selected",true);
}

var initGroupSelect = function(group_list) {
  for (var idx in group_list) {
    appendGroup(group_list[idx].id, group_list[idx].name);
  }
  $("#group").children("option").eq(0).attr("selected",true);
}

var loadPage = function() {
  $.post(
    "/api/admin/page_list",
    function(data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          initPageSelect(json_data.page_list);
					initDelPageSelect(json_data.page_list);
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("加载页面失败!");
      }
    }
  );
}

var loadGroup = function() {
  $.post(
    "/api/admin/group_list",
    function(data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          initGroupSelect(json_data.group_list);
          loadGroupUserData();
          loadGroupPageData();
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("加载用户组失败!");
      }
    }
  );
}

var handleGroupUserResults = function(data) {
  $("#user-list").children(".item").remove();
  var btn_template = $("<button>").addClass("btn-link").text("删除");
  for (var idx in data.group_user_list) {
    var btn_del = btn_template.clone();
    btn_del.click(groupDelUser);
    var html = $("#user-template").clone().css("display", "").addClass("item");
    html.children("td").eq(0).text(data.group_user_list[idx].id);
    html.children("td").eq(1).text(data.group_user_list[idx].email);
    html.children("td").eq(2).text(data.group_user_list[idx].name);
    html.children("td").eq(3).append(btn_del);
    html.appendTo($("#user-list"));
  }
}

var loadGroupUserData = function() {
  $.post(
    "/api/admin/group_user_list",
    {
      group_id: $("#group").find("option:selected").val()
    },
    function(data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          handleGroupUserResults(json_data);
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("请求group user失败!");
      }
    }
  );
}

var handleGroupPageResults = function(data) {
  $("#page-list").children(".item").remove();
  var btn_template = $("<button>").addClass("btn-link").text("删除");
  for (var idx in data.group_page_list) {
    var btn_del = btn_template.clone();
    btn_del.click(groupDelPage);
    var html = $("#page-template").clone().css("display", "").addClass("item");
    html.children("td").eq(0).text(data.group_page_list[idx].id);
    html.children("td").eq(1).text(data.group_page_list[idx].name);
    html.children("td").eq(2).text(data.group_page_list[idx].page);
    html.children("td").eq(3).append(btn_del);
    html.appendTo($("#page-list"));
  }
}

var loadGroupPageData = function() {
  $.post(
    "/api/admin/group_page_list",
    {
      group_id: $("#group").find("option:selected").val()
    },
    function(data, status) {
      if (status == "success") {
        var json_data = eval('(' + data + ')');
        if (json_data.code == 0) {
          handleGroupPageResults(json_data);
        } else {
          alert(json_data.msg);
        }
      } else {
        alert("请求group page失败!");
      }
    }
  );
}

$(function(){
  loadGroup();
  loadPage();
  bindEvent();
});

