<?php defined('SYSPATH') OR die('No direct script access.');

class SqlBuilder {
  private $has_select_from = false;

  private $sql_select = "";

  private $from_alias = "";

  private $left_alias = "";

  private $use_left_alias = false;

  private $sql_from = "";

  private $sql_join_on = array();

  private $sql_where = "";

  private $group_by = "";

  private $sql_order_by = "";

  private $sql_group_by = "";

  private $sql_having = "";

  public function select($cols, $is_distinct = false) {
    $this->sql_select = $is_distinct ? "SELECT DISTINCT " : "SELECT ";
    $first = true;
    foreach ($cols as $val) {
      if (!$first) {
        $this->sql_select .= ", ";
      }
      $this->sql_select .=
        $val["table"] . (empty($val["table"]) ? "" : ".") . $val["col"] . " AS " . $val["as"];
      $first = false;
    }
    return $this;
  }

  public function from($table, $alias, $is_sub_select = false) {
    $this->from_alias = $alias;
    $table = $is_sub_select ? "(" . $table . ")" : $table;
    $this->sql_from = "FROM " . $table . " " . $alias;
    return $this;
  }

  public function left_alias($alias) {
    $this->left_alias = $alias;
    $this->use_left_alias = true;
    return $this;
  }

  public function inner_join_on($table, $alias, $on_conditions, $is_sub_select = false) {
    $left_alias = $this->from_alias;
    if ($this->use_left_alias) {
      $left_alias = $this->left_alias;
      $this->use_left_alias = false;
    }

    $table = $is_sub_select ? "(" . $table . ")" : $table;
    $sql = "INNER JOIN " . $table . " " . $alias . " ON ";
    $first = true;
    foreach ($on_conditions as $cond) {
      if (!$first) {
        $sql .= "AND ";
      }
      $sql .= $left_alias . "." . $cond[0] . " " . $cond[1] . " " . $alias . "." . $cond[2];
      $first =false;
    }
    $this->sql_join_on[] = $sql;
    return $this;
  }

  public function left_outer_join_on($table, $alias, $on_conditions, $is_sub_select = false) {
    $left_alias = $this->from_alias;
    if ($this->use_left_alias) {
      $left_alias = $this->left_alias;
      $this->use_left_alias = false;
    }

    $table = $is_sub_select ? "(" . $table . ")" : $table;
    $sql = "LEFT OUTER JOIN " . $table . " " . $alias . " ON ";
    $first = true;
    foreach ($on_conditions as $cond) {
      if (!$first) {
        $sql .= " AND ";
      }
      $sql .= $left_alias . "." . $cond[0] . " " . $cond[1] . " " . $alias . "." . $cond[2];
      $first =false;
    }
    $this->sql_join_on[] = $sql;
    return $this;
  }

  public function group_by($conditions){
    if (empty($conditions))  {
        return;
    }
    $this->sql_group_by = "GROUP BY ";
    $this->sql_group_by .= $conditions[0];

    for ($x=1;$x<count($conditions);$x++) {
      $this->sql_group_by .= ",". $conditions[$x];
    }
    return $this;
  }


  public function order_by($conditions){
    if (empty($conditions))  {
        return;
    }
    $this->sql_order_by = "ORDER BY ";
    $this->sql_order_by .= $conditions[0];

    for ($x=1;$x<count($conditions);$x++) {
      $this->sql_order_by .= ",". $conditions[$x];
    }
    return $this;
  }


  public function where($conditions) {
    if (empty($conditions)) {
      return;
    }
    $this->sql_where = "WHERE ";
    $first = true;
    foreach ($conditions as $cond) {
      if (!$first) {
        $this->sql_where .= " AND ";
      }
      $this->sql_where .= $cond[0] . " " . $cond[1] . " " . $cond[2];
      $first =false;
    }
    return $this;
  }


  public function having($conditions) {
    if (empty($conditions)) {
      return;
    }
    $this->sql_having = "HAVING ";
    $first = true;
    foreach ($conditions as $cond) {
      if (!$first) {
        $this->sql_having .= " AND ";
      }
      $this->sql_having .= $cond[0] . " " . $cond[1] . " " . $cond[2];
      $first =false;
    }
    return $this;
  }

  public function build() {
    $sql =  $this->sql_select . " " . $this->sql_from;
    foreach($this->sql_join_on as $join_sql) {
      $sql .= " " . $join_sql;
    }
    $sql .= " " . $this->sql_where;
    $sql .= " " . $this->sql_group_by;
    $sql .= " " . $this->sql_order_by;
    $sql .= " " . $this->sql_having;
    return $sql;
  }
}
